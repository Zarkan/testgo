package nhsppoc

import (
	"log"
	wsclient "nhsppoc/nhspInterfaces"
	wsmodel "nhsppoc/nhspInterfaces/wsdlgo"
	protobuff "nhsppoc/protobuffModel"
	//"sync"
	"time"
)

const PageSize = uint32(21)
const DeleyPeriod = 500

type SearchDataSummary struct {
	TotalNumber       uint32
	PartsNumber       uint32
	LoadedPartsNumber uint32
	TabId             string
	RedisKey          string
	Compleated        bool
	DataChannel       chan *wsmodel.IVRShiftType
	token             string
	WebUserId         string
	partData          []*[]*wsmodel.IVRShiftType
}

func NewSearchDataSummary() *SearchDataSummary {
	return &SearchDataSummary{
		DataChannel:       make(chan *wsmodel.IVRShiftType, 4500),
		TotalNumber:       uint32(20),
		PartsNumber:       uint32(0),
		Compleated:        false,
		LoadedPartsNumber: 0,
		WebUserId:         "SAdminNHSP",
	}
}

func (search *SearchDataSummary) performSearch(startDate time.Time, endDate time.Time) {
	search.getToken()
	search.PartsNumber = uint32(endDate.Sub(startDate).Hours() / 24)
	search.partData = make([]*[]*wsmodel.IVRShiftType, search.PartsNumber)
	duration, err := time.ParseDuration("24h")
	if err != nil {
		log.Panicln("Wrong Duration")
	}
	tempDate := startDate
	// log.Printf("start req=%v\n", time.Now())
	deleyMili := 0
	var resultPartChan = make(chan uint32, search.PartsNumber)
	for i := uint32(0); i < search.PartsNumber; i++ {
		delay := time.Duration(deleyMili) * time.Millisecond
		go func(tempDate time.Time, tempidx uint32) {
			time.AfterFunc(delay, func() {
				search.performPartSearch(tempDate, tempidx, resultPartChan)
			})
		}(tempDate, i)
		tempDate = tempDate.Add(duration)
		deleyMili = deleyMili + DeleyPeriod

	}
	go search.handleResults(resultPartChan)

}

func (search *SearchDataSummary) handleResults(in chan uint32) {
	startidx := uint32(0)
	done := false
	sendsize := uint32(0)

	for i := uint32(0); i < search.PartsNumber; i++ {
		// log.Printf("loading=%v loop search.PartsNumber=%v\n", i, search.PartsNumber)
		idx := <-in
		// log.Printf("Done loading=%v, search.PartsNumber=%v\n", idx, search.PartsNumber)
		if idx == startidx && done == false {
			startidx, sendsize = search.sendInitResponse(idx, sendsize)
			// log.Printf("startidx=%v, sendsize=%v\n", startidx, sendsize)
			if sendsize == PageSize {
				done = true
			}
		}
	}

	log.Printf("!!!!!!!!!!!!!!!!Sended=%v\n", sendsize)

}

func (search *SearchDataSummary) sendInitResponse(idx uint32, sendSize uint32) (uint32, uint32) {
	if search.partData[idx] == nil {
		return idx, sendSize
	}
	data := *search.partData[idx]
	size := uint32(len(data))
	if size > PageSize-sendSize {
		size = PageSize - sendSize
	}
	for i := uint32(0); i < size; i++ {
		search.DataChannel <- data[i]
	}
	if size != PageSize && idx < search.PartsNumber-1 {
		newIdx := idx + 1
		return search.sendInitResponse(newIdx, uint32(sendSize+uint32(size)))
	}
	return idx, sendSize
}

func (search *SearchDataSummary) performPartSearch(date time.Time, idx uint32, out chan uint32) {
	// log.Printf("partStore idx=%v\n", idx)
	var shiftDateString = (wsmodel.DateTime)(date.UTC().Format(time.RFC3339))
	boardViewResponse, err := wsclient.Wsclient.BoardView(&wsmodel.BoardView{
		RequestId:   55,
		LocationId:  "All",
		WardId:      "All",
		CallerToken: search.token,
		WebUserId:   search.WebUserId,
		DateRange:   "CustomDateRange",
		StartDate:   shiftDateString,
		EndDate:     shiftDateString,
	})
	if err != nil {
		log.Panicf("Expected BoardView, got %v , err=%v", boardViewResponse, err)
	}
	search.partData[idx] = &boardViewResponse.Shifts.IVRShiftType
	log.Printf("Done= %v \n", idx)
	out <- idx

	/*for _, shift := range boardViewResponse.Shifts.IVRShiftType {
		for j := 0; j < 5; j++ {
			out <- shift

		}
	}*/

}
func (search *SearchDataSummary) getToken() {
	reponse, err := wsclient.Wsclient.AuthenticateCaller(&wsmodel.AuthenticateCaller{RequestId: 55, Username: "Trust:Bank", Password: "Warszawa*0900"})
	if reponse.CallerToken == "" || err != nil {
		log.Panicf("Expected CallerToken, got %v , err=%v", reponse, err)
	}
	search.token = reponse.CallerToken
}

func SearchForShifts() *SearchDataSummary {
	startDate := time.Now()
	tempEndDuration, err := time.ParseDuration("640h")
	if err != nil {
		log.Panicln("Wrong Duration")
	}
	endDate := startDate.Add(tempEndDuration)

	search := NewSearchDataSummary()
	log.Println("SearchForShifts")
	search.performSearch(startDate, endDate)
	return search
	/*	var WebUserId = "SAdminNHSP"

		out := make(chan *wsmodel.IVRShiftType, 4500)
		var wg sync.WaitGroup
		wg.Add(partsNumber)
		for i := 0; i < partsNumber; i++ {
			t = t.Add(d)

			go func(out chan *wsmodel.IVRShiftType, shiftDate time.Time, deleyMili int) {
				//

				delay := time.Duration(deleyMili) * time.Millisecond
				time.AfterFunc(delay, func() {

					// log.Printf("Done %v\n", deleyMili)
					wg.Done()
				})
				//done <- struct{}{}

			}(out, t, i)
		}
		go func() {
			wg.Wait()

			close(out)
		}()

		return &SearchDataSummary{DataChannel: out, TotalNumber: uint32(20 * partsNumber), PartsNumber: uint32(partsNumber)}*/

}

func cleanModel(wsdlShift *wsmodel.IVRShiftType) *protobuff.Shift {
	shift := protobuff.Shift{}
	shift.BookingReferenceNumber = &wsdlShift.BookingReferenceNumber
	shift.AssignmentCode = &wsdlShift.AssignmentCode
	g := string(wsdlShift.Gender)
	shift.Gender = &g
	shift.HasComment = &wsdlShift.HasComment
	shift.HasGoldenKey = &wsdlShift.HasGoldenKey
	shift.IsQualifiedAssignment = &wsdlShift.IsQualifiedAssignment
	shift.IsRangeRequest = &wsdlShift.IsRangeRequest
	shift.LocationID = &wsdlShift.LocationId
	shift.NoAgency = &wsdlShift.NoAgency
	shift.RangeFilledBy = &wsdlShift.RangeFilledBy
	shift.SecondaryAssignmentCode = &wsdlShift.SecondaryAssignmentCode
	shift.SecondaryAssignmentCodeDuration = &wsdlShift.SecondaryAssignmentCodeDuration
	ss := string(wsdlShift.ShiftStatus)
	st := string(wsdlShift.StartTime)
	et := string(wsdlShift.EndTime)
	dd := string(wsdlShift.Date)
	shift.ShiftStatus = &ss
	shift.StartTime = &st
	shift.EndTime = &et
	shift.Date = &dd
	shift.TrustAuthorisationCode = &wsdlShift.TrustAuthorisationCode
	shift.TrustID = &wsdlShift.TrustId
	shift.TwoTierAuthorisationShiftStatus = &wsdlShift.TwoTierAuthorisationShiftStatus
	shift.WardID = &wsdlShift.WardId
	shift.BookingReferenceNumber = &wsdlShift.BookingReferenceNumber
	shift.BookingReferenceNumber = &wsdlShift.BookingReferenceNumber

	return &shift

}

/*func performFiltering(shifts *[]Shift) *[]*Shift {

	//defer close(out)
	t4 := time.Now()
	var wg sync.WaitGroup
	wg.Add(len(*shifts))
	for _, shiftEl := range *shifts {
		go func(out chan *Shift, shift Shift) {

			if shift.ShiftStatus == 6 {
				//fmt.Println("send: ", shift.BookingReferenceNumber)
				out <- &shift
			}
			wg.Done()
			//done <- struct{}{}

		}(out, shiftEl)
	}

	wg.Wait()

	close(out)

	for shift := range out {
		filterred = append(filterred, shift)

	}
	t5 := time.Now()
	fmt.Println("LEN filtered: ")

	fmt.Printf("before %+v\n\n", len(*shifts))
	fmt.Printf("after %+v\n\n", len(filterred))

	fmt.Printf("Filtering took %v to run.\n", t5.Sub(t4))
	return &filterred
}*/
