// Code generated by protoc-gen-go.
// source: model.proto
// DO NOT EDIT!

/*
Package model is a generated protocol buffer package.

It is generated from these files:
	model.proto

It has these top-level messages:
	Message
	DataSummary
	Agency
	BookingReason
	StaffGroup
	Shift
*/
package model

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Message struct {
	Text             *string `protobuf:"bytes,1,opt,name=text" json:"text,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Message) Reset()                    { *m = Message{} }
func (m *Message) String() string            { return proto.CompactTextString(m) }
func (*Message) ProtoMessage()               {}
func (*Message) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Message) GetText() string {
	if m != nil && m.Text != nil {
		return *m.Text
	}
	return ""
}

type DataSummary struct {
	TotalNumber      *uint32 `protobuf:"varint,1,opt,name=TotalNumber,json=totalNumber" json:"TotalNumber,omitempty"`
	PartsNumber      *uint32 `protobuf:"varint,2,opt,name=PartsNumber,json=partsNumber" json:"PartsNumber,omitempty"`
	TabId            *string `protobuf:"bytes,3,opt,name=TabId,json=tabId" json:"TabId,omitempty"`
	RedisKey         *string `protobuf:"bytes,4,opt,name=RedisKey,json=redisKey" json:"RedisKey,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *DataSummary) Reset()                    { *m = DataSummary{} }
func (m *DataSummary) String() string            { return proto.CompactTextString(m) }
func (*DataSummary) ProtoMessage()               {}
func (*DataSummary) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *DataSummary) GetTotalNumber() uint32 {
	if m != nil && m.TotalNumber != nil {
		return *m.TotalNumber
	}
	return 0
}

func (m *DataSummary) GetPartsNumber() uint32 {
	if m != nil && m.PartsNumber != nil {
		return *m.PartsNumber
	}
	return 0
}

func (m *DataSummary) GetTabId() string {
	if m != nil && m.TabId != nil {
		return *m.TabId
	}
	return ""
}

func (m *DataSummary) GetRedisKey() string {
	if m != nil && m.RedisKey != nil {
		return *m.RedisKey
	}
	return ""
}

type Agency struct {
	AgencyID         *string `protobuf:"bytes,1,opt,name=AgencyID,json=agencyID" json:"AgencyID,omitempty"`
	AgencyName       *string `protobuf:"bytes,2,opt,name=AgencyName,json=agencyName" json:"AgencyName,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Agency) Reset()                    { *m = Agency{} }
func (m *Agency) String() string            { return proto.CompactTextString(m) }
func (*Agency) ProtoMessage()               {}
func (*Agency) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Agency) GetAgencyID() string {
	if m != nil && m.AgencyID != nil {
		return *m.AgencyID
	}
	return ""
}

func (m *Agency) GetAgencyName() string {
	if m != nil && m.AgencyName != nil {
		return *m.AgencyName
	}
	return ""
}

type BookingReason struct {
	ReasonCode        *string `protobuf:"bytes,1,opt,name=ReasonCode,json=reasonCode" json:"ReasonCode,omitempty"`
	ReasonDescription *string `protobuf:"bytes,2,opt,name=ReasonDescription,json=reasonDescription" json:"ReasonDescription,omitempty"`
	XXX_unrecognized  []byte  `json:"-"`
}

func (m *BookingReason) Reset()                    { *m = BookingReason{} }
func (m *BookingReason) String() string            { return proto.CompactTextString(m) }
func (*BookingReason) ProtoMessage()               {}
func (*BookingReason) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *BookingReason) GetReasonCode() string {
	if m != nil && m.ReasonCode != nil {
		return *m.ReasonCode
	}
	return ""
}

func (m *BookingReason) GetReasonDescription() string {
	if m != nil && m.ReasonDescription != nil {
		return *m.ReasonDescription
	}
	return ""
}

type StaffGroup struct {
	StaffGroupID     *string `protobuf:"bytes,1,opt,name=StaffGroupID,json=staffGroupID" json:"StaffGroupID,omitempty"`
	StaffGroupName   *string `protobuf:"bytes,2,opt,name=StaffGroupName,json=staffGroupName" json:"StaffGroupName,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *StaffGroup) Reset()                    { *m = StaffGroup{} }
func (m *StaffGroup) String() string            { return proto.CompactTextString(m) }
func (*StaffGroup) ProtoMessage()               {}
func (*StaffGroup) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *StaffGroup) GetStaffGroupID() string {
	if m != nil && m.StaffGroupID != nil {
		return *m.StaffGroupID
	}
	return ""
}

func (m *StaffGroup) GetStaffGroupName() string {
	if m != nil && m.StaffGroupName != nil {
		return *m.StaffGroupName
	}
	return ""
}

type Shift struct {
	Agency                 *Agency        `protobuf:"bytes,1,opt,name=Agency,json=agency" json:"Agency,omitempty"`
	AssignmentCode         *string        `protobuf:"bytes,2,opt,name=AssignmentCode,json=assignmentCode" json:"AssignmentCode,omitempty"`
	BookingReason          *BookingReason `protobuf:"bytes,3,opt,name=BookingReason,json=bookingReason" json:"BookingReason,omitempty"`
	BookingReferenceNumber *string        `protobuf:"bytes,4,opt,name=BookingReferenceNumber,json=bookingReferenceNumber" json:"BookingReferenceNumber,omitempty"`
	Date                   *string        `protobuf:"bytes,5,opt,name=Date,json=date" json:"Date,omitempty"`
	EndTime                *string        `protobuf:"bytes,6,opt,name=EndTime,json=endTime" json:"EndTime,omitempty"`
	Gender                 *string        `protobuf:"bytes,7,opt,name=Gender,json=gender" json:"Gender,omitempty"`
	HasComment             *bool          `protobuf:"varint,8,opt,name=HasComment,json=hasComment" json:"HasComment,omitempty"`
	HasGoldenKey           *bool          `protobuf:"varint,9,opt,name=HasGoldenKey,json=hasGoldenKey" json:"HasGoldenKey,omitempty"`
	// Induction              interface{} `json:"Induction"`
	IsQualifiedAssignment *bool   `protobuf:"varint,10,opt,name=IsQualifiedAssignment,json=isQualifiedAssignment" json:"IsQualifiedAssignment,omitempty"`
	IsRangeRequest        *bool   `protobuf:"varint,11,opt,name=IsRangeRequest,json=isRangeRequest" json:"IsRangeRequest,omitempty"`
	LocationID            *string `protobuf:"bytes,12,opt,name=LocationID,json=locationID" json:"LocationID,omitempty"`
	NoAgency              *bool   `protobuf:"varint,13,opt,name=NoAgency,json=noAgency" json:"NoAgency,omitempty"`
	// Note                   []string    `json:"Note"`
	// NoteDetails            []struct {
	// 	Note       string   `json:"Note"`
	// 	ViewableBy []string `json:"ViewableBy"`
	// } `json:"NoteDetails"`
	// OnCall                          bool        `json:"OnCall"`
	// ProbabilityScore                float64     `json:"ProbabilityScore"`
	RangeFilledBy                   *string `protobuf:"bytes,14,opt,name=RangeFilledBy,json=rangeFilledBy" json:"RangeFilledBy,omitempty"`
	SecondaryAssignmentCode         *string `protobuf:"bytes,15,opt,name=SecondaryAssignmentCode,json=secondaryAssignmentCode" json:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration *string `protobuf:"bytes,16,opt,name=SecondaryAssignmentCodeDuration,json=secondaryAssignmentCodeDuration" json:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftStatus                     *string `protobuf:"bytes,17,opt,name=ShiftStatus,json=shiftStatus" json:"ShiftStatus,omitempty"`
	ShiftType                       *uint32 `protobuf:"varint,18,opt,name=ShiftType,json=shiftType" json:"ShiftType,omitempty"`
	// StaffFacade                     interface{} `json:"StaffFacade"`
	StaffGroup                      *StaffGroup `protobuf:"bytes,24,opt,name=StaffGroup,json=staffGroup" json:"StaffGroup,omitempty"`
	StartTime                       *string     `protobuf:"bytes,19,opt,name=StartTime,json=startTime" json:"StartTime,omitempty"`
	TrustAuthorisationCode          *string     `protobuf:"bytes,20,opt,name=TrustAuthorisationCode,json=trustAuthorisationCode" json:"TrustAuthorisationCode,omitempty"`
	TrustID                         *string     `protobuf:"bytes,21,opt,name=TrustID,json=trustID" json:"TrustID,omitempty"`
	TwoTierAuthorisationShiftStatus *string     `protobuf:"bytes,22,opt,name=TwoTierAuthorisationShiftStatus,json=twoTierAuthorisationShiftStatus" json:"TwoTierAuthorisationShiftStatus,omitempty"`
	// WardDetails                     interface{} `json:"WardDetails"`
	WardID           *string `protobuf:"bytes,23,opt,name=WardID,json=wardID" json:"WardID,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Shift) Reset()                    { *m = Shift{} }
func (m *Shift) String() string            { return proto.CompactTextString(m) }
func (*Shift) ProtoMessage()               {}
func (*Shift) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{5} }

func (m *Shift) GetAgency() *Agency {
	if m != nil {
		return m.Agency
	}
	return nil
}

func (m *Shift) GetAssignmentCode() string {
	if m != nil && m.AssignmentCode != nil {
		return *m.AssignmentCode
	}
	return ""
}

func (m *Shift) GetBookingReason() *BookingReason {
	if m != nil {
		return m.BookingReason
	}
	return nil
}

func (m *Shift) GetBookingReferenceNumber() string {
	if m != nil && m.BookingReferenceNumber != nil {
		return *m.BookingReferenceNumber
	}
	return ""
}

func (m *Shift) GetDate() string {
	if m != nil && m.Date != nil {
		return *m.Date
	}
	return ""
}

func (m *Shift) GetEndTime() string {
	if m != nil && m.EndTime != nil {
		return *m.EndTime
	}
	return ""
}

func (m *Shift) GetGender() string {
	if m != nil && m.Gender != nil {
		return *m.Gender
	}
	return ""
}

func (m *Shift) GetHasComment() bool {
	if m != nil && m.HasComment != nil {
		return *m.HasComment
	}
	return false
}

func (m *Shift) GetHasGoldenKey() bool {
	if m != nil && m.HasGoldenKey != nil {
		return *m.HasGoldenKey
	}
	return false
}

func (m *Shift) GetIsQualifiedAssignment() bool {
	if m != nil && m.IsQualifiedAssignment != nil {
		return *m.IsQualifiedAssignment
	}
	return false
}

func (m *Shift) GetIsRangeRequest() bool {
	if m != nil && m.IsRangeRequest != nil {
		return *m.IsRangeRequest
	}
	return false
}

func (m *Shift) GetLocationID() string {
	if m != nil && m.LocationID != nil {
		return *m.LocationID
	}
	return ""
}

func (m *Shift) GetNoAgency() bool {
	if m != nil && m.NoAgency != nil {
		return *m.NoAgency
	}
	return false
}

func (m *Shift) GetRangeFilledBy() string {
	if m != nil && m.RangeFilledBy != nil {
		return *m.RangeFilledBy
	}
	return ""
}

func (m *Shift) GetSecondaryAssignmentCode() string {
	if m != nil && m.SecondaryAssignmentCode != nil {
		return *m.SecondaryAssignmentCode
	}
	return ""
}

func (m *Shift) GetSecondaryAssignmentCodeDuration() string {
	if m != nil && m.SecondaryAssignmentCodeDuration != nil {
		return *m.SecondaryAssignmentCodeDuration
	}
	return ""
}

func (m *Shift) GetShiftStatus() string {
	if m != nil && m.ShiftStatus != nil {
		return *m.ShiftStatus
	}
	return ""
}

func (m *Shift) GetShiftType() uint32 {
	if m != nil && m.ShiftType != nil {
		return *m.ShiftType
	}
	return 0
}

func (m *Shift) GetStaffGroup() *StaffGroup {
	if m != nil {
		return m.StaffGroup
	}
	return nil
}

func (m *Shift) GetStartTime() string {
	if m != nil && m.StartTime != nil {
		return *m.StartTime
	}
	return ""
}

func (m *Shift) GetTrustAuthorisationCode() string {
	if m != nil && m.TrustAuthorisationCode != nil {
		return *m.TrustAuthorisationCode
	}
	return ""
}

func (m *Shift) GetTrustID() string {
	if m != nil && m.TrustID != nil {
		return *m.TrustID
	}
	return ""
}

func (m *Shift) GetTwoTierAuthorisationShiftStatus() string {
	if m != nil && m.TwoTierAuthorisationShiftStatus != nil {
		return *m.TwoTierAuthorisationShiftStatus
	}
	return ""
}

func (m *Shift) GetWardID() string {
	if m != nil && m.WardID != nil {
		return *m.WardID
	}
	return ""
}

func init() {
	proto.RegisterType((*Message)(nil), "Message")
	proto.RegisterType((*DataSummary)(nil), "DataSummary")
	proto.RegisterType((*Agency)(nil), "Agency")
	proto.RegisterType((*BookingReason)(nil), "BookingReason")
	proto.RegisterType((*StaffGroup)(nil), "StaffGroup")
	proto.RegisterType((*Shift)(nil), "Shift")
}

func init() { proto.RegisterFile("model.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 663 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x7c, 0x54, 0x4d, 0x6f, 0x13, 0x31,
	0x10, 0x55, 0xa0, 0xf9, 0x9a, 0x4d, 0x02, 0x35, 0x6d, 0x6a, 0x21, 0xa0, 0x55, 0x84, 0x50, 0x25,
	0x50, 0x0e, 0x55, 0x85, 0xb8, 0xb6, 0x5d, 0x68, 0x23, 0xa0, 0x82, 0x4d, 0x24, 0xb8, 0x70, 0x70,
	0xb2, 0x93, 0xc4, 0x62, 0x77, 0x1d, 0x6c, 0xaf, 0x4a, 0xce, 0xfc, 0x3c, 0xfe, 0x14, 0xf2, 0x78,
	0x9b, 0x4d, 0xaa, 0xd0, 0x9b, 0xe7, 0xbd, 0x37, 0xe3, 0xf1, 0xcc, 0xdb, 0x85, 0x20, 0x55, 0x31,
	0x26, 0xfd, 0x85, 0x56, 0x56, 0xf5, 0x9e, 0x43, 0xfd, 0x33, 0x1a, 0x23, 0x66, 0xc8, 0x18, 0xec,
	0x58, 0xfc, 0x6d, 0x79, 0xe5, 0xa8, 0x72, 0xdc, 0x8c, 0xe8, 0xdc, 0xfb, 0x53, 0x81, 0x20, 0x14,
	0x56, 0x0c, 0xf3, 0x34, 0x15, 0x7a, 0xc9, 0x8e, 0x20, 0x18, 0x29, 0x2b, 0x92, 0xeb, 0x3c, 0x1d,
	0xa3, 0x26, 0x69, 0x3b, 0x0a, 0x6c, 0x09, 0x39, 0xc5, 0x17, 0xa1, 0xad, 0x29, 0x14, 0x0f, 0xbc,
	0x62, 0x51, 0x42, 0x6c, 0x0f, 0xaa, 0x23, 0x31, 0x1e, 0xc4, 0xfc, 0x21, 0x5d, 0x54, 0xb5, 0x2e,
	0x60, 0x4f, 0xa1, 0x11, 0x61, 0x2c, 0xcd, 0x47, 0x5c, 0xf2, 0x1d, 0x22, 0x1a, 0xba, 0x88, 0x7b,
	0x21, 0xd4, 0xce, 0x66, 0x98, 0x4d, 0x96, 0x4e, 0xe5, 0x4f, 0x83, 0xb0, 0xe8, 0xb3, 0x21, 0x8a,
	0x98, 0xbd, 0x00, 0xf0, 0xdc, 0xb5, 0x48, 0x91, 0x2e, 0x6e, 0x46, 0x20, 0x56, 0x48, 0xef, 0x07,
	0xb4, 0xcf, 0x95, 0xfa, 0x29, 0xb3, 0x59, 0x84, 0xc2, 0xa8, 0xcc, 0x25, 0xf8, 0xd3, 0x85, 0x8a,
	0xb1, 0x28, 0x07, 0x7a, 0x85, 0xb0, 0x37, 0xb0, 0xeb, 0xf9, 0x10, 0xcd, 0x44, 0xcb, 0x85, 0x95,
	0x2a, 0x2b, 0xea, 0xee, 0xea, 0xbb, 0x44, 0xef, 0x3b, 0xc0, 0xd0, 0x8a, 0xe9, 0xf4, 0x52, 0xab,
	0x7c, 0xc1, 0x7a, 0xd0, 0x2a, 0xa3, 0x55, 0xb3, 0x2d, 0xb3, 0x86, 0xb1, 0x57, 0xd0, 0x29, 0x35,
	0x6b, 0x4d, 0x77, 0xcc, 0x06, 0xda, 0xfb, 0x5b, 0x87, 0xea, 0x70, 0x2e, 0xa7, 0x96, 0x1d, 0xde,
	0x0e, 0x82, 0xea, 0x05, 0x27, 0xf5, 0xbe, 0x0f, 0xa3, 0x9a, 0x7f, 0xa7, 0x2b, 0x79, 0x66, 0x8c,
	0x9c, 0x65, 0x29, 0x66, 0x96, 0x9e, 0x55, 0x94, 0x14, 0x1b, 0x28, 0x3b, 0xbd, 0x33, 0x0b, 0xda,
	0x45, 0x70, 0xd2, 0xe9, 0x6f, 0xa0, 0x51, 0x7b, 0xbc, 0x31, 0xb0, 0xb7, 0xd0, 0x5d, 0xf1, 0x53,
	0xd4, 0x98, 0x4d, 0xb0, 0x58, 0xb3, 0xdf, 0x58, 0x77, 0xbc, 0x95, 0x75, 0xce, 0x0a, 0x85, 0x45,
	0x5e, 0xf5, 0xce, 0x8a, 0x85, 0x45, 0xc6, 0xa1, 0xfe, 0x3e, 0x8b, 0x47, 0x32, 0x45, 0x5e, 0x23,
	0xb8, 0x8e, 0x3e, 0x64, 0x5d, 0xa8, 0x5d, 0x62, 0x16, 0xa3, 0xe6, 0x75, 0x22, 0x6a, 0x33, 0x8a,
	0xdc, 0xba, 0xae, 0x84, 0xb9, 0x50, 0xa9, 0x7b, 0x05, 0x6f, 0x1c, 0x55, 0x8e, 0x1b, 0x11, 0xcc,
	0x57, 0x88, 0x1b, 0xf9, 0x95, 0x30, 0x97, 0x2a, 0x89, 0x31, 0x73, 0x2e, 0x6a, 0x92, 0xa2, 0x35,
	0x5f, 0xc3, 0xd8, 0x29, 0xec, 0x0f, 0xcc, 0xd7, 0x5c, 0x24, 0x72, 0x2a, 0x31, 0x2e, 0x47, 0xc5,
	0x81, 0xc4, 0xfb, 0x72, 0x1b, 0xe9, 0xa6, 0x3a, 0x30, 0x91, 0xc8, 0x66, 0x18, 0xe1, 0xaf, 0x1c,
	0x8d, 0xe5, 0x01, 0xc9, 0x3b, 0x72, 0x03, 0x75, 0x1d, 0x7e, 0x52, 0x13, 0xe1, 0xec, 0x30, 0x08,
	0x79, 0xcb, 0x1b, 0x2a, 0x59, 0x21, 0xce, 0xbd, 0xd7, 0xaa, 0x58, 0x60, 0x9b, 0x2a, 0x34, 0xb2,
	0x22, 0x66, 0x2f, 0xa1, 0x4d, 0xb5, 0x3e, 0xc8, 0x24, 0xc1, 0xf8, 0x7c, 0xc9, 0x3b, 0x94, 0xde,
	0xd6, 0xeb, 0x20, 0x7b, 0x07, 0x07, 0x43, 0x9c, 0xa8, 0x2c, 0x16, 0x7a, 0x79, 0x67, 0xd1, 0x8f,
	0x48, 0x7f, 0x60, 0xb6, 0xd3, 0xec, 0x0a, 0x0e, 0xff, 0x93, 0x19, 0xe6, 0x9a, 0x1a, 0xe4, 0x8f,
	0xa9, 0xc2, 0xa1, 0xb9, 0x5f, 0xe6, 0xbe, 0x70, 0x72, 0xe3, 0xd0, 0x0a, 0x9b, 0x1b, 0xbe, 0x4b,
	0x59, 0x81, 0x29, 0x21, 0xf6, 0x0c, 0x9a, 0xa4, 0x18, 0x2d, 0x17, 0xc8, 0x19, 0xfd, 0x01, 0x9a,
	0xe6, 0x16, 0x60, 0xaf, 0xd7, 0x3f, 0x14, 0xce, 0xc9, 0x78, 0x41, 0xbf, 0x84, 0x22, 0x28, 0xfd,
	0x4f, 0xa5, 0xac, 0xd0, 0x96, 0x8c, 0xf2, 0x84, 0xae, 0x6a, 0x9a, 0x5b, 0xc0, 0x19, 0x72, 0xa4,
	0x73, 0x63, 0xcf, 0x72, 0x3b, 0x57, 0x5a, 0x1a, 0x6a, 0x90, 0xa6, 0xb1, 0xe7, 0x0d, 0x69, 0xb7,
	0xb2, 0xce, 0x7c, 0x94, 0x37, 0x08, 0xf9, 0xbe, 0x37, 0x9f, 0xf5, 0xa1, 0x1b, 0xd3, 0xe8, 0x46,
	0x8d, 0x24, 0xea, 0x8d, 0xac, 0xf5, 0x07, 0x77, 0xfd, 0x98, 0xec, 0xfd, 0x32, 0x67, 0xe3, 0x6f,
	0x42, 0xc7, 0x83, 0x90, 0x1f, 0x78, 0x1b, 0xdf, 0x50, 0xf4, 0x2f, 0x00, 0x00, 0xff, 0xff, 0xe0,
	0xe3, 0x6a, 0xe1, 0x7f, 0x05, 0x00, 0x00,
}
