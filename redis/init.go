package redis

import (
	"crypto/tls"
	"crypto/x509"
	"log"

	"github.com/garyburd/redigo/redis"
)

var redisPool *redis.Pool

const redisPEM = `
-----BEGIN CERTIFICATE-----
MIIGJzCCBQ+gAwIBAgIKdCkfwgABAADZ3TANBgkqhkiG9w0BAQUFADCBgDETMBEG
CgmSJomT8ixkARkWA2NvbTEZMBcGCgmSJomT8ixkARkWCW1pY3Jvc29mdDEUMBIG
CgmSJomT8ixkARkWBGNvcnAxFzAVBgoJkiaJk/IsZAEZFgdyZWRtb25kMR8wHQYD
VQQDExZNU0lUIE1hY2hpbmUgQXV0aCBDQSAyMB4XDTE0MDQyMjAxMjkzNFoXDTE2
MDQyMTAxMjkzNFowfzELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAldBMRAwDgYDVQQH
EwdSZWRtb25kMRIwEAYDVQQKEwlNaWNyb3NvZnQxGTAXBgNVBAsTEE9yZ2FuaXph
dGlvbk5hbWUxIjAgBgNVBAMMGSoucmVkaXMuY2FjaGUud2luZG93cy5uZXQwggEi
MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCmV52qzt3ifQk6ATYkhu4qlN4Z
B7yX3NUvRzWX/eLCqVOPN9b57GLTLo4bTETDPoh6P1KhM1gETAxgETWEElfD/yQA
pDGdISZUk0dLJ6OnElau9LNgz+MifVbw00T2fANB5R1NQMEBQCSGtpUwyQVozK9F
lgw2ncp9YpE/qHIAXaWGjtgDyZXBkS2QaaL0Zte4yc3oTTURism0JmKImm6tPcgb
Yz7iVfE+wzhDQGi7QCy9KrLYiGEsVZhaWE+2Z7JQ9SW5NpgI54wv/RnAaCTSRHtg
5ZE6GewWiw72dqX/F5D3WIUr/JrT1l2Ju2viAnjY89mMuov45V2T5pxzmwV3AgMB
AAGjggKhMIICnTALBgNVHQ8EBAMCBLAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsG
AQUFBwMBMB0GA1UdDgQWBBQ6gV5a0Eco8S28w1PYAm1JQuqRTjAfBgNVHSMEGDAW
gBTr2xFe+Ame2NZinP1ineOESijhJzCB7gYDVR0fBIHmMIHjMIHgoIHdoIHahk9o
dHRwOi8vbXNjcmwubWljcm9zb2Z0LmNvbS9wa2kvbXNjb3JwL2NybC9NU0lUJTIw
TWFjaGluZSUyMEF1dGglMjBDQSUyMDIoMSkuY3Jshk1odHRwOi8vY3JsLm1pY3Jv
c29mdC5jb20vcGtpL21zY29ycC9jcmwvTVNJVCUyME1hY2hpbmUlMjBBdXRoJTIw
Q0ElMjAyKDEpLmNybIY4aHR0cDovL2NvcnBwa2kvY3JsL01TSVQlMjBNYWNoaW5l
JTIwQXV0aCUyMENBJTIwMigxKS5jcmwwga0GCCsGAQUFBwEBBIGgMIGdMFUGCCsG
AQUFBzAChklodHRwOi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL21zY29ycC9NU0lU
JTIwTWFjaGluZSUyMEF1dGglMjBDQSUyMDIoMSkuY3J0MEQGCCsGAQUFBzAChjho
dHRwOi8vY29ycHBraS9haWEvTVNJVCUyME1hY2hpbmUlMjBBdXRoJTIwQ0ElMjAy
KDEpLmNydDA/BgkrBgEEAYI3FQcEMjAwBigrBgEEAYI3FQiDz4lNrfIChaGfDIL6
yn2B4ft0gU+Dwu2FCI6p0oVjAgFkAgEKMCcGCSsGAQQBgjcVCgQaMBgwCgYIKwYB
BQUHAwIwCgYIKwYBBQUHAwEwJAYDVR0RBB0wG4IZKi5yZWRpcy5jYWNoZS53aW5k
b3dzLm5ldDANBgkqhkiG9w0BAQUFAAOCAQEAoBVVFCOILyB07v7Crzn++5Y7hnlJ
0/K0GUC4JiGKRWrss7f9V3pz2dKIIxhjjTC6y1llH/FW+D4cpIuYd0bIP+UbtgOD
juOMu/0tipBc/ANx1NxUW8R5qG/oUbmibg6WYZ8A/tXpikZfnf8DY7P0ZUHv8eNr
pNLSPrBUCRXpZUIrcQtXvXoSR2nGKStn3UGuz9e9tSD9Pasouy3w6QaLfH/r8QXk
VJr8oF8ndNgSCqgOT21jWyzRP0jWkKMcmcTCXA0aXQOdfqu5h2XH+8vBiGTVO8yP
wvqqnK7ETSNqiRHEVEtQEDaV3GprVpM043MqEyJ2Ob1Z9SGbs99/XVi41Q==
-----END CERTIFICATE-----`

func initRedisConnection() {

	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(redisPEM))
	if !ok {
		panic("failed to parse root certificate")
	}

	redisPool = redis.NewPool(func() (redis.Conn, error) {
		//log.Println("Dial")
		c, err := redis.Dial("tcp", "nhsp.redis.cache.windows.net:6380", &tls.Config{
			RootCAs:            roots,
			InsecureSkipVerify: true,
		})

		if err != nil {
			log.Println("Could not CONNECT:", err)
			return nil, err
		}

		if _, err := c.Do("AUTH", "/9iL0DholzG+HDzdex7ScM8KDOqUWAJ1Ke/nBhHR0qM="); err != nil {
			log.Println("Could not AUTH: EEEEEEEEEEEEEEEEEEEEEE", err)
			c.Close()

			return nil, err
		}

		if _, err := c.Do("SELECT", "2"); err != nil {
			log.Println("Could not SELECT", err)
			c.Close()

			return nil, err
		}

		return c, err
	}, 10)
}
