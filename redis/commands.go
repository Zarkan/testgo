package redis

import (
	"bytes"
	"encoding/gob"
	"log"
	protobuff "nhsppoc/protobuffModel"
	//"time"
)

func sendGob(key string, gobValue []byte) {

	c := redisPool.Get()

	defer c.Close()

	// t0 := time.Now()
	_, err := c.Do("SETEX", key, 20, gobValue)
	// t1 := time.Now()
	if err != nil {
		log.Println("Could not SET: ", err)
		//message := fmt.Sprintf("Could not SET %s:%s", "key", "value")

	}
	// log.Printf("REDIS SET GOB: %v\n", t1.Sub(t0))

}

func getGob(key string) []byte {

	c := redisPool.Get()

	defer c.Close()

	// t0 := time.Now()
	status, err := c.Do("GET", key)
	// t1 := time.Now()
	if err != nil {
		log.Println("Could not GET: ", err)
		//message := fmt.Sprintf("Could not SET %s:%s", "key", "value")

	} else {
		//log.Println("STATUS: ", status)
	}
	// log.Printf("REDIS GET GOB: %v\n", t1.Sub(t0))
	switch v := status.(type) {
	case bytes.Buffer: // return as is
		return v.Bytes() // Here v is of type bytes.Buffer
	case []byte: // return as is
		return v
	}
	return nil

}

func encode(shifts *[]protobuff.Shift) {
	var encodeBuffer bytes.Buffer
	enc := gob.NewEncoder(&encodeBuffer)
	// Encode (send) some values.
	err := enc.Encode(shifts)
	if err != nil {
		log.Fatal("encode error:", err)
	}
	sendGob("test1", encodeBuffer.Bytes())

}

func decode() *[]protobuff.Shift {
	resp := getGob("test1")
	respBuffer := bytes.NewBuffer(resp)

	dec := gob.NewDecoder(respBuffer) // Will read from network.

	shifts := []protobuff.Shift{}
	dec.Decode(&shifts)
	return &shifts
}
