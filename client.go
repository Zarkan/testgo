package nhsppoc

import (
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/proto"
	"golang.org/x/net/websocket"
	"io"
	//	"io/ioutil"
	"gopkg.in/mgo.v2/bson"
	"log"
	protobuff "nhsppoc/protobuffModel"
)

const channelBufSize = 100

var maxId int = 0

type Action int
type MessageType int

const (
	ACTION_SEARCH Action = iota
	ACTION_FILTER
	ACTION_SORT
	ACTION_MORE
)
const (
	TYPE_SUMMARY MessageType = iota
	TYPE_SHIFT
)

type IncomingMessage struct {
	Action Action
	Params *json.RawMessage
}

type ResponseMessage struct {
	MessageType MessageType `json:"t"`
	Message     []byte      `json:"m"`
}

type SearchParams struct {
	LocationId string
	WardId     string
	DateRange  string
	StartDate  string
	EndDate    string
	/*	WebUserId   string
		CallerToken string*/
}

// Chat client.
type Client struct {
	id     int
	ws     *websocket.Conn
	server *Server
	ch     chan interface{}
	doneCh chan bool
}

// Create new chat client.
func NewClient(ws *websocket.Conn, server *Server) *Client {

	if ws == nil {
		panic("ws cannot be nil")
	}

	if server == nil {
		panic("server cannot be nil")
	}

	maxId++
	ch := make(chan interface{}, channelBufSize)
	doneCh := make(chan bool)

	return &Client{maxId, ws, server, ch, doneCh}
}

func (c *Client) Conn() *websocket.Conn {
	return c.ws
}

func (c *Client) Write(msg interface{}) {
	select {
	case c.ch <- msg:
	default:
		c.server.Del(c)
		err := fmt.Errorf("client %d is disconnected.", c.id)
		c.server.Err(err)
	}
}

func (c *Client) Done() {
	c.doneCh <- true
}

// Listen Write and Read request via chanel
func (c *Client) Listen() {
	go c.listenWrite()
	c.listenRead()
}

// Listen write request via chanel
func (c *Client) listenWrite() {
	log.Println("Listening write to client")
	for {

		select {

		// send message to the client
		case msg := <-c.ch:

			//var resp = ResponseMessage{}
			/*switch msg := msg.(type) {
			default:
				fmt.Printf("unexpected type %b\n", msg) // %T prints whatever type t has
			case *protobuff.DataSummary:
				resp.MessageType = TYPE_SUMMARY
				if resp.Message, err = proto.Marshal(msg); err != nil {
					log.Panicf("Can't encode PROTO SUMMARY", err.Error())
				}
			case *protobuff.Shift:
				resp.MessageType = TYPE_SHIFT
				if resp.Message, err = proto.Marshal(msg); err != nil {
					log.Panicf("Can't encode PROTO SHIFT", err.Error())
				}
			}*/
			/*var marshalledJSON []byte
			var err error
			if marshalledJSON, err = json.Marshal(msg); err != nil {
				log.Panicf("Can't encode BSON RESP %v", err.Error())
			}
			log.Println("Send:", string(marshalledJSON))*/
			//BSON.Send(c.ws, msg)
			websocket.JSON.Send(c.ws, &msg)

		// receive done request
		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true // for listenRead method
			return
		}
	}
}

// Listen read request via chanel
func (c *Client) listenRead() {
	log.Println("Listening read from client")
	for {
		select {

		// receive done request
		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true // for listenWrite method
			return

		// read data from websocket connection
		default:
			var incMessage IncomingMessage
			//log.Printf("INC MESSAG1E\n")
			/*b, err := ioutil.ReadAll(c.ws)
			if err != nil {
				log.Fatal(err)
			}*/

			err := websocket.JSON.Receive(c.ws, &incMessage)
			if err == io.EOF {
				c.doneCh <- true
			} else if err != nil {
				c.server.Err(err)
			} else {
				log.Printf("Received %v %v\n", incMessage.Action, string(*incMessage.Params))
				switch incMessage.Action {
				case 0:
					var params SearchParams
					if err := json.Unmarshal(*incMessage.Params, &params); err != nil {
						log.Panicf("error: %v, json= %v\n", err.Error(), string(*incMessage.Params))
					}
					// log.Printf("Received PARAM %v\n", params)
					c.HandleSearch(SearchForShifts())
				}

			}
		}
	}
}

func (c *Client) HandleSearch(ds *SearchDataSummary) {
	status := &protobuff.DataSummary{}
	status.TotalNumber = &ds.TotalNumber
	status.PartsNumber = &ds.PartsNumber
	status.TabId = &ds.TabId
	status.RedisKey = &ds.RedisKey
	//c.Write(status)
	for shift := range ds.DataChannel {
		c.Write(shift)

	}
}

func protoMarshal(v interface{}) (msg []byte, payloadType byte, err error) {

	vv := v.(proto.Message)
	msg, err = proto.Marshal(vv)
	return msg, 2, err
}

func protoUnmarshal(msg []byte, payloadType byte, v interface{}) (err error) {
	vv := v.(proto.Message)
	return proto.Unmarshal(msg, vv)
}
func bsonMarshal(v interface{}) (msg []byte, payloadType byte, err error) {

	// vv := v.(proto.Message)
	msg, err = bson.Marshal(v)
	return msg, 2, err
}

func bsonUnmarshal(msg []byte, payloadType byte, v interface{}) (err error) {
	// vv := v.(proto.Message)
	return bson.Unmarshal(msg, v)
}

var PROTO = websocket.Codec{protoMarshal, protoUnmarshal}
var BSON = websocket.Codec{bsonMarshal, bsonUnmarshal}
