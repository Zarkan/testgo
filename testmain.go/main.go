package main

import (
	"log"
	"net/http"
	"nhsppoc"
	wssbclient "nhsppoc/nhspInterfaces"
	"os"
)

func main() {
	wssbclient.InitClient()
	log.Println("Start!")
	//fs := http.FileServer(http.Dir("./webroot"))
	log.SetFlags(log.Lshortfile)

	// websocket server
	server := nhsppoc.NewServer("/ws")
	go server.Listen()
	server.GetHandler()
	// static files
	/*http.Handle("/lib/", fs)
	http.HandleFunc("/", DefaultHandler)
	*/
	port := "3001"
	if os.Getenv("HTTP_PLATFORM_PORT") != "" {
		port = os.Getenv("HTTP_PLATFORM_PORT")
	}
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
	//log.Fatal(http.ListenAndServe(":8080", nil))
}
