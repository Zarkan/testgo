package wsclient

import (
	model "nhsppoc/nhspInterfaces/wsdlgo"
	"testing"
)

func TestConnection(t *testing.T) {
	InitClient()
	reply, err1 := Wsclient.AuthenticateCaller(&model.AuthenticateCaller{RequestId: 55, Username: "Trust:Bank", Password: "Warszawa*0900"})
	if reply.CallerToken == "" || err1 != nil {
		t.Error("Expected CallerToken, got ", reply, err1)
	}

	reply2, err2 := Wsclient.AuthenticateUser(&model.AuthenticateUser{RequestId: 55, UserName: "SAdminNHSP", Password: "Password1", CallerToken: reply.CallerToken})
	if reply2.WebUserID == "" || err2 != nil {
		t.Error("Expected WebUserID, got ", reply2, err2)
	}

	reply3, err3 := Wsclient.BoardView(&model.BoardView{RequestId: 55, LocationId: "All", WardId: "All", CallerToken: reply.CallerToken, WebUserId: reply2.WebUserID})
	if err3 != nil {
		t.Error("reply3.Shifts.IVRShiftType, got ", reply3, err3)
	}
	/*	type msgT struct{ A, B string }
		type envT struct{ Body struct{ Message msgT } }
		echo := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method != "POST" {
				http.NotFound(w, r)
				return
			}
			if v := r.Header.Get("X-Test"); v != "true" {
				http.NotFound(w, r)
				return
			}
			io.Copy(w, r.Body)
		})
		s := httptest.NewServer(echo)
		defer s.Close()
		pre := func(r *http.Request) { r.Header.Set("X-Test", "true") }
		cases := []struct {
			C       *Client
			In, Out Message
			Fail    bool
		}{
			{
				C:   &Client{URL: s.URL, Pre: pre},
				In:  &msgT{A: "model", B: "world"},
				Out: &envT{},
			},
			{
				C:   &Client{URL: s.URL, Pre: pre},
				In:  &msgT{A: "foo", B: "bar"},
				Out: &envT{},
			},
			{
				C:    &Client{URL: "", Pre: pre},
				Out:  &envT{},
				Fail: true,
			},
		}
		for i, tc := range cases {
			err := tc.C.RoundTrip(tc.In, tc.Out)
			if err != nil && !tc.Fail {
				t.Errorf("test %d: %v", i, err)
				continue
			}
			if tc.Fail {
				continue
			}
			env, ok := tc.Out.(*envT)
			if !ok {
				t.Errorf("test %d: response to %#v is not an envelope", i, tc.In)
				continue
			}
			if !reflect.DeepEqual(env.Body.Message, *tc.In.(*msgT)) {
				t.Errorf("test %d: message mismatch\nwant: %#v\nhave: %#v",
					i, tc.In, &env.Body.Message)
				continue
			}
		}*/
}
