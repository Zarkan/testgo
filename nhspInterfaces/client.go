package wsclient

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
	model "nhsppoc/nhspInterfaces/wsdlgo"

	"github.com/fiorix/wsdl2go/soap"
)

var Wsclient model.IClient

func InitClient() {
	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(nhspInterfacePEM))
	if !ok {
		panic("failed to parse root certificate")
	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs:            roots,
			InsecureSkipVerify: true,
		},
	}
	client := &http.Client{Transport: tr}
	cli := soap.Client{
		URL:       "https://uat1nhspinterface.nhsp.co.uk/Client.svc",
		Namespace: model.Namespace,
		Config:    client,
	}
	Wsclient = model.NewIClient(&cli)

}

const nhspInterfacePEM = `
-----BEGIN CERTIFICATE-----
MIIGBDCCBOygAwIBAgIQBW13/xRk/Jmt4XAgOffcGDANBgkqhkiG9w0BAQUFADCB
tTELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL
ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTswOQYDVQQLEzJUZXJtcyBvZiB1c2Ug
YXQgaHR0cHM6Ly93d3cudmVyaXNpZ24uY29tL3JwYSAoYykxMDEvMC0GA1UEAxMm
VmVyaVNpZ24gQ2xhc3MgMyBTZWN1cmUgU2VydmVyIENBIC0gRzMwHhcNMTExMjI5
MDAwMDAwWhcNMTYxMjI3MjM1OTU5WjCB7jELMAkGA1UEBhMCR0IxFjAUBgNVBAgU
DUhlcnRmb3Jkc2hpcmUxEDAOBgNVBAcUB1dhdGZvcmQxGjAYBgNVBAoUEU5IUyBQ
cm9mZXNzaW9uYWxzMTUwMwYDVQQLFCxUZXJtcyBvZiB1c2UgYXQgd3d3LnZlcmlz
aWduLmNvLnVrL3JwYSAoYykwNTEiMCAGA1UECxMZQXV0aGVudGljYXRlZCBieSBW
ZXJpU2lnbjEnMCUGA1UECxMeTWVtYmVyLCBWZXJpU2lnbiBUcnVzdCBOZXR3b3Jr
MRUwEwYDVQQDFAwqLm5oc3AuY28udWswggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
ggEKAoIBAQDDQten6KEwxWkzvNoAJ5qGyCBjTFITDWewfnl0G5r7saQ1bBOTl0ZU
ctLETxnsp8g3NABMOwcU+ieWAd2/rtfez2aFz/gvA4gcq3aqDRpz1kIO24NQyLS0
OU7lioI8+qMZS9nt2SgEI+IcyYG2Y1fQ2AVwWi2ZTe1u/sS/g09xkLx5E6/otpRX
1CNG1/j3g8z3y15jRzMqSoYtVvZpwogrPi1f7vF7kLKgj2hp7NshQ6GoFf0sg/mB
C5k/Y1Zy8Vejx2/hmJ3V7icPyn/RK9fCIWM+q4kA8Q0SGb93t0LNrEL03bETGWIg
RyWX/Qz91mH9moFNfKmfrRrNvEA5v/DrAgMBAAGjggHTMIIBzzAJBgNVHRMEAjAA
MAsGA1UdDwQEAwIFoDBFBgNVHR8EPjA8MDqgOKA2hjRodHRwOi8vU1ZSU2VjdXJl
LUczLWNybC52ZXJpc2lnbi5jb20vU1ZSU2VjdXJlRzMuY3JsMEYGA1UdIAQ/MD0w
OwYLYIZIAYb4RQEHFwMwLDAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cudmVyaXNp
Z24uY28udWsvcnBhMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAfBgNV
HSMEGDAWgBQNRFwWU0TBgn4dIKsl9AFj2L55pTB2BggrBgEFBQcBAQRqMGgwJAYI
KwYBBQUHMAGGGGh0dHA6Ly9vY3NwLnZlcmlzaWduLmNvbTBABggrBgEFBQcwAoY0
aHR0cDovL1NWUlNlY3VyZS1HMy1haWEudmVyaXNpZ24uY29tL1NWUlNlY3VyZUcz
LmNlcjBuBggrBgEFBQcBDARiMGChXqBcMFowWDBWFglpbWFnZS9naWYwITAfMAcG
BSsOAwIaBBRLa7kolgYMu9BSOJsprEsHiyEFGDAmFiRodHRwOi8vbG9nby52ZXJp
c2lnbi5jb20vdnNsb2dvMS5naWYwDQYJKoZIhvcNAQEFBQADggEBAFvjsKXJkdfx
QlN8Z3RS7zjAPgc1m9WPn05UKMEoc4cyhyvYoCTm2nYn9sfgBtPr+hUjmgjKyCid
pF8uFplFgxxIhIL1h4cN+RvioS4t93DzNu2dckuKEt9LUKUfo9rL4Wb5N2mAVJdS
n4l3ihq5AZYqgkG8ZpSq5VrplNeafmmLqJ4P+U+buIJgj0DzbmWxL1PrPMuN7P/s
ZPBwUxvO/DHyuj1DuVipORSlrdE/jaFybzEvRKRhfPjT/deKstjmIMHgvMWWa8RH
fL4o62EHBGGMlsh0ecwKTWLLCAFDr7wXs97k+d8zr7KBPjiSHZT8suxznTZVf4uU
CakTp9/J/YI=
-----END CERTIFICATE-----`
