package wsmodel

import (
	"encoding/xml"
	"reflect"

	"github.com/fiorix/wsdl2go/soap"
)

// Namespace was auto-generated from WSDL.
var Namespace = "http://tempuri.org/"

// NewIClient creates an initializes a IClient.
func NewIClient(cli *soap.Client) IClient {
	return &iClient{cli}
}

// IClient was auto-generated from WSDL
// and defines interface for the remote service. Useful for testing.
type IClient interface {
	// AuthenticateCaller was auto-generated from WSDL.
	AuthenticateCaller(α *AuthenticateCaller) (β *AuthenticateCallerResponse, err error)

	// AcknowledgePushNotification was auto-generated from WSDL.
	AcknowledgePushNotification(α *AcknowledgePushNotification) (β *AcknowledgePushNotificationResponse, err error)

	// AgencyRatesContract was auto-generated from WSDL.
	AgencyRatesContract(α *AgencyRatesContract) (β *AgencyRatesContractResponse, err error)

	// AgencyRequest was auto-generated from WSDL.
	AgencyRequest(α *AgencyRequest) (β *AgencyRequestResponse, err error)

	// AgencyStaffRequest was auto-generated from WSDL.
	AgencyStaffRequest(α *AgencyStaffRequest) (β *AgencyStaffRequestResponse, err error)

	// AgencyUserRequest was auto-generated from WSDL.
	AgencyUserRequest(α *AgencyUserRequest) (β *AgencyUserRequestResponse, err error)

	// AmendTimeSheet was auto-generated from WSDL.
	AmendTimeSheet(α *AmendTimeSheet) (β *AmendTimeSheetResponse, err error)

	// AssignmentCodesRequest was auto-generated from WSDL.
	AssignmentCodesRequest(α *AssignmentCodesRequest) (β *AssignmentCodesRequestResponse, err error)

	// AuthenticateUser was auto-generated from WSDL.
	AuthenticateUser(α *AuthenticateUser) (β *AuthenticateUserResponse, err error)

	// AvailableShifts was auto-generated from WSDL.
	AvailableShifts(α *AvailableShifts) (β *AvailableShiftsResponse, err error)

	// AvailableShifts_AdvancedSearch was auto-generated from WSDL.
	AvailableShifts_AdvancedSearch(α *AvailableShifts_AdvancedSearch) (β *AvailableShifts_AdvancedSearchResponse, err error)

	// BookStaffRequest was auto-generated from WSDL.
	BookStaffRequest(α *BookStaffRequest) (β *BookStaffRequestResponse, err error)

	// CancelBookingRequest was auto-generated from WSDL.
	CancelBookingRequest(α *CancelBookingRequest) (β *CancelBookingRequestResponse, err error)

	// CompletePerformanceEvaluation was auto-generated from WSDL.
	CompletePerformanceEvaluation(α *CompletePerformanceEvaluation) (β *CompletePerformanceEvaluationResponse, err error)

	// CreateShiftRequest was auto-generated from WSDL.
	CreateShiftRequest(α *CreateShiftRequest) (β *CreateShiftRequestResponse, err error)

	// DeleteShiftRequest was auto-generated from WSDL.
	DeleteShiftRequest(α *DeleteShiftRequest) (β *DeleteShiftRequestResponse, err error)

	// ExactShiftRequest was auto-generated from WSDL.
	ExactShiftRequest(α *ExactShiftRequest) (β *ExactShiftRequestResponse, err error)

	// ExactStaffRequest was auto-generated from WSDL.
	ExactStaffRequest(α *ExactStaffRequest) (β *ExactStaffRequestResponse, err error)

	// ExactUserRequest was auto-generated from WSDL.
	ExactUserRequest(α *ExactUserRequest) (β *ExactUserRequestResponse, err error)

	// ForwardPerformanceEvaluation was auto-generated from WSDL.
	ForwardPerformanceEvaluation(α *ForwardPerformanceEvaluation) (β *ForwardPerformanceEvaluationResponse, err error)

	// GetAgencies was auto-generated from WSDL.
	GetAgencies(α *GetAgencies) (β *GetAgenciesResponse, err error)

	// GetAgenciesUsedByTrust was auto-generated from WSDL.
	GetAgenciesUsedByTrust(α *GetAgenciesUsedByTrust) (β *GetAgenciesUsedByTrustResponse, err error)

	// GetAgencyRateContracts was auto-generated from WSDL.
	GetAgencyRateContracts(α *GetAgencyRateContracts) (β *GetAgencyRateContractsResponse, err error)

	// GetAvailableShifts was auto-generated from WSDL.
	GetAvailableShifts(α *GetAvailableShifts) (β *GetAvailableShiftsResponse, err error)

	// GetBackDatedShifts was auto-generated from WSDL.
	GetBackDatedShifts(α *GetBackDatedShifts) (β *GetBackDatedShiftsResponse, err error)

	// GetBackingReport was auto-generated from WSDL.
	GetBackingReport(α *GetBackingReport) (β *GetBackingReportResponse, err error)

	// GetContractInformation was auto-generated from WSDL.
	GetContractInformation(α *GetContractInformation) (β *GetContractInformationResponse, err error)

	// GetInvoiceBackingReportsList was auto-generated from WSDL.
	GetInvoiceBackingReportsList(α *GetInvoiceBackingReportsList) (β *GetInvoiceBackingReportsListResponse, err error)

	// GetInvoiceDetails was auto-generated from WSDL.
	GetInvoiceDetails(α *GetInvoiceDetails) (β *GetInvoiceDetailsResponse, err error)

	// GetInvoiceDetailsReportPdf was auto-generated from WSDL.
	GetInvoiceDetailsReportPdf(α *GetInvoiceDetailsReportPdf) (β *GetInvoiceDetailsReportPdfResponse, err error)

	// GetInvoiceSummaryReportPdf was auto-generated from WSDL.
	GetInvoiceSummaryReportPdf(α *GetInvoiceSummaryReportPdf) (β *GetInvoiceSummaryReportPdfResponse, err error)

	// GetNotifications was auto-generated from WSDL.
	GetNotifications(α *GetNotifications) (β *GetNotificationsResponse, err error)

	// GetPerformanceEvaluation was auto-generated from WSDL.
	GetPerformanceEvaluation(α *GetPerformanceEvaluation) (β *GetPerformanceEvaluationResponse, err error)

	// GetPerformanceEvaluationReferral was auto-generated from WSDL.
	GetPerformanceEvaluationReferral(α *GetPerformanceEvaluationReferral) (β *GetPerformanceEvaluationReferralResponse, err error)

	// GetPersonalisedRatesPermission was auto-generated from WSDL.
	GetPersonalisedRatesPermission(α *GetPersonalisedRatesPermission) (β *GetPersonalisedRatesPermissionResponse, err error)

	// GetPriorQualifications was auto-generated from WSDL.
	GetPriorQualifications(α *GetPriorQualifications) (β *GetPriorQualificationsResponse, err error)

	// GetPushNotifications was auto-generated from WSDL.
	GetPushNotifications(α *GetPushNotifications) (β *GetPushNotificationsResponse, err error)

	// GetSelfBillingAgreements was auto-generated from WSDL.
	GetSelfBillingAgreements(α *GetSelfBillingAgreements) (β *GetSelfBillingAgreementsResponse, err error)

	// GetShiftsSince was auto-generated from WSDL.
	GetShiftsSince(α *GetShiftsSince) (β *GetShiftsSinceResponse, err error)

	// GetTrusts was auto-generated from WSDL.
	GetTrusts(α *GetTrusts) (β *GetTrustsResponse, err error)

	// GetUserInfoForTimeSheetModification was auto-generated from
	// WSDL.
	GetUserInfoForTimeSheetModification(α *GetUserInfoForTimeSheetModification) (β *GetUserInfoForTimeSheetModificationResponse, err error)

	// GetWebUserIdOfWardUsers was auto-generated from WSDL.
	GetWebUserIdOfWardUsers(α *GetWebUserIdOfWardUsers) (β *GetWebUserIdOfWardUsersResponse, err error)

	// ModifyContractStatus was auto-generated from WSDL.
	ModifyContractStatus(α *ModifyContractStatus) (β *ModifyContractStatusResponse, err error)

	// ModifyPersonalisedRatesPermission was auto-generated from WSDL.
	ModifyPersonalisedRatesPermission(α *ModifyPersonalisedRatesPermission) (β *ModifyPersonalisedRatesPermissionResponse, err error)

	// ModifyPriorQualification was auto-generated from WSDL.
	ModifyPriorQualification(α *ModifyPriorQualification) (β *ModifyPriorQualificationResponse, err error)

	// ModifySelfBillingAgreement was auto-generated from WSDL.
	ModifySelfBillingAgreement(α *ModifySelfBillingAgreement) (β *ModifySelfBillingAgreementResponse, err error)

	// ModifyShiftRequest was auto-generated from WSDL.
	ModifyShiftRequest(α *ModifyShiftRequest) (β *ModifyShiftRequestResponse, err error)

	// ModifyTimeSheets was auto-generated from WSDL.
	ModifyTimeSheets(α *ModifyTimeSheets) (β *ModifyTimeSheetsResponse, err error)

	// QueryTimeSheet was auto-generated from WSDL.
	QueryTimeSheet(α *QueryTimeSheet) (β *QueryTimeSheetResponse, err error)

	// SearchFlexibleWorker was auto-generated from WSDL.
	SearchFlexibleWorker(α *SearchFlexibleWorker) (β *SearchFlexibleWorkerResponse, err error)

	// SearchWorker was auto-generated from WSDL.
	SearchWorker(α *SearchWorker) (β *SearchWorkerResponse, err error)

	// ShiftsRequest was auto-generated from WSDL.
	ShiftsRequest(α *ShiftsRequest) (β *ShiftsRequestResponse, err error)

	// SplitShift was auto-generated from WSDL.
	SplitShift(α *SplitShift) (β *SplitShiftResponse, err error)

	// StaffRequest was auto-generated from WSDL.
	StaffRequest(α *StaffRequest) (β *StaffRequestResponse, err error)

	// TimeSheets was auto-generated from WSDL.
	TimeSheets(α *TimeSheets) (β *TimeSheetsResponse, err error)

	// TwoTierAuthorisation was auto-generated from WSDL.
	TwoTierAuthorisation(α *TwoTierAuthorisation) (β *TwoTierAuthorisationResponse, err error)

	// TwoTierAuthorisationShiftStatus was auto-generated from WSDL.
	TwoTierAuthorisationShiftStatus(α *TwoTierAuthorisationShiftStatus) (β *TwoTierAuthorisationShiftStatusResponse, err error)

	// UnAvailableReasonCodesInformation was auto-generated from WSDL.
	UnAvailableReasonCodesInformation(α *UnAvailableReasonCodesInformation) (β *UnAvailableReasonCodesInformationResponse, err error)

	// UnitChildrenRequest was auto-generated from WSDL.
	UnitChildrenRequest(α *UnitChildrenRequest) (β *UnitChildrenRequestResponse, err error)

	// WardInduction was auto-generated from WSDL.
	WardInduction(α *WardInduction) (β *WardInductionResponse, err error)

	// WardUserRequest was auto-generated from WSDL.
	WardUserRequest(α *WardUserRequest) (β *WardUserRequestResponse, err error)

	// GetCancellationReasons was auto-generated from WSDL.
	GetCancellationReasons(α *GetCancellationReasons) (β *GetCancellationReasonsResponse, err error)

	// GetJobCodesForTrust was auto-generated from WSDL.
	GetJobCodesForTrust(α *GetJobCodesForTrust) (β *GetJobCodesForTrustResponse, err error)

	// GetModifyTimeSheetReasons was auto-generated from WSDL.
	GetModifyTimeSheetReasons(α *GetModifyTimeSheetReasons) (β *GetModifyTimeSheetReasonsResponse, err error)

	// GetQueriedTimeSheets was auto-generated from WSDL.
	GetQueriedTimeSheets(α *GetQueriedTimeSheets) (β *GetQueriedTimeSheetsResponse, err error)

	// GetRangeRequestDetails was auto-generated from WSDL.
	GetRangeRequestDetails(α *GetRangeRequestDetails) (β *GetRangeRequestDetailsResponse, err error)

	// GetTrustStaffGroupAuthorisationCodeSetting was auto-generated
	// from WSDL.
	GetTrustStaffGroupAuthorisationCodeSetting(α *GetTrustStaffGroupAuthorisationCodeSetting) (β *GetTrustStaffGroupAuthorisationCodeSettingResponse, err error)

	// BoardView was auto-generated from WSDL.
	BoardView(α *BoardView) (β *BoardViewResponse, err error)

	// GetSurfaceInformation was auto-generated from WSDL.
	GetSurfaceInformation(α *GetSurfaceInformation) (β *GetSurfaceInformationResponse, err error)

	// GetAwaitingAuthorisationTimesheets was auto-generated from WSDL.
	GetAwaitingAuthorisationTimesheets(α *GetAwaitingAuthorisationTimesheets) (β *GetAwaitingAuthorisationTimesheetsResponse, err error)

	// SuggestedWorkerRequest was auto-generated from WSDL.
	SuggestedWorkerRequest(α *SuggestedWorkerRequest) (β *SuggestedWorkerRequestResponse, err error)
}

// DateTime in WSDL format.
type DateTime string
type Decimal int

// Action was auto-generated from WSDL.
type Action string

// Validate validates Action.
func (v Action) Validate() bool {
	for _, vv := range []string{
		"authorise",
		"Reject",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// AgencyRateCardsActionStatus was auto-generated from WSDL.
type AgencyRateCardsActionStatus string

// Validate validates AgencyRateCardsActionStatus.
func (v AgencyRateCardsActionStatus) Validate() bool {
	for _, vv := range []string{
		"Authorize",
		"Query",
		"ReAuthorize",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// AgencyRateCardsStatus was auto-generated from WSDL.
type AgencyRateCardsStatus string

// Validate validates AgencyRateCardsStatus.
func (v AgencyRateCardsStatus) Validate() bool {
	for _, vv := range []string{
		"All",
		"Unauthorized",
		"Queried",
		"Authorized",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// BoardviewDateRangeEnum was auto-generated from WSDL.
type BoardviewDateRangeEnum string

// Validate validates BoardviewDateRangeEnum.
func (v BoardviewDateRangeEnum) Validate() bool {
	for _, vv := range []string{
		"Today",
		"Day",
		"Tomorrow",
		"Thisweek",
		"Thismonth",
		"Nextweek",
		"Nextmonth",
		"Next28days",
		"Next7days",
		"Next24hours",
		"Prevweek",
		"Prevmonth",
		"Yesterday",
		"CustomDateRange",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// CancelOptionsEnum was auto-generated from WSDL.
type CancelOptionsEnum string

// Validate validates CancelOptionsEnum.
func (v CancelOptionsEnum) Validate() bool {
	for _, vv := range []string{
		"WardUnitcancelledFlexibleWorker",
		"FlexibleWorkerCancelledshift",
		"FlexibleWorker_DNA",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// DNASNC was auto-generated from WSDL.
type DNASNC string

// Validate validates DNASNC.
func (v DNASNC) Validate() bool {
	for _, vv := range []string{
		"DNA",
		"SNC",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// DateRange was auto-generated from WSDL.
type DateRange string

// Validate validates DateRange.
func (v DateRange) Validate() bool {
	for _, vv := range []string{
		"Today",
		"Yesterday",
		"PreviousWeek",
		"PreviousMonth",
		"CustomDateRange",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// Date_x0020_Range was auto-generated from WSDL.
type Date_x0020_Range string

// Validate validates Date_x0020_Range.
func (v Date_x0020_Range) Validate() bool {
	for _, vv := range []string{
		"Today",
		"Day",
		"Tomorrow",
		"Thisweek",
		"Thismonth",
		"Nextweek",
		"Nextmonth",
		"Next28days",
		"Next7days",
		"Next24hours",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// EmploymentStatusEnum was auto-generated from WSDL.
type EmploymentStatusEnum string

// Validate validates EmploymentStatusEnum.
func (v EmploymentStatusEnum) Validate() bool {
	for _, vv := range []string{
		"Bank",
		"Trust",
		"BankTA",
		"BankImpl",
		"BankTUPE",
		"Substantive",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// EngineCapacity was auto-generated from WSDL.
type EngineCapacity string

// Validate validates EngineCapacity.
func (v EngineCapacity) Validate() bool {
	for _, vv := range []string{
		"None",
		"EngineCapacity_500To1000",
		"EngineCapacity_1001To1500",
		"EngineCapacity_1501To2000",
		"EngineCapacity_Over2000",
		"EngineCapacity_UpTo125",
		"EngineCapacity_Over125",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// GenderEnum was auto-generated from WSDL.
type GenderEnum string

// Validate validates GenderEnum.
func (v GenderEnum) Validate() bool {
	for _, vv := range []string{
		"M",
		"F",
		"NA",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// HeaderStatus was auto-generated from WSDL.
type HeaderStatus string

// Validate validates HeaderStatus.
func (v HeaderStatus) Validate() bool {
	for _, vv := range []string{
		"Paid",
		"Unpaid",
		"Disputed",
		"Deleted",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// HolidayBookingStatus was auto-generated from WSDL.
type HolidayBookingStatus string

// Validate validates HolidayBookingStatus.
func (v HolidayBookingStatus) Validate() bool {
	for _, vv := range []string{
		"booked",
		"authorised",
		"released",
		"queried",
		"paid",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// IVRDateRangeEnum was auto-generated from WSDL.
type IVRDateRangeEnum string

// Validate validates IVRDateRangeEnum.
func (v IVRDateRangeEnum) Validate() bool {
	for _, vv := range []string{
		"Today",
		"Day",
		"Tomorrow",
		"Thisweek",
		"Thismonth",
		"Nextweek",
		"Nextmonth",
		"Next28days",
		"Next7days",
		"Next24hours",
		"Prevweek",
		"Prevmonth",
		"Yesterday",
		"CustomDateRange",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// InductionStatus was auto-generated from WSDL.
type InductionStatus string

// Validate validates InductionStatus.
func (v InductionStatus) Validate() bool {
	for _, vv := range []string{
		"NotRequired",
		"Required",
		"Warning",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// Miles was auto-generated from WSDL.
type Miles string

// Validate validates Miles.
func (v Miles) Validate() bool {
	for _, vv := range []string{
		"None",
		"Miles_UpTo3500Miles",
		"Miles_3500To9000Miles",
		"Miles_9001To15000Miles",
		"Miles_Thereafter",
		"Miles_UpTo5000Miles",
		"Miles_Over5000Miles",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ModifyPersonalisedRatePermissionStatus was auto-generated from
// WSDL.
type ModifyPersonalisedRatePermissionStatus string

// Validate validates ModifyPersonalisedRatePermissionStatus.
func (v ModifyPersonalisedRatePermissionStatus) Validate() bool {
	for _, vv := range []string{
		"Approve",
		"Reject",
		"Query",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// PersonalisedRatesRequestStatusEnum was auto-generated from WSDL.
type PersonalisedRatesRequestStatusEnum string

// Validate validates PersonalisedRatesRequestStatusEnum.
func (v PersonalisedRatesRequestStatusEnum) Validate() bool {
	for _, vv := range []string{
		"All",
		"Submitted",
		"Queried",
		"Rejected",
		"Approved",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// PersonalisedRatesRequest_Status was auto-generated from WSDL.
type PersonalisedRatesRequest_Status string

// Validate validates PersonalisedRatesRequest_Status.
func (v PersonalisedRatesRequest_Status) Validate() bool {
	for _, vv := range []string{
		"UnKnown",
		"Submitted",
		"Queried",
		"Rejected",
		"Approved",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// PriorQualificationStatusAction was auto-generated from WSDL.
type PriorQualificationStatusAction string

// Validate validates PriorQualificationStatusAction.
func (v PriorQualificationStatusAction) Validate() bool {
	for _, vv := range []string{
		"All",
		"Submitted",
		"Queried",
		"Rejected",
		"Approved",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// PriorQualificationsStatus was auto-generated from WSDL.
type PriorQualificationsStatus string

// Validate validates PriorQualificationsStatus.
func (v PriorQualificationsStatus) Validate() bool {
	for _, vv := range []string{
		"Query",
		"Reject",
		"Approve",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// RangeRequestFillOptions was auto-generated from WSDL.
type RangeRequestFillOptions string

// Validate validates RangeRequestFillOptions.
func (v RangeRequestFillOptions) Validate() bool {
	for _, vv := range []string{
		"JustThisShift",
		"AllUnFilledShiftsInThisRange",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ReasonOptionsEnum was auto-generated from WSDL.
type ReasonOptionsEnum string

// Validate validates ReasonOptionsEnum.
func (v ReasonOptionsEnum) Validate() bool {
	for _, vv := range []string{
		"ReasonsForDeletionofBookings",
		"ReasonsForRefusalofBookings",
		"ReasonsForRefusalofBookingsAgency",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ReasonTypesForModifyingShiftAfterRelease was auto-generated
// from WSDL.
type ReasonTypesForModifyingShiftAfterRelease string

// Validate validates ReasonTypesForModifyingShiftAfterRelease.
func (v ReasonTypesForModifyingShiftAfterRelease) Validate() bool {
	for _, vv := range []string{
		"FlexibleWorkerHoursIncorrect",
		"ShiftNotWorkedBySpecifiedFlexibleWorker",
		"WrongFlexibleWorkerInShift",
		"AmendDate",
		"WrongAssignmentCodeBooked",
		"PayrollCalculationAmendment",
		"IncorrectShiftType",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// RequestMethodEnum was auto-generated from WSDL.
type RequestMethodEnum string

// Validate validates RequestMethodEnum.
func (v RequestMethodEnum) Validate() bool {
	for _, vv := range []string{
		"IVR",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// SelfBillingStatus was auto-generated from WSDL.
type SelfBillingStatus string

// Validate validates SelfBillingStatus.
func (v SelfBillingStatus) Validate() bool {
	for _, vv := range []string{
		"Unauthorised",
		"Authorised",
		"Expired",
		"Cancelled",
		"Deactivated",
		"Rejected",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftAuthorisationStatus was auto-generated from WSDL.
type ShiftAuthorisationStatus string

// Validate validates ShiftAuthorisationStatus.
func (v ShiftAuthorisationStatus) Validate() bool {
	for _, vv := range []string{
		"Pending",
		"Authorised",
		"Cancelled",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftStatusEnum was auto-generated from WSDL.
type ShiftStatusEnum string

// Validate validates ShiftStatusEnum.
func (v ShiftStatusEnum) Validate() bool {
	for _, vv := range []string{
		"AgencyUninformed",
		"AgencyInformed",
		"AgencyUnfilled",
		"BankUninformed",
		"BankInformed",
		"BankUnconfirmed",
		"BankUnfilled",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftTimeType was auto-generated from WSDL.
type ShiftTimeType string

// Validate validates ShiftTimeType.
func (v ShiftTimeType) Validate() bool {
	for _, vv := range []string{
		"Early",
		"Late",
		"Night",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftType was auto-generated from WSDL.
type ShiftType string

// Validate validates ShiftType.
func (v ShiftType) Validate() bool {
	for _, vv := range []string{
		"Standard",
		"OnCall",
		"SleepIn",
		"ProgrammedActivity",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftTypeEnumWithAll was auto-generated from WSDL.
type ShiftTypeEnumWithAll string

// Validate validates ShiftTypeEnumWithAll.
func (v ShiftTypeEnumWithAll) Validate() bool {
	for _, vv := range []string{
		"All",
		"Standard",
		"OnCall",
		"SleepIn",
		"ProgrammedActivity",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// Shift_x0020_Time_x0020_Enum was auto-generated from WSDL.
type Shift_x0020_Time_x0020_Enum string

// Validate validates Shift_x0020_Time_x0020_Enum.
func (v Shift_x0020_Time_x0020_Enum) Validate() bool {
	for _, vv := range []string{
		"Day",
		"Night",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ShiftsSinceBookingStatus was auto-generated from WSDL.
type ShiftsSinceBookingStatus string

// Validate validates ShiftsSinceBookingStatus.
func (v ShiftsSinceBookingStatus) Validate() bool {
	for _, vv := range []string{
		"booked",
		"approved",
		"released",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// TimeSheetStatus was auto-generated from WSDL.
type TimeSheetStatus string

// Validate validates TimeSheetStatus.
func (v TimeSheetStatus) Validate() bool {
	for _, vv := range []string{
		"UnVerified",
		"Verified",
		"NotAuthorised",
		"AwaitingRelease",
		"Released",
		"ProcessedForPayment",
		"Queried",
		"ALL",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// VehicleType was auto-generated from WSDL.
type VehicleType string

// Validate validates VehicleType.
func (v VehicleType) Validate() bool {
	for _, vv := range []string{
		"MotorCarsWithThreeOrFourWheels",
		"OtherMotorVehicles",
		"PassengerAllowance",
		"PedalCycles",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// ViewNotesEnum was auto-generated from WSDL.
type ViewNotesEnum string

// Validate validates ViewNotesEnum.
func (v ViewNotesEnum) Validate() bool {
	for _, vv := range []string{
		"PO",
		"FW",
		"AU",
		"All",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// WarningLevel was auto-generated from WSDL.
type WarningLevel string

// Validate validates WarningLevel.
func (v WarningLevel) Validate() bool {
	for _, vv := range []string{
		"LEVEL_1",
		"LEVEL_2",
		"LEVEL_3",
		"LEVEL_4",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// WebUserType was auto-generated from WSDL.
type WebUserType string

// Validate validates WebUserType.
func (v WebUserType) Validate() bool {
	for _, vv := range []string{
		"WardManager",
		"WardClerk",
		"StaffBankMember",
		"SeniorAdmin",
		"AgencyRatesApprover",
	} {
		if reflect.DeepEqual(v, vv) {
			return true
		}
	}
	return false
}

// Char was auto-generated from WSDL.
type char int

// Duration was auto-generated from WSDL.
type Duration string

// Guid was auto-generated from WSDL.
type guid string

// AcknowledgePushNotification was auto-generated from WSDL.
type AcknowledgePushNotification struct {
	XMLName            xml.Name `xml:"http://tempuri.org/ AcknowledgePushNotification" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AcknowledgePushNotification"`
	RequestId          int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	PushNotificationID int      `xml:"PushNotificationID,omitempty" json:"PushNotificationID,omitempty" yaml:"PushNotificationID,omitempty"`
	WebUserId          string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId        string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken        string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AcknowledgePushNotificationResponse was auto-generated from
// WSDL.
type AcknowledgePushNotificationResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ActivateReasons was auto-generated from WSDL.
type ActivateReasons struct {
	Code        string `xml:"Code,omitempty" json:"Code,omitempty" yaml:"Code,omitempty"`
	Description string `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
}

// AddAssignmentToFlexibleWorkerResponse was auto-generated from
// WSDL.
type AddAssignmentToFlexibleWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// AddTrustToFlexibleWorkerResponse was auto-generated from WSDL.
type AddTrustToFlexibleWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// AgenciesUsedByTrust was auto-generated from WSDL.
type AgenciesUsedByTrust struct {
	Agency     *Agency `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	AreaOfWork string  `xml:"AreaOfWork,omitempty" json:"AreaOfWork,omitempty" yaml:"AreaOfWork,omitempty"`
	Assignment string  `xml:"Assignment,omitempty" json:"Assignment,omitempty" yaml:"Assignment,omitempty"`
	StaffGroup string  `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	Ward       *Ward   `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// AgenciesUsedByTrustResponse was auto-generated from WSDL.
type AgenciesUsedByTrustResponse struct {
	ErrorCode              int                         `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string                      `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int                         `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GetAgenciesUsedByTrust *ArrayOfAgenciesUsedByTrust `xml:"GetAgenciesUsedByTrust,omitempty" json:"GetAgenciesUsedByTrust,omitempty" yaml:"GetAgenciesUsedByTrust,omitempty"`
}

// Agency was auto-generated from WSDL.
type Agency struct {
	AgencyId   string `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	AgencyName string `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
}

// AgencyRateContract was auto-generated from WSDL.
type AgencyRateContract struct {
	Agency               *Agency          `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	BackdatedShiftsExist bool             `xml:"BackdatedShiftsExist,omitempty" json:"BackdatedShiftsExist,omitempty" yaml:"BackdatedShiftsExist,omitempty"`
	Comments             *ArrayOfComments `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
	ContactInfo          string           `xml:"ContactInfo,omitempty" json:"ContactInfo,omitempty" yaml:"ContactInfo,omitempty"`
	ContractID           string           `xml:"ContractID,omitempty" json:"ContractID,omitempty" yaml:"ContractID,omitempty"`
	EffectiveFrom        DateTime         `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
	EffectiveTo          DateTime         `xml:"EffectiveTo,omitempty" json:"EffectiveTo,omitempty" yaml:"EffectiveTo,omitempty"`
	Name                 string           `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	StaffGroup           *StaffGroup      `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	Status               string           `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Type                 string           `xml:"Type,omitempty" json:"Type,omitempty" yaml:"Type,omitempty"`
	VBL                  int              `xml:"VBL,omitempty" json:"VBL,omitempty" yaml:"VBL,omitempty"`
	Worker               string           `xml:"Worker,omitempty" json:"Worker,omitempty" yaml:"Worker,omitempty"`
	WorkerID             string           `xml:"WorkerID,omitempty" json:"WorkerID,omitempty" yaml:"WorkerID,omitempty"`
}

// AgencyRateContractResponse was auto-generated from WSDL.
type AgencyRateContractResponse struct {
	ErrorCode           int                        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string                     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int                        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AgencyRatesContract *ArrayOfAgencyRateContract `xml:"AgencyRatesContract,omitempty" json:"AgencyRatesContract,omitempty" yaml:"AgencyRatesContract,omitempty"`
}

// AgencyRatesContract was auto-generated from WSDL.
type AgencyRatesContract struct {
	XMLName     xml.Name             `xml:"http://tempuri.org/ AgencyRatesContract" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AgencyRatesContract"`
	RequestId   int                  `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	ContractId  int                  `xml:"ContractId,omitempty" json:"ContractId,omitempty" yaml:"ContractId,omitempty"`
	ShiftType   ShiftTypeEnumWithAll `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	WebUserId   string               `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string               `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AgencyRatesContractResponse was auto-generated from WSDL.
type AgencyRatesContractResponse struct {
	ErrorCode                             int                               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription                      string                            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                            int                               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ContactInformation                    string                            `xml:"ContactInformation,omitempty" json:"ContactInformation,omitempty" yaml:"ContactInformation,omitempty"`
	ContactName                           string                            `xml:"ContactName,omitempty" json:"ContactName,omitempty" yaml:"ContactName,omitempty"`
	ContractName                          string                            `xml:"ContractName,omitempty" json:"ContractName,omitempty" yaml:"ContractName,omitempty"`
	DayDefinitions                        *ArrayOfDayDefinitionsComplexType `xml:"DayDefinitions,omitempty" json:"DayDefinitions,omitempty" yaml:"DayDefinitions,omitempty"`
	DiscountInCommission                  float64                           `xml:"DiscountInCommission,omitempty" json:"DiscountInCommission,omitempty" yaml:"DiscountInCommission,omitempty"`
	EffectiveFrom                         DateTime                          `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
	FrameWork                             string                            `xml:"FrameWork,omitempty" json:"FrameWork,omitempty" yaml:"FrameWork,omitempty"`
	FrameWorkManagementCharge             float64                           `xml:"FrameWorkManagementCharge,omitempty" json:"FrameWorkManagementCharge,omitempty" yaml:"FrameWorkManagementCharge,omitempty"`
	FrameWorkManagementChargeIsPercentage bool                              `xml:"FrameWorkManagementChargeIsPercentage,omitempty" json:"FrameWorkManagementChargeIsPercentage,omitempty" yaml:"FrameWorkManagementChargeIsPercentage,omitempty"`
	Mileage                               *ArrayOfMileageComplexType        `xml:"Mileage,omitempty" json:"Mileage,omitempty" yaml:"Mileage,omitempty"`
	MileageExpensesPaid                   bool                              `xml:"MileageExpensesPaid,omitempty" json:"MileageExpensesPaid,omitempty" yaml:"MileageExpensesPaid,omitempty"`
	OnCallRate                            float64                           `xml:"OnCallRate,omitempty" json:"OnCallRate,omitempty" yaml:"OnCallRate,omitempty"`
	ProgrammedActivityRate                int                               `xml:"ProgrammedActivityRate,omitempty" json:"ProgrammedActivityRate,omitempty" yaml:"ProgrammedActivityRate,omitempty"`
	Rate                                  *ArrayOfRateComplexType           `xml:"Rate,omitempty" json:"Rate,omitempty" yaml:"Rate,omitempty"`
	Region                                string                            `xml:"Region,omitempty" json:"Region,omitempty" yaml:"Region,omitempty"`
	SleepInRate                           float64                           `xml:"SleepInRate,omitempty" json:"SleepInRate,omitempty" yaml:"SleepInRate,omitempty"`
	Status                                string                            `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	VAT                                   *VATComplexType                   `xml:"VAT,omitempty" json:"VAT,omitempty" yaml:"VAT,omitempty"`
	VBL                                   string                            `xml:"VBL,omitempty" json:"VBL,omitempty" yaml:"VBL,omitempty"`
}

// AgencyRequest was auto-generated from WSDL.
type AgencyRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ AgencyRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AgencyRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AgencyRequestResponse was auto-generated from WSDL.
type AgencyRequestResponse struct {
	ErrorCode        int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Agency           *ArrayOfAgency `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
}

// AgencyResponse was auto-generated from WSDL.
type AgencyResponse struct {
	ErrorCode        int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Agencies         *ArrayOfAgency `xml:"Agencies,omitempty" json:"Agencies,omitempty" yaml:"Agencies,omitempty"`
}

// AgencyStaff was auto-generated from WSDL.
type AgencyStaff struct {
	Agency         *ArrayOfAgencyType `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	AssignmentCode *ArrayOfstring     `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	DateOfBirth    DateTime           `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Gender         GenderEnum         `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	NInumber       string             `xml:"NInumber,omitempty" json:"NInumber,omitempty" yaml:"NInumber,omitempty"`
	Name           *Name              `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	PostCode       string             `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	StaffID        string             `xml:"StaffID,omitempty" json:"StaffID,omitempty" yaml:"StaffID,omitempty"`
}

// AgencyStaff2Type was auto-generated from WSDL.
type AgencyStaff2Type struct {
	DateOfBirth DateTime  `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Name        *NameType `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	PostCode    string    `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	StaffId     string    `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// AgencyStaffFacadeType was auto-generated from WSDL.
type AgencyStaffFacadeType struct {
	Agency  *AgencyType `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	Name    *NameType   `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	StaffId string      `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// AgencyStaffRequest was auto-generated from WSDL.
type AgencyStaffRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ AgencyStaffRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AgencyStaffRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyId    string   `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AgencyStaffRequestResponse was auto-generated from WSDL.
type AgencyStaffRequestResponse struct {
	AgencyStaffRequestResult *AgencyStaffResponse `xml:"AgencyStaffRequestResult,omitempty" json:"AgencyStaffRequestResult,omitempty" yaml:"AgencyStaffRequestResult,omitempty"`
}

// AgencyStaffResponse was auto-generated from WSDL.
type AgencyStaffResponse struct {
	ErrorCode        int                      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AgencyStaff      *ArrayOfAgencyStaff2Type `xml:"AgencyStaff,omitempty" json:"AgencyStaff,omitempty" yaml:"AgencyStaff,omitempty"`
}

// AgencyType was auto-generated from WSDL.
type AgencyType struct {
	AgencyId   string `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	AgencyName string `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
}

// AgencyUserRequest was auto-generated from WSDL.
type AgencyUserRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ AgencyUserRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AgencyUserRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyName  string   `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	DateOfBirth DateTime `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Name        *Name    `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AgencyUserRequestResponse was auto-generated from WSDL.
type AgencyUserRequestResponse struct {
	AgencyUserRequestResult *AgencyUserResponse `xml:"AgencyUserRequestResult,omitempty" json:"AgencyUserRequestResult,omitempty" yaml:"AgencyUserRequestResult,omitempty"`
}

// AgencyUserResponse was auto-generated from WSDL.
type AgencyUserResponse struct {
	ErrorCode        int                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AgencyUser       *ArrayOfAgencyUserType `xml:"AgencyUser,omitempty" json:"AgencyUser,omitempty" yaml:"AgencyUser,omitempty"`
}

// AgencyUserType was auto-generated from WSDL.
type AgencyUserType struct {
	Agency      *Agency          `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	DateOfBirth DateTime         `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Name        *Name            `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	Ward        *ArrayOfWardType `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WebUserID   string           `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
}

// AgencyWorker was auto-generated from WSDL.
type AgencyWorker struct {
	AgencyUniqueID string `xml:"AgencyUniqueID,omitempty" json:"AgencyUniqueID,omitempty" yaml:"AgencyUniqueID,omitempty"`
	FirstName      string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	NurseID        string `xml:"NurseID,omitempty" json:"NurseID,omitempty" yaml:"NurseID,omitempty"`
	Surname        string `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
}

// AmendTimeSheet was auto-generated from WSDL.
type AmendTimeSheet struct {
	XMLName            xml.Name                                 `xml:"http://tempuri.org/ AmendTimeSheet" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AmendTimeSheet"`
	RequestId          int                                      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffId            string                                   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebuserId          string                                   `xml:"WebuserId,omitempty" json:"WebuserId,omitempty" yaml:"WebuserId,omitempty"`
	RequesterID        string                                   `xml:"RequesterID,omitempty" json:"RequesterID,omitempty" yaml:"RequesterID,omitempty"`
	AgencyCode         int                                      `xml:"AgencyCode,omitempty" json:"AgencyCode,omitempty" yaml:"AgencyCode,omitempty"`
	AssignmentCode     string                                   `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	ShiftDate          DateTime                                 `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	StartTime          DateTime                                 `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	EndTime            DateTime                                 `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	BreakInMins        int                                      `xml:"BreakInMins,omitempty" json:"BreakInMins,omitempty" yaml:"BreakInMins,omitempty"`
	ShiftType          ShiftType                                `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	CancellationReason string                                   `xml:"CancellationReason,omitempty" json:"CancellationReason,omitempty" yaml:"CancellationReason,omitempty"`
	Reason             ReasonTypesForModifyingShiftAfterRelease `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	IgnoreWarning      bool                                     `xml:"IgnoreWarning,omitempty" json:"IgnoreWarning,omitempty" yaml:"IgnoreWarning,omitempty"`
	ReferenceNumber    int                                      `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	CallerToken        string                                   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AmendTimeSheetResponse was auto-generated from WSDL.
type AmendTimeSheetResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// Answer was auto-generated from WSDL.
type Answer struct {
	AddressedDuringAssignment string `xml:"AddressedDuringAssignment,omitempty" json:"AddressedDuringAssignment,omitempty" yaml:"AddressedDuringAssignment,omitempty"`
	Answer                    string `xml:"Answer,omitempty" json:"Answer,omitempty" yaml:"Answer,omitempty"`
	AnswerID                  string `xml:"AnswerID,omitempty" json:"AnswerID,omitempty" yaml:"AnswerID,omitempty"`
	ReasonRequired            string `xml:"ReasonRequired,omitempty" json:"ReasonRequired,omitempty" yaml:"ReasonRequired,omitempty"`
}

// ArrayOfActivateReasons was auto-generated from WSDL.
type ArrayOfActivateReasons struct {
	ActivateReasons []*ActivateReasons `xml:"ActivateReasons,omitempty" json:"ActivateReasons,omitempty" yaml:"ActivateReasons,omitempty"`
}

// ArrayOfAgenciesUsedByTrust was auto-generated from WSDL.
type ArrayOfAgenciesUsedByTrust struct {
	AgenciesUsedByTrust []*AgenciesUsedByTrust `xml:"AgenciesUsedByTrust,omitempty" json:"AgenciesUsedByTrust,omitempty" yaml:"AgenciesUsedByTrust,omitempty"`
}

// ArrayOfAgency was auto-generated from WSDL.
type ArrayOfAgency struct {
	Agency []*Agency `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
}

// ArrayOfAgencyRateContract was auto-generated from WSDL.
type ArrayOfAgencyRateContract struct {
	AgencyRateContract []*AgencyRateContract `xml:"AgencyRateContract,omitempty" json:"AgencyRateContract,omitempty" yaml:"AgencyRateContract,omitempty"`
}

// ArrayOfAgencyStaff2Type was auto-generated from WSDL.
type ArrayOfAgencyStaff2Type struct {
	AgencyStaff2Type []*AgencyStaff2Type `xml:"AgencyStaff2Type,omitempty" json:"AgencyStaff2Type,omitempty" yaml:"AgencyStaff2Type,omitempty"`
}

// ArrayOfAgencyType was auto-generated from WSDL.
type ArrayOfAgencyType struct {
	AgencyType []*AgencyType `xml:"AgencyType,omitempty" json:"AgencyType,omitempty" yaml:"AgencyType,omitempty"`
}

// ArrayOfAgencyUserType was auto-generated from WSDL.
type ArrayOfAgencyUserType struct {
	AgencyUserType []*AgencyUserType `xml:"AgencyUserType,omitempty" json:"AgencyUserType,omitempty" yaml:"AgencyUserType,omitempty"`
}

// ArrayOfAnswer was auto-generated from WSDL.
type ArrayOfAnswer struct {
	Answer []*Answer `xml:"Answer,omitempty" json:"Answer,omitempty" yaml:"Answer,omitempty"`
}

// ArrayOfArrayOfMessages was auto-generated from WSDL.
type ArrayOfArrayOfMessages struct {
	ArrayOfMessages []*ArrayOfMessages `xml:"ArrayOfMessages,omitempty" json:"ArrayOfMessages,omitempty" yaml:"ArrayOfMessages,omitempty"`
}

// ArrayOfAssignmentCodeType was auto-generated from WSDL.
type ArrayOfAssignmentCodeType struct {
	AssignmentCodeType []*AssignmentCodeType `xml:"AssignmentCodeType,omitempty" json:"AssignmentCodeType,omitempty" yaml:"AssignmentCodeType,omitempty"`
}

// ArrayOfAuthorisedShiftsDetails was auto-generated from WSDL.
type ArrayOfAuthorisedShiftsDetails struct {
	AuthorisedShiftsDetails []*AuthorisedShiftsDetails `xml:"AuthorisedShiftsDetails,omitempty" json:"AuthorisedShiftsDetails,omitempty" yaml:"AuthorisedShiftsDetails,omitempty"`
}

// ArrayOfAvailabilityEntry was auto-generated from WSDL.
type ArrayOfAvailabilityEntry struct {
	AvailabilityEntry []*AvailabilityEntry `xml:"AvailabilityEntry,omitempty" json:"AvailabilityEntry,omitempty" yaml:"AvailabilityEntry,omitempty"`
}

// ArrayOfBackDatedShifts was auto-generated from WSDL.
type ArrayOfBackDatedShifts struct {
	BackDatedShifts []*BackDatedShifts `xml:"BackDatedShifts,omitempty" json:"BackDatedShifts,omitempty" yaml:"BackDatedShifts,omitempty"`
}

// ArrayOfBackingReport was auto-generated from WSDL.
type ArrayOfBackingReport struct {
	BackingReport []*BackingReport `xml:"BackingReport,omitempty" json:"BackingReport,omitempty" yaml:"BackingReport,omitempty"`
}

// ArrayOfBackingReportShift was auto-generated from WSDL.
type ArrayOfBackingReportShift struct {
	BackingReportShift []*BackingReportShift `xml:"BackingReportShift,omitempty" json:"BackingReportShift,omitempty" yaml:"BackingReportShift,omitempty"`
}

// ArrayOfBankStaff was auto-generated from WSDL.
type ArrayOfBankStaff struct {
	BankStaff []*BankStaff `xml:"BankStaff,omitempty" json:"BankStaff,omitempty" yaml:"BankStaff,omitempty"`
}

// ArrayOfBookingData was auto-generated from WSDL.
type ArrayOfBookingData struct {
	BookingData []*BookingData `xml:"BookingData,omitempty" json:"BookingData,omitempty" yaml:"BookingData,omitempty"`
}

// ArrayOfCancelledBookings was auto-generated from WSDL.
type ArrayOfCancelledBookings struct {
	CancelledBookings []*CancelledBookings `xml:"CancelledBookings,omitempty" json:"CancelledBookings,omitempty" yaml:"CancelledBookings,omitempty"`
}

// ArrayOfComments was auto-generated from WSDL.
type ArrayOfComments struct {
	Comments []*Comments `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
}

// ArrayOfCommentsComplexType was auto-generated from WSDL.
type ArrayOfCommentsComplexType struct {
	CommentsComplexType []*CommentsComplexType `xml:"CommentsComplexType,omitempty" json:"CommentsComplexType,omitempty" yaml:"CommentsComplexType,omitempty"`
}

// ArrayOfCompletePerformanceEvaluation was auto-generated from
// WSDL.
type ArrayOfCompletePerformanceEvaluation struct {
	CompletePerformanceEvaluation []*CompletePerformanceEvaluation `xml:"CompletePerformanceEvaluation,omitempty" json:"CompletePerformanceEvaluation,omitempty" yaml:"CompletePerformanceEvaluation,omitempty"`
}

// ArrayOfDNASNCNotes was auto-generated from WSDL.
type ArrayOfDNASNCNotes struct {
	DNASNCNotes []*DNASNCNotes `xml:"DNASNCNotes,omitempty" json:"DNASNCNotes,omitempty" yaml:"DNASNCNotes,omitempty"`
}

// ArrayOfDNASNCWarnings was auto-generated from WSDL.
type ArrayOfDNASNCWarnings struct {
	DNASNCWarnings []*DNASNCWarnings `xml:"DNASNCWarnings,omitempty" json:"DNASNCWarnings,omitempty" yaml:"DNASNCWarnings,omitempty"`
}

// ArrayOfDayDefinitionsComplexType was auto-generated from WSDL.
type ArrayOfDayDefinitionsComplexType struct {
	DayDefinitionsComplexType []*DayDefinitionsComplexType `xml:"DayDefinitionsComplexType,omitempty" json:"DayDefinitionsComplexType,omitempty" yaml:"DayDefinitionsComplexType,omitempty"`
}

// ArrayOfEvidenceDetail was auto-generated from WSDL.
type ArrayOfEvidenceDetail struct {
	EvidenceDetail []*EvidenceDetail `xml:"EvidenceDetail,omitempty" json:"EvidenceDetail,omitempty" yaml:"EvidenceDetail,omitempty"`
}

// ArrayOfFWCancelReason was auto-generated from WSDL.
type ArrayOfFWCancelReason struct {
	FWCancelReason []*FWCancelReason `xml:"FWCancelReason,omitempty" json:"FWCancelReason,omitempty" yaml:"FWCancelReason,omitempty"`
}

// ArrayOfFWContacts was auto-generated from WSDL.
type ArrayOfFWContacts struct {
	FWContacts []*FWContacts `xml:"FWContacts,omitempty" json:"FWContacts,omitempty" yaml:"FWContacts,omitempty"`
}

// ArrayOfFlexibleWorker was auto-generated from WSDL.
type ArrayOfFlexibleWorker struct {
	FlexibleWorker []*FlexibleWorker `xml:"FlexibleWorker,omitempty" json:"FlexibleWorker,omitempty" yaml:"FlexibleWorker,omitempty"`
}

// ArrayOfGetPerformanceEvaluation was auto-generated from WSDL.
type ArrayOfGetPerformanceEvaluation struct {
	GetPerformanceEvaluation []*GetPerformanceEvaluation `xml:"GetPerformanceEvaluation,omitempty" json:"GetPerformanceEvaluation,omitempty" yaml:"GetPerformanceEvaluation,omitempty"`
}

// ArrayOfGetSelfBillingAgreements was auto-generated from WSDL.
type ArrayOfGetSelfBillingAgreements struct {
	GetSelfBillingAgreements []*GetSelfBillingAgreements `xml:"GetSelfBillingAgreements,omitempty" json:"GetSelfBillingAgreements,omitempty" yaml:"GetSelfBillingAgreements,omitempty"`
}

// ArrayOfHistory was auto-generated from WSDL.
type ArrayOfHistory struct {
	History []*History `xml:"History,omitempty" json:"History,omitempty" yaml:"History,omitempty"`
}

// ArrayOfHolidayBooking was auto-generated from WSDL.
type ArrayOfHolidayBooking struct {
	HolidayBooking []*HolidayBooking `xml:"HolidayBooking,omitempty" json:"HolidayBooking,omitempty" yaml:"HolidayBooking,omitempty"`
}

// ArrayOfIVRShiftType was auto-generated from WSDL.
type ArrayOfIVRShiftType struct {
	IVRShiftType []*IVRShiftType `xml:"IVRShiftType,omitempty" json:"IVRShiftType,omitempty" yaml:"IVRShiftType,omitempty"`
}

// ArrayOfInvoicedetail was auto-generated from WSDL.
type ArrayOfInvoicedetail struct {
	Invoicedetail []*Invoicedetail `xml:"Invoicedetail,omitempty" json:"Invoicedetail,omitempty" yaml:"Invoicedetail,omitempty"`
}

// ArrayOfJobCodes was auto-generated from WSDL.
type ArrayOfJobCodes struct {
	JobCodes []*JobCodes `xml:"JobCodes,omitempty" json:"JobCodes,omitempty" yaml:"JobCodes,omitempty"`
}

// ArrayOfLeadTimeEntry was auto-generated from WSDL.
type ArrayOfLeadTimeEntry struct {
	LeadTimeEntry []*LeadTimeEntry `xml:"LeadTimeEntry,omitempty" json:"LeadTimeEntry,omitempty" yaml:"LeadTimeEntry,omitempty"`
}

// ArrayOfLocation was auto-generated from WSDL.
type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
}

// ArrayOfLocationType was auto-generated from WSDL.
type ArrayOfLocationType struct {
	LocationType []*LocationType `xml:"LocationType,omitempty" json:"LocationType,omitempty" yaml:"LocationType,omitempty"`
}

// ArrayOfMarketingShift was auto-generated from WSDL.
type ArrayOfMarketingShift struct {
	MarketingShift []*MarketingShift `xml:"MarketingShift,omitempty" json:"MarketingShift,omitempty" yaml:"MarketingShift,omitempty"`
}

// ArrayOfMessages was auto-generated from WSDL.
type ArrayOfMessages struct {
	MessageCode        int    `xml:"MessageCode,omitempty" json:"MessageCode,omitempty" yaml:"MessageCode,omitempty"`
	MessageDescription string `xml:"MessageDescription,omitempty" json:"MessageDescription,omitempty" yaml:"MessageDescription,omitempty"`
}

// ArrayOfMileageComplexType was auto-generated from WSDL.
type ArrayOfMileageComplexType struct {
	MileageComplexType []*MileageComplexType `xml:"MileageComplexType,omitempty" json:"MileageComplexType,omitempty" yaml:"MileageComplexType,omitempty"`
}

// ArrayOfModifyTimeSheetReasons was auto-generated from WSDL.
type ArrayOfModifyTimeSheetReasons struct {
	ModifyTimeSheetReasons []*ModifyTimeSheetReasons `xml:"ModifyTimeSheetReasons,omitempty" json:"ModifyTimeSheetReasons,omitempty" yaml:"ModifyTimeSheetReasons,omitempty"`
}

// ArrayOfNewStarter was auto-generated from WSDL.
type ArrayOfNewStarter struct {
	NewStarter []*NewStarter `xml:"NewStarter,omitempty" json:"NewStarter,omitempty" yaml:"NewStarter,omitempty"`
}

// ArrayOfNote was auto-generated from WSDL.
type ArrayOfNote struct {
	Note []*Note `xml:"Note,omitempty" json:"Note,omitempty" yaml:"Note,omitempty"`
}

// ArrayOfNoteComplexType was auto-generated from WSDL.
type ArrayOfNoteComplexType struct {
	NoteComplexType []*NoteComplexType `xml:"NoteComplexType,omitempty" json:"NoteComplexType,omitempty" yaml:"NoteComplexType,omitempty"`
}

// ArrayOfNoteDetails was auto-generated from WSDL.
type ArrayOfNoteDetails struct {
	NoteDetails []*NoteDetails `xml:"NoteDetails,omitempty" json:"NoteDetails,omitempty" yaml:"NoteDetails,omitempty"`
}

// ArrayOfNotification was auto-generated from WSDL.
type ArrayOfNotification struct {
	Notification []*Notification `xml:"Notification,omitempty" json:"Notification,omitempty" yaml:"Notification,omitempty"`
}

// ArrayOfPatternItem was auto-generated from WSDL.
type ArrayOfPatternItem struct {
	PatternItem []*PatternItem `xml:"PatternItem,omitempty" json:"PatternItem,omitempty" yaml:"PatternItem,omitempty"`
}

// ArrayOfPayslip was auto-generated from WSDL.
type ArrayOfPayslip struct {
	Payslip []*Payslip `xml:"Payslip,omitempty" json:"Payslip,omitempty" yaml:"Payslip,omitempty"`
}

// ArrayOfPayslipDeductions was auto-generated from WSDL.
type ArrayOfPayslipDeductions struct {
	PayslipDeductions []*PayslipDeductions `xml:"PayslipDeductions,omitempty" json:"PayslipDeductions,omitempty" yaml:"PayslipDeductions,omitempty"`
}

// ArrayOfPayslipPayments was auto-generated from WSDL.
type ArrayOfPayslipPayments struct {
	PayslipPayments []*PayslipPayments `xml:"PayslipPayments,omitempty" json:"PayslipPayments,omitempty" yaml:"PayslipPayments,omitempty"`
}

// ArrayOfPerformanceEvaluationComments was auto-generated from
// WSDL.
type ArrayOfPerformanceEvaluationComments struct {
	PerformanceEvaluationComments []*PerformanceEvaluationComments `xml:"PerformanceEvaluationComments,omitempty" json:"PerformanceEvaluationComments,omitempty" yaml:"PerformanceEvaluationComments,omitempty"`
}

// ArrayOfPerformanceEvaluationReferrals was auto-generated from
// WSDL.
type ArrayOfPerformanceEvaluationReferrals struct {
	PerformanceEvaluationReferrals []*PerformanceEvaluationReferrals `xml:"PerformanceEvaluationReferrals,omitempty" json:"PerformanceEvaluationReferrals,omitempty" yaml:"PerformanceEvaluationReferrals,omitempty"`
}

// ArrayOfPerformanceEvaluation_Answer was auto-generated from
// WSDL.
type ArrayOfPerformanceEvaluation_Answer struct {
	PerformanceEvaluation_Answer []*PerformanceEvaluation_Answer `xml:"PerformanceEvaluation_Answer,omitempty" json:"PerformanceEvaluation_Answer,omitempty" yaml:"PerformanceEvaluation_Answer,omitempty"`
}

// ArrayOfPersonalisedRatesRequests was auto-generated from WSDL.
type ArrayOfPersonalisedRatesRequests struct {
	PersonalisedRatesRequests []*PersonalisedRatesRequests `xml:"PersonalisedRatesRequests,omitempty" json:"PersonalisedRatesRequests,omitempty" yaml:"PersonalisedRatesRequests,omitempty"`
}

// ArrayOfPriorQualificataion was auto-generated from WSDL.
type ArrayOfPriorQualificataion struct {
	PriorQualificataion []*PriorQualificataion `xml:"PriorQualificataion,omitempty" json:"PriorQualificataion,omitempty" yaml:"PriorQualificataion,omitempty"`
}

// ArrayOfPushNotification was auto-generated from WSDL.
type ArrayOfPushNotification struct {
	PushNotification []*PushNotification `xml:"PushNotification,omitempty" json:"PushNotification,omitempty" yaml:"PushNotification,omitempty"`
}

// ArrayOfQuery was auto-generated from WSDL.
type ArrayOfQuery struct {
	Query []*Query `xml:"Query,omitempty" json:"Query,omitempty" yaml:"Query,omitempty"`
}

// ArrayOfRateComplexType was auto-generated from WSDL.
type ArrayOfRateComplexType struct {
	RateComplexType []*RateComplexType `xml:"RateComplexType,omitempty" json:"RateComplexType,omitempty" yaml:"RateComplexType,omitempty"`
}

// ArrayOfReasonCode was auto-generated from WSDL.
type ArrayOfReasonCode struct {
	ReasonCode []*ReasonCode `xml:"ReasonCode,omitempty" json:"ReasonCode,omitempty" yaml:"ReasonCode,omitempty"`
}

// ArrayOfReasons was auto-generated from WSDL.
type ArrayOfReasons struct {
	Reasons []*Reasons `xml:"Reasons,omitempty" json:"Reasons,omitempty" yaml:"Reasons,omitempty"`
}

// ArrayOfRefusedBookings was auto-generated from WSDL.
type ArrayOfRefusedBookings struct {
	RefusedBookings []*RefusedBookings `xml:"RefusedBookings,omitempty" json:"RefusedBookings,omitempty" yaml:"RefusedBookings,omitempty"`
}

// ArrayOfShift was auto-generated from WSDL.
type ArrayOfShift struct {
	Shift []*Shift `xml:"Shift,omitempty" json:"Shift,omitempty" yaml:"Shift,omitempty"`
}

// ArrayOfShiftDetails was auto-generated from WSDL.
type ArrayOfShiftDetails struct {
	ShiftDetails []*ShiftDetails `xml:"ShiftDetails,omitempty" json:"ShiftDetails,omitempty" yaml:"ShiftDetails,omitempty"`
}

// ArrayOfShiftType was auto-generated from WSDL.
type ArrayOfShiftType struct {
	ShiftType []ShiftType `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
}

// ArrayOfSingleSignOnLink was auto-generated from WSDL.
type ArrayOfSingleSignOnLink struct {
	SingleSignOnLink []*SingleSignOnLink `xml:"SingleSignOnLink,omitempty" json:"SingleSignOnLink,omitempty" yaml:"SingleSignOnLink,omitempty"`
}

// ArrayOfSubstantiveWorkerHolidays was auto-generated from WSDL.
type ArrayOfSubstantiveWorkerHolidays struct {
	SubstantiveWorkerHolidays []*SubstantiveWorkerHolidays `xml:"SubstantiveWorkerHolidays,omitempty" json:"SubstantiveWorkerHolidays,omitempty" yaml:"SubstantiveWorkerHolidays,omitempty"`
}

// ArrayOfSuggestedWorkers was auto-generated from WSDL.
type ArrayOfSuggestedWorkers struct {
	SuggestedWorkers []*SuggestedWorkers `xml:"SuggestedWorkers,omitempty" json:"SuggestedWorkers,omitempty" yaml:"SuggestedWorkers,omitempty"`
}

// ArrayOfTemporalNonAvailability was auto-generated from WSDL.
type ArrayOfTemporalNonAvailability struct {
	TemporalNonAvailability []*TemporalNonAvailability `xml:"TemporalNonAvailability,omitempty" json:"TemporalNonAvailability,omitempty" yaml:"TemporalNonAvailability,omitempty"`
}

// ArrayOfTimeSheet was auto-generated from WSDL.
type ArrayOfTimeSheet struct {
	TimeSheet []*TimeSheet `xml:"TimeSheet,omitempty" json:"TimeSheet,omitempty" yaml:"TimeSheet,omitempty"`
}

// ArrayOfTrust was auto-generated from WSDL.
type ArrayOfTrust struct {
	Trust []*Trust `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// ArrayOfTrustUsersWhoCanCompletePerformanceEvaluation was auto-generated
// from WSDL.
type ArrayOfTrustUsersWhoCanCompletePerformanceEvaluation struct {
	TrustUsersWhoCanCompletePerformanceEvaluation []*TrustUsersWhoCanCompletePerformanceEvaluation `xml:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty" json:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty" yaml:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty"`
}

// ArrayOfTwoTierAuthorisationShiftStatusType was auto-generated
// from WSDL.
type ArrayOfTwoTierAuthorisationShiftStatusType struct {
	TwoTierAuthorisationShiftStatusType []*TwoTierAuthorisationShiftStatusType `xml:"TwoTierAuthorisationShiftStatusType,omitempty" json:"TwoTierAuthorisationShiftStatusType,omitempty" yaml:"TwoTierAuthorisationShiftStatusType,omitempty"`
}

// ArrayOfUnAvailablePeriod was auto-generated from WSDL.
type ArrayOfUnAvailablePeriod struct {
	UnAvailablePeriod []*UnAvailablePeriod `xml:"UnAvailablePeriod,omitempty" json:"UnAvailablePeriod,omitempty" yaml:"UnAvailablePeriod,omitempty"`
}

// ArrayOfUnrestrictedWorkPeriod was auto-generated from WSDL.
type ArrayOfUnrestrictedWorkPeriod struct {
	UnrestrictedWorkPeriod []*UnrestrictedWorkPeriod `xml:"UnrestrictedWorkPeriod,omitempty" json:"UnrestrictedWorkPeriod,omitempty" yaml:"UnrestrictedWorkPeriod,omitempty"`
}

// ArrayOfVisaReview was auto-generated from WSDL.
type ArrayOfVisaReview struct {
	VisaReview []*VisaReview `xml:"VisaReview,omitempty" json:"VisaReview,omitempty" yaml:"VisaReview,omitempty"`
}

// ArrayOfWard was auto-generated from WSDL.
type ArrayOfWard struct {
	Ward []*Ward `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// ArrayOfWardCancelReason was auto-generated from WSDL.
type ArrayOfWardCancelReason struct {
	WardCancelReason []*WardCancelReason `xml:"WardCancelReason,omitempty" json:"WardCancelReason,omitempty" yaml:"WardCancelReason,omitempty"`
}

// ArrayOfWardDetails was auto-generated from WSDL.
type ArrayOfWardDetails struct {
	WardDetails []*WardDetails `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
}

// ArrayOfWardType was auto-generated from WSDL.
type ArrayOfWardType struct {
	WardType []*WardType `xml:"WardType,omitempty" json:"WardType,omitempty" yaml:"WardType,omitempty"`
}

// ArrayOfWardUserType was auto-generated from WSDL.
type ArrayOfWardUserType struct {
	WardUserType []*WardUserType `xml:"WardUserType,omitempty" json:"WardUserType,omitempty" yaml:"WardUserType,omitempty"`
}

// ArrayOfWorker was auto-generated from WSDL.
type ArrayOfWorker struct {
	Worker []*Worker `xml:"Worker,omitempty" json:"Worker,omitempty" yaml:"Worker,omitempty"`
}

// ArrayOfchar was auto-generated from WSDL.
type ArrayOfchar struct {
	Char []char `xml:"char,omitempty" json:"char,omitempty" yaml:"char,omitempty"`
}

// ArrayOfdateTime was auto-generated from WSDL.
type ArrayOfdateTime struct {
	DateTime []DateTime `xml:"dateTime,omitempty" json:"dateTime,omitempty" yaml:"dateTime,omitempty"`
}

// ArrayOfint was auto-generated from WSDL.
type ArrayOfint struct {
	Int []int `xml:"int,omitempty" json:"int,omitempty" yaml:"int,omitempty"`
}

// ArrayOfstring was auto-generated from WSDL.
type ArrayOfstring struct {
	String []string `xml:"string,omitempty" json:"string,omitempty" yaml:"string,omitempty"`
}

// AssignmentCodeType was auto-generated from WSDL.
type AssignmentCodeType struct {
	AssignmentCode string   `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Disable        bool     `xml:"Disable,omitempty" json:"Disable,omitempty" yaml:"Disable,omitempty"`
	EffectiveFrom  DateTime `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
}

// AssignmentCodesRequest was auto-generated from WSDL.
type AssignmentCodesRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ AssignmentCodesRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AssignmentCodesRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	TrustID     string   `xml:"TrustID,omitempty" json:"TrustID,omitempty" yaml:"TrustID,omitempty"`
	StaffGroup  string   `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AssignmentCodesRequestResponse was auto-generated from WSDL.
type AssignmentCodesRequestResponse struct {
	AssignmentCodesRequestResult *AssignmentCodesResponse `xml:"AssignmentCodesRequestResult,omitempty" json:"AssignmentCodesRequestResult,omitempty" yaml:"AssignmentCodesRequestResult,omitempty"`
}

// AssignmentCodesResponse was auto-generated from WSDL.
type AssignmentCodesResponse struct {
	ErrorCode        int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AssignmentCode   *ArrayOfstring `xml:"assignmentCode,omitempty" json:"assignmentCode,omitempty" yaml:"assignmentCode,omitempty"`
}

// AuthenticateCaller was auto-generated from WSDL.
type AuthenticateCaller struct {
	XMLName   xml.Name `xml:"http://tempuri.org/ AuthenticateCaller" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AuthenticateCaller"`
	RequestId int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Username  string   `xml:"username,omitempty" json:"username,omitempty" yaml:"username,omitempty"`
	Password  string   `xml:"password,omitempty" json:"password,omitempty" yaml:"password,omitempty"`
}

// AuthenticateCallerResponse was auto-generated from WSDL.
type AuthenticateCallerResponse struct {
	ErrorCode        int      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	CallerToken      string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
	ExpiresOn        DateTime `xml:"ExpiresOn,omitempty" json:"ExpiresOn,omitempty" yaml:"ExpiresOn,omitempty"`
}

// AuthenticateFlexibleWorkerResponse was auto-generated from WSDL.
type AuthenticateFlexibleWorkerResponse struct {
	ErrorCode             int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription      string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId            int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	CanLogin              bool   `xml:"CanLogin,omitempty" json:"CanLogin,omitempty" yaml:"CanLogin,omitempty"`
	PasswordResetRequired bool   `xml:"PasswordResetRequired,omitempty" json:"PasswordResetRequired,omitempty" yaml:"PasswordResetRequired,omitempty"`
	StaffId               string `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// AuthenticateUser was auto-generated from WSDL.
type AuthenticateUser struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ AuthenticateUser" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AuthenticateUser"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	UserName    string   `xml:"UserName,omitempty" json:"UserName,omitempty" yaml:"UserName,omitempty"`
	Password    string   `xml:"Password,omitempty" json:"Password,omitempty" yaml:"Password,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AuthenticateUserResponse was auto-generated from WSDL.
type AuthenticateUserResponse struct {
	ErrorCode             int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription      string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId            int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	CanLogin              bool   `xml:"CanLogin,omitempty" json:"CanLogin,omitempty" yaml:"CanLogin,omitempty"`
	PasswordResetRequired bool   `xml:"PasswordResetRequired,omitempty" json:"PasswordResetRequired,omitempty" yaml:"PasswordResetRequired,omitempty"`
	StaffId               string `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	UserType              string `xml:"UserType,omitempty" json:"UserType,omitempty" yaml:"UserType,omitempty"`
	WebUserID             string `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
}

// AuthorisedShiftsDetails was auto-generated from WSDL.
type AuthorisedShiftsDetails struct {
	DummyReferenceNumber int            `xml:"DummyReferenceNumber,omitempty" json:"DummyReferenceNumber,omitempty" yaml:"DummyReferenceNumber,omitempty"`
	NewReferenceNumber   int            `xml:"NewReferenceNumber,omitempty" json:"NewReferenceNumber,omitempty" yaml:"NewReferenceNumber,omitempty"`
	ReasonForFailure     *ArrayOfstring `xml:"ReasonForFailure,omitempty" json:"ReasonForFailure,omitempty" yaml:"ReasonForFailure,omitempty"`
}

// AvailabilityEntry was auto-generated from WSDL.
type AvailabilityEntry struct {
	AvailabilityItemId int         `xml:"AvailabilityItemId,omitempty" json:"AvailabilityItemId,omitempty" yaml:"AvailabilityItemId,omitempty"`
	AvailabilityType   string      `xml:"AvailabilityType,omitempty" json:"AvailabilityType,omitempty" yaml:"AvailabilityType,omitempty"`
	EndDate            DateTime    `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	EndTime            DateTime    `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Recurrence         *Recurrence `xml:"Recurrence,omitempty" json:"Recurrence,omitempty" yaml:"Recurrence,omitempty"`
	StartDate          DateTime    `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	StartTime          DateTime    `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
}

// AvailableShifts was auto-generated from WSDL.
type AvailableShifts struct {
	XMLName          xml.Name                    `xml:"http://tempuri.org/ AvailableShifts" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AvailableShifts"`
	RequestId        int                         `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AssignmentCode   string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Date             DateTime                    `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	DateRange        Date_x0020_Range            `xml:"DateRange,omitempty" json:"DateRange,omitempty" yaml:"DateRange,omitempty"`
	Shift_Types      *TypeOfShifts               `xml:"Shift_Types,omitempty" json:"Shift_Types,omitempty" yaml:"Shift_Types,omitempty"`
	ShiftTimeType    Shift_x0020_Time_x0020_Enum `xml:"ShiftTimeType,omitempty" json:"ShiftTimeType,omitempty" yaml:"ShiftTimeType,omitempty"`
	MaximumNumber    string                      `xml:"MaximumNumber,omitempty" json:"MaximumNumber,omitempty" yaml:"MaximumNumber,omitempty"`
	TrustId          string                      `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	StaffId          string                      `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebUserId        string                      `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RemoveDuplicates bool                        `xml:"RemoveDuplicates,omitempty" json:"RemoveDuplicates,omitempty" yaml:"RemoveDuplicates,omitempty"`
	CallerToken      string                      `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AvailableShiftsResponse was auto-generated from WSDL.
type AvailableShiftsResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shift            *ArrayOfIVRShiftType `xml:"Shift,omitempty" json:"Shift,omitempty" yaml:"Shift,omitempty"`
}

// AvailableShifts_AdvancedSearch was auto-generated from WSDL.
type AvailableShifts_AdvancedSearch struct {
	XMLName                     xml.Name       `xml:"http://tempuri.org/ AvailableShifts_AdvancedSearch" json:"-" yaml:"-" action:"http://tempuri.org/IClient/AvailableShifts_AdvancedSearch"`
	RequestId                   int            `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AssignmentCode              *ArrayOfstring `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	StaffId                     string         `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	StartDate                   DateTime       `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	StartTime                   DateTime       `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	EndDate                     DateTime       `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	EndTime                     DateTime       `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	TrustCode                   *ArrayOfstring `xml:"TrustCode,omitempty" json:"TrustCode,omitempty" yaml:"TrustCode,omitempty"`
	LocationCode                *ArrayOfstring `xml:"LocationCode,omitempty" json:"LocationCode,omitempty" yaml:"LocationCode,omitempty"`
	WardCode                    *ArrayOfstring `xml:"WardCode,omitempty" json:"WardCode,omitempty" yaml:"WardCode,omitempty"`
	ShiftType                   *ArrayOfstring `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	MatchShiftsToMyAvailability bool           `xml:"MatchShiftsToMyAvailability,omitempty" json:"MatchShiftsToMyAvailability,omitempty" yaml:"MatchShiftsToMyAvailability,omitempty"`
	TimeOfDay                   ShiftTimeType  `xml:"TimeOfDay,omitempty" json:"TimeOfDay,omitempty" yaml:"TimeOfDay,omitempty"`
	CallerToken                 string         `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// AvailableShifts_AdvancedSearchResponse was auto-generated from
// WSDL.
type AvailableShifts_AdvancedSearchResponse struct {
	ErrorCode        int           `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string        `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int           `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shifts           *ArrayOfShift `xml:"Shifts,omitempty" json:"Shifts,omitempty" yaml:"Shifts,omitempty"`
}

// BackDatedShifts was auto-generated from WSDL.
type BackDatedShifts struct {
	ActionTaken        string         `xml:"ActionTaken,omitempty" json:"ActionTaken,omitempty" yaml:"ActionTaken,omitempty"`
	Agency             *ArrayOfAgency `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	BackingReportID    int            `xml:"BackingReportID,omitempty" json:"BackingReportID,omitempty" yaml:"BackingReportID,omitempty"`
	EndTime            DateTime       `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	InvoiceNumber      string         `xml:"InvoiceNumber,omitempty" json:"InvoiceNumber,omitempty" yaml:"InvoiceNumber,omitempty"`
	OriginalCharge     float64        `xml:"OriginalCharge,omitempty" json:"OriginalCharge,omitempty" yaml:"OriginalCharge,omitempty"`
	PaymentVariation   float64        `xml:"PaymentVariation,omitempty" json:"PaymentVariation,omitempty" yaml:"PaymentVariation,omitempty"`
	RecalculatedCharge float64        `xml:"RecalculatedCharge,omitempty" json:"RecalculatedCharge,omitempty" yaml:"RecalculatedCharge,omitempty"`
	ReferenceNo        int            `xml:"ReferenceNo,omitempty" json:"ReferenceNo,omitempty" yaml:"ReferenceNo,omitempty"`
	ShiftDate          DateTime       `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	StartTime          DateTime       `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	Trust              *ArrayOfTrust  `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// BackingReport was auto-generated from WSDL.
type BackingReport struct {
	BackingReportID       string       `xml:"BackingReportID,omitempty" json:"BackingReportID,omitempty" yaml:"BackingReportID,omitempty"`
	GeneratedBy           string       `xml:"GeneratedBy,omitempty" json:"GeneratedBy,omitempty" yaml:"GeneratedBy,omitempty"`
	GeneratedOn           DateTime     `xml:"GeneratedOn,omitempty" json:"GeneratedOn,omitempty" yaml:"GeneratedOn,omitempty"`
	InvoiceID             string       `xml:"InvoiceID,omitempty" json:"InvoiceID,omitempty" yaml:"InvoiceID,omitempty"`
	InvoiceNumber         string       `xml:"InvoiceNumber,omitempty" json:"InvoiceNumber,omitempty" yaml:"InvoiceNumber,omitempty"`
	Parameters            *Parameters  `xml:"Parameters,omitempty" json:"Parameters,omitempty" yaml:"Parameters,omitempty"`
	SelfBillingSwitchedOn bool         `xml:"SelfBillingSwitchedOn,omitempty" json:"SelfBillingSwitchedOn,omitempty" yaml:"SelfBillingSwitchedOn,omitempty"`
	Status                HeaderStatus `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Trust                 *Trust       `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// BackingReportShift was auto-generated from WSDL.
type BackingReportShift struct {
	ActualBreak     int           `xml:"ActualBreak,omitempty" json:"ActualBreak,omitempty" yaml:"ActualBreak,omitempty"`
	ActualEndTime   DateTime      `xml:"ActualEndTime,omitempty" json:"ActualEndTime,omitempty" yaml:"ActualEndTime,omitempty"`
	ActualStartTime DateTime      `xml:"ActualStartTime,omitempty" json:"ActualStartTime,omitempty" yaml:"ActualStartTime,omitempty"`
	ActualTotalTime DateTime      `xml:"ActualTotalTime,omitempty" json:"ActualTotalTime,omitempty" yaml:"ActualTotalTime,omitempty"`
	AgencyWorker    *AgencyWorker `xml:"AgencyWorker,omitempty" json:"AgencyWorker,omitempty" yaml:"AgencyWorker,omitempty"`
	AssignmentCode  string        `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	BreakInMin      int           `xml:"BreakInMin,omitempty" json:"BreakInMin,omitempty" yaml:"BreakInMin,omitempty"`
	Commission      float64       `xml:"Commission,omitempty" json:"Commission,omitempty" yaml:"Commission,omitempty"`
	EndTime         DateTime      `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	RateType        string        `xml:"RateType,omitempty" json:"RateType,omitempty" yaml:"RateType,omitempty"`
	RefNum          int           `xml:"RefNum,omitempty" json:"RefNum,omitempty" yaml:"RefNum,omitempty"`
	ShiftDate       DateTime      `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	StartTime       DateTime      `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	TotalCost       float64       `xml:"TotalCost,omitempty" json:"TotalCost,omitempty" yaml:"TotalCost,omitempty"`
	TotalTime       DateTime      `xml:"TotalTime,omitempty" json:"TotalTime,omitempty" yaml:"TotalTime,omitempty"`
	Ward            *Ward         `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// BankStaff was auto-generated from WSDL.
type BankStaff struct {
	AssignmentCode *ArrayOfstring       `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	DateOfBirth    DateTime             `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	EmployeeStatus EmploymentStatusEnum `xml:"EmployeeStatus,omitempty" json:"EmployeeStatus,omitempty" yaml:"EmployeeStatus,omitempty"`
	Gender         GenderEnum           `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	NInumber       string               `xml:"NInumber,omitempty" json:"NInumber,omitempty" yaml:"NInumber,omitempty"`
	Name           *Name                `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	PostCode       string               `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	PrimaryTrust   string               `xml:"PrimaryTrust,omitempty" json:"PrimaryTrust,omitempty" yaml:"PrimaryTrust,omitempty"`
	StaffID        string               `xml:"StaffID,omitempty" json:"StaffID,omitempty" yaml:"StaffID,omitempty"`
	TrustCode      *ArrayOfstring       `xml:"TrustCode,omitempty" json:"TrustCode,omitempty" yaml:"TrustCode,omitempty"`
	WebUserID      string               `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
}

// BankStaffFacadeType was auto-generated from WSDL.
type BankStaffFacadeType struct {
	Name    *NameType `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	StaffId string    `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// BoardView was auto-generated from WSDL.
type BoardView struct {
	XMLName     xml.Name               `xml:"http://tempuri.org/ BoardView" json:"-" yaml:"-" action:"http://tempuri.org/IClient/BoardView"`
	RequestId   int                    `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Date        DateTime               `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	DateRange   BoardviewDateRangeEnum `xml:"DateRange,omitempty" json:"DateRange,omitempty" yaml:"DateRange,omitempty"`
	EndDate     DateTime               `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	LocationId  string                 `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	StartDate   DateTime               `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	WardId      string                 `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WebUserId   string                 `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string                 `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// BoardViewResponse was auto-generated from WSDL.
type BoardViewResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shifts           *ArrayOfIVRShiftType `xml:"Shifts,omitempty" json:"Shifts,omitempty" yaml:"Shifts,omitempty"`
	TimeSheets       *ArrayOfTimeSheet    `xml:"TimeSheets,omitempty" json:"TimeSheets,omitempty" yaml:"TimeSheets,omitempty"`
}

// BookStaffRequest was auto-generated from WSDL.
type BookStaffRequest struct {
	XMLName                xml.Name                `xml:"http://tempuri.org/ BookStaffRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/BookStaffRequest"`
	RequestId              int                     `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AssignmentCode         string                  `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	AgencyId               string                  `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	BookingReferenceNumber string                  `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	StaffId                string                  `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebUserId              string                  `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId            string                  `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	FillOptions            RangeRequestFillOptions `xml:"FillOptions,omitempty" json:"FillOptions,omitempty" yaml:"FillOptions,omitempty"`
	IgnoreWarning          bool                    `xml:"IgnoreWarning,omitempty" json:"IgnoreWarning,omitempty" yaml:"IgnoreWarning,omitempty"`
	CallerToken            string                  `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// BookStaffRequestResponse was auto-generated from WSDL.
type BookStaffRequestResponse struct {
	BookStaffRequestResult *BookStaffResponse `xml:"BookStaffRequestResult,omitempty" json:"BookStaffRequestResult,omitempty" yaml:"BookStaffRequestResult,omitempty"`
}

// BookStaffResponse was auto-generated from WSDL.
type BookStaffResponse struct {
	ErrorCode              int                             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string                          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int                             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Induction              *BookStaffResponseTypeInduction `xml:"Induction,omitempty" json:"Induction,omitempty" yaml:"Induction,omitempty"`
	Informational          *Message                        `xml:"Informational,omitempty" json:"Informational,omitempty" yaml:"Informational,omitempty"`
	NonOverridableWarnings *Message                        `xml:"NonOverridableWarnings,omitempty" json:"NonOverridableWarnings,omitempty" yaml:"NonOverridableWarnings,omitempty"`
	OverridableWarnings    *Message                        `xml:"OverridableWarnings,omitempty" json:"OverridableWarnings,omitempty" yaml:"OverridableWarnings,omitempty"`
}

// BookStaffResponseTypeInduction was auto-generated from WSDL.
type BookStaffResponseTypeInduction struct {
	InductionExpiry DateTime        `xml:"InductionExpiry,omitempty" json:"InductionExpiry,omitempty" yaml:"InductionExpiry,omitempty"`
	InductionStatus InductionStatus `xml:"InductionStatus,omitempty" json:"InductionStatus,omitempty" yaml:"InductionStatus,omitempty"`
}

// BookingData was auto-generated from WSDL.
type BookingData struct {
	AssignmentCode   string   `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Email            string   `xml:"Email,omitempty" json:"Email,omitempty" yaml:"Email,omitempty"`
	EngagementStatus string   `xml:"EngagementStatus,omitempty" json:"EngagementStatus,omitempty" yaml:"EngagementStatus,omitempty"`
	FirstName        string   `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	LastName         string   `xml:"LastName,omitempty" json:"LastName,omitempty" yaml:"LastName,omitempty"`
	Location         string   `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	LocationCode     string   `xml:"LocationCode,omitempty" json:"LocationCode,omitempty" yaml:"LocationCode,omitempty"`
	PhoneNumber      string   `xml:"PhoneNumber,omitempty" json:"PhoneNumber,omitempty" yaml:"PhoneNumber,omitempty"`
	ShiftDate        DateTime `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftEndTime     DateTime `xml:"ShiftEndTime,omitempty" json:"ShiftEndTime,omitempty" yaml:"ShiftEndTime,omitempty"`
	ShiftID          int      `xml:"ShiftID,omitempty" json:"ShiftID,omitempty" yaml:"ShiftID,omitempty"`
	ShiftStartTime   DateTime `xml:"ShiftStartTime,omitempty" json:"ShiftStartTime,omitempty" yaml:"ShiftStartTime,omitempty"`
	StaffId          string   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	Trust            string   `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	TrustCode        string   `xml:"TrustCode,omitempty" json:"TrustCode,omitempty" yaml:"TrustCode,omitempty"`
	Ward             string   `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WardCode         string   `xml:"WardCode,omitempty" json:"WardCode,omitempty" yaml:"WardCode,omitempty"`
}

// BookingReason was auto-generated from WSDL.
type BookingReason struct {
	ReasonCode        string `xml:"ReasonCode,omitempty" json:"ReasonCode,omitempty" yaml:"ReasonCode,omitempty"`
	ReasonDescription string `xml:"ReasonDescription,omitempty" json:"ReasonDescription,omitempty" yaml:"ReasonDescription,omitempty"`
}

// CalendarResponse was auto-generated from WSDL.
type CalendarResponse struct {
	ErrorCode                int                             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription         string                          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId               int                             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Availability             *ArrayOfAvailabilityEntry       `xml:"Availability,omitempty" json:"Availability,omitempty" yaml:"Availability,omitempty"`
	CalendarEndDate          DateTime                        `xml:"CalendarEndDate,omitempty" json:"CalendarEndDate,omitempty" yaml:"CalendarEndDate,omitempty"`
	CalendarStartDate        DateTime                        `xml:"CalendarStartDate,omitempty" json:"CalendarStartDate,omitempty" yaml:"CalendarStartDate,omitempty"`
	CancelledBookings        *ArrayOfCancelledBookings       `xml:"CancelledBookings,omitempty" json:"CancelledBookings,omitempty" yaml:"CancelledBookings,omitempty"`
	InformationType          string                          `xml:"InformationType,omitempty" json:"InformationType,omitempty" yaml:"InformationType,omitempty"`
	LeadTime                 *ArrayOfLeadTimeEntry           `xml:"LeadTime,omitempty" json:"LeadTime,omitempty" yaml:"LeadTime,omitempty"`
	RefusedBookings          *ArrayOfRefusedBookings         `xml:"RefusedBookings,omitempty" json:"RefusedBookings,omitempty" yaml:"RefusedBookings,omitempty"`
	Shifts                   *ArrayOfShift                   `xml:"Shifts,omitempty" json:"Shifts,omitempty" yaml:"Shifts,omitempty"`
	StaffId                  string                          `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	TemporaryNonAvailability *ArrayOfTemporalNonAvailability `xml:"TemporaryNonAvailability,omitempty" json:"TemporaryNonAvailability,omitempty" yaml:"TemporaryNonAvailability,omitempty"`
	UnAvailablePeriod        *ArrayOfUnAvailablePeriod       `xml:"UnAvailablePeriod,omitempty" json:"UnAvailablePeriod,omitempty" yaml:"UnAvailablePeriod,omitempty"`
	WorkingPattern           *ArrayOfPatternItem             `xml:"WorkingPattern,omitempty" json:"WorkingPattern,omitempty" yaml:"WorkingPattern,omitempty"`
}

// CancelBookingRequest was auto-generated from WSDL.
type CancelBookingRequest struct {
	XMLName                xml.Name          `xml:"http://tempuri.org/ CancelBookingRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/CancelBookingRequest"`
	RequestId              int               `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CancelOptions          CancelOptionsEnum `xml:"CancelOptions,omitempty" json:"CancelOptions,omitempty" yaml:"CancelOptions,omitempty"`
	ReasonCode             string            `xml:"ReasonCode,omitempty" json:"ReasonCode,omitempty" yaml:"ReasonCode,omitempty"`
	ReasonDescription      string            `xml:"ReasonDescription,omitempty" json:"ReasonDescription,omitempty" yaml:"ReasonDescription,omitempty"`
	BookingReferenceNumber string            `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	WebUserId              string            `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	DeleteRequest          bool              `xml:"DeleteRequest,omitempty" json:"DeleteRequest,omitempty" yaml:"DeleteRequest,omitempty"`
	FlexibleWorkerNotified bool              `xml:"FlexibleWorkerNotified,omitempty" json:"FlexibleWorkerNotified,omitempty" yaml:"FlexibleWorkerNotified,omitempty"`
	RequesterId            string            `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	IgnoreWarning          bool              `xml:"IgnoreWarning,omitempty" json:"IgnoreWarning,omitempty" yaml:"IgnoreWarning,omitempty"`
	IgnoreDNASNCWarning    bool              `xml:"IgnoreDNASNCWarning,omitempty" json:"IgnoreDNASNCWarning,omitempty" yaml:"IgnoreDNASNCWarning,omitempty"`
	CallerToken            string            `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// CancelBookingRequestResponse was auto-generated from WSDL.
type CancelBookingRequestResponse struct {
	CancelBookingRequestResult *CancelBookingResponse `xml:"CancelBookingRequestResult,omitempty" json:"CancelBookingRequestResult,omitempty" yaml:"CancelBookingRequestResult,omitempty"`
}

// CancelBookingResponse was auto-generated from WSDL.
type CancelBookingResponse struct {
	ErrorCode           int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	OverridableWarnings *ArrayOfstring `xml:"OverridableWarnings,omitempty" json:"OverridableWarnings,omitempty" yaml:"OverridableWarnings,omitempty"`
	WarningMessage      string         `xml:"WarningMessage,omitempty" json:"WarningMessage,omitempty" yaml:"WarningMessage,omitempty"`
}

// CancelHolidayBookingResponse was auto-generated from WSDL.
type CancelHolidayBookingResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// CancelledBookings was auto-generated from WSDL.
type CancelledBookings struct {
	AgencyId                        string                      `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	AgencyName                      string                      `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	AssignmentCode                  string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Gender                          string                      `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	HasComment                      bool                        `xml:"HasComment,omitempty" json:"HasComment,omitempty" yaml:"HasComment,omitempty"`
	IsRangeRequest                  bool                        `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	Location                        string                      `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	NoAgency                        bool                        `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Notes                           *ArrayOfCommentsComplexType `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	OverlappingBookingRefNum        *ArrayOfint                 `xml:"OverlappingBookingRefNum,omitempty" json:"OverlappingBookingRefNum,omitempty" yaml:"OverlappingBookingRefNum,omitempty"`
	PartialMatch                    bool                        `xml:"PartialMatch,omitempty" json:"PartialMatch,omitempty" yaml:"PartialMatch,omitempty"`
	RangeFilledBy                   string                      `xml:"RangeFilledBy,omitempty" json:"RangeFilledBy,omitempty" yaml:"RangeFilledBy,omitempty"`
	SecondaryAssignmentCode         string                      `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration int                         `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftBookingReferenceNumber     int                         `xml:"ShiftBookingReferenceNumber,omitempty" json:"ShiftBookingReferenceNumber,omitempty" yaml:"ShiftBookingReferenceNumber,omitempty"`
	ShiftDate                       DateTime                    `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftEndTime                    DateTime                    `xml:"ShiftEndTime,omitempty" json:"ShiftEndTime,omitempty" yaml:"ShiftEndTime,omitempty"`
	ShiftStartTime                  DateTime                    `xml:"ShiftStartTime,omitempty" json:"ShiftStartTime,omitempty" yaml:"ShiftStartTime,omitempty"`
	ShiftStatus                     string                      `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	ShiftTimeType                   string                      `xml:"ShiftTimeType,omitempty" json:"ShiftTimeType,omitempty" yaml:"ShiftTimeType,omitempty"`
	ShiftType                       string                      `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	Trust                           string                      `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward                            string                      `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WardDetails                     *WardDetails                `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
	DeletedBy                       string                      `xml:"DeletedBy,omitempty" json:"DeletedBy,omitempty" yaml:"DeletedBy,omitempty"`
	Reason                          string                      `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
}

// CheckAuthenticatedUserAuthorisationResponse was auto-generated
// from WSDL.
type CheckAuthenticatedUserAuthorisationResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Authorised       bool   `xml:"Authorised,omitempty" json:"Authorised,omitempty" yaml:"Authorised,omitempty"`
	ThirdPartySystem string `xml:"ThirdPartySystem,omitempty" json:"ThirdPartySystem,omitempty" yaml:"ThirdPartySystem,omitempty"`
	Username         string `xml:"Username,omitempty" json:"Username,omitempty" yaml:"Username,omitempty"`
}

// Comments was auto-generated from WSDL.
type Comments struct {
	ByWhen    DateTime `xml:"ByWhen,omitempty" json:"ByWhen,omitempty" yaml:"ByWhen,omitempty"`
	ByWhom    string   `xml:"ByWhom,omitempty" json:"ByWhom,omitempty" yaml:"ByWhom,omitempty"`
	Comment   string   `xml:"Comment,omitempty" json:"Comment,omitempty" yaml:"Comment,omitempty"`
	CommentID string   `xml:"CommentID,omitempty" json:"CommentID,omitempty" yaml:"CommentID,omitempty"`
	Contact   string   `xml:"Contact,omitempty" json:"Contact,omitempty" yaml:"Contact,omitempty"`
}

// CommentsComplexType was auto-generated from WSDL.
type CommentsComplexType struct {
	ByWhom      string   `xml:"ByWhom,omitempty" json:"ByWhom,omitempty" yaml:"ByWhom,omitempty"`
	Comment     string   `xml:"Comment,omitempty" json:"Comment,omitempty" yaml:"Comment,omitempty"`
	CommentedOn DateTime `xml:"CommentedOn,omitempty" json:"CommentedOn,omitempty" yaml:"CommentedOn,omitempty"`
}

// CompletePerformanceEvaluation was auto-generated from WSDL.
type CompletePerformanceEvaluation struct {
	XMLName    xml.Name                             `xml:"http://tempuri.org/ CompletePerformanceEvaluation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/CompletePerformanceEvaluation"`
	Answers    *ArrayOfPerformanceEvaluation_Answer `xml:"Answers,omitempty" json:"Answers,omitempty" yaml:"Answers,omitempty"`
	QuestionID string                               `xml:"QuestionID,omitempty" json:"QuestionID,omitempty" yaml:"QuestionID,omitempty"`
}

// CompletePerformanceEvaluationResponse was auto-generated from
// WSDL.
type CompletePerformanceEvaluationResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ContactsDetailsResponse was auto-generated from WSDL.
type ContactsDetailsResponse struct {
	ErrorCode              int                `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string             `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int                `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Address                *FWAddress         `xml:"Address,omitempty" json:"Address,omitempty" yaml:"Address,omitempty"`
	AvailabilityOptOut     bool               `xml:"AvailabilityOptOut,omitempty" json:"AvailabilityOptOut,omitempty" yaml:"AvailabilityOptOut,omitempty"`
	CancellationOptOut     bool               `xml:"CancellationOptOut,omitempty" json:"CancellationOptOut,omitempty" yaml:"CancellationOptOut,omitempty"`
	Contacts               *ArrayOfFWContacts `xml:"Contacts,omitempty" json:"Contacts,omitempty" yaml:"Contacts,omitempty"`
	EmergencyContactName   string             `xml:"EmergencyContactName,omitempty" json:"EmergencyContactName,omitempty" yaml:"EmergencyContactName,omitempty"`
	EmergencyContactNumber string             `xml:"EmergencyContactNumber,omitempty" json:"EmergencyContactNumber,omitempty" yaml:"EmergencyContactNumber,omitempty"`
	ReminderOptOut         bool               `xml:"ReminderOptOut,omitempty" json:"ReminderOptOut,omitempty" yaml:"ReminderOptOut,omitempty"`
}

// ContractInformationResponse was auto-generated from WSDL.
type ContractInformationResponse struct {
	ErrorCode        int     `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string  `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int     `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	EmploymentHeldAt string  `xml:"EmploymentHeldAt,omitempty" json:"EmploymentHeldAt,omitempty" yaml:"EmploymentHeldAt,omitempty"`
	EngagementStatus string  `xml:"EngagementStatus,omitempty" json:"EngagementStatus,omitempty" yaml:"EngagementStatus,omitempty"`
	HoursContracted  float64 `xml:"HoursContracted,omitempty" json:"HoursContracted,omitempty" yaml:"HoursContracted,omitempty"`
}

// CreateAvailabilityItemResponse was auto-generated from WSDL.
type CreateAvailabilityItemResponse struct {
	ErrorCode          int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription   string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId         int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AvailabilityItemId *ArrayOfstring `xml:"AvailabilityItemId,omitempty" json:"AvailabilityItemId,omitempty" yaml:"AvailabilityItemId,omitempty"`
	GroupId            int            `xml:"GroupId,omitempty" json:"GroupId,omitempty" yaml:"GroupId,omitempty"`
}

// CreateHolidayBookingResponse was auto-generated from WSDL.
type CreateHolidayBookingResponse struct {
	ErrorCode              int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BookingReferenceNumber int    `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
}

// CreateLeadTimesResponse was auto-generated from WSDL.
type CreateLeadTimesResponse struct {
	ErrorCode        int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GroupIDs         *ArrayOfstring `xml:"GroupIDs,omitempty" json:"GroupIDs,omitempty" yaml:"GroupIDs,omitempty"`
	LeadTimeIDs      *ArrayOfstring `xml:"LeadTimeIDs,omitempty" json:"LeadTimeIDs,omitempty" yaml:"LeadTimeIDs,omitempty"`
}

// CreateShiftRequest was auto-generated from WSDL.
type CreateShiftRequest struct {
	XMLName                         xml.Name          `xml:"http://tempuri.org/ CreateShiftRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/CreateShiftRequest"`
	RequestId                       int               `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyID                        string            `xml:"AgencyID,omitempty" json:"AgencyID,omitempty" yaml:"AgencyID,omitempty"`
	AssignmentCode                  string            `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	AuthorizationCode               string            `xml:"AuthorizationCode,omitempty" json:"AuthorizationCode,omitempty" yaml:"AuthorizationCode,omitempty"`
	Date                            DateTime          `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	EndDateOfRange                  DateTime          `xml:"EndDateOfRange,omitempty" json:"EndDateOfRange,omitempty" yaml:"EndDateOfRange,omitempty"`
	EndTime                         DateTime          `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	IgnoreWarning                   bool              `xml:"IgnoreWarning,omitempty" json:"IgnoreWarning,omitempty" yaml:"IgnoreWarning,omitempty"`
	LocationID                      string            `xml:"LocationID,omitempty" json:"LocationID,omitempty" yaml:"LocationID,omitempty"`
	Notes                           *ArrayOfNote      `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	OneFW                           bool              `xml:"OneFW,omitempty" json:"OneFW,omitempty" yaml:"OneFW,omitempty"`
	ReasonCode                      string            `xml:"ReasonCode,omitempty" json:"ReasonCode,omitempty" yaml:"ReasonCode,omitempty"`
	RemoveDates                     *ArrayOfdateTime  `xml:"RemoveDates,omitempty" json:"RemoveDates,omitempty" yaml:"RemoveDates,omitempty"`
	RemoveDays                      *ArrayOfstring    `xml:"RemoveDays,omitempty" json:"RemoveDays,omitempty" yaml:"RemoveDays,omitempty"`
	RequestMethod                   RequestMethodEnum `xml:"RequestMethod,omitempty" json:"RequestMethod,omitempty" yaml:"RequestMethod,omitempty"`
	RequestorWebUserID              string            `xml:"RequestorWebUserID,omitempty" json:"RequestorWebUserID,omitempty" yaml:"RequestorWebUserID,omitempty"`
	SecondaryAssignmentCode         string            `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration string            `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	Shift_Type                      ShiftType         `xml:"Shift_Type,omitempty" json:"Shift_Type,omitempty" yaml:"Shift_Type,omitempty"`
	StaffID                         string            `xml:"StaffID,omitempty" json:"StaffID,omitempty" yaml:"StaffID,omitempty"`
	StartTime                       DateTime          `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	TrustID                         string            `xml:"TrustID,omitempty" json:"TrustID,omitempty" yaml:"TrustID,omitempty"`
	WardID                          string            `xml:"WardID,omitempty" json:"WardID,omitempty" yaml:"WardID,omitempty"`
	Gender                          GenderEnum        `xml:"gender,omitempty" json:"gender,omitempty" yaml:"gender,omitempty"`
	IsLocked                        bool              `xml:"isLocked,omitempty" json:"isLocked,omitempty" yaml:"isLocked,omitempty"`
	NoAgency                        bool              `xml:"noAgency,omitempty" json:"noAgency,omitempty" yaml:"noAgency,omitempty"`
	RequesterId                     string            `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken                     string            `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// CreateShiftRequestResponse was auto-generated from WSDL.
type CreateShiftRequestResponse struct {
	CreateShiftRequestResult *CreateShiftResponse `xml:"CreateShiftRequestResult,omitempty" json:"CreateShiftRequestResult,omitempty" yaml:"CreateShiftRequestResult,omitempty"`
}

// CreateShiftResponse was auto-generated from WSDL.
type CreateShiftResponse struct {
	ErrorCode                          int                             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription                   string                          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                         int                             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BookingReferenceNumber             string                          `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	BookingResponse                    *BookStaffResponseTypeInduction `xml:"BookingResponse,omitempty" json:"BookingResponse,omitempty" yaml:"BookingResponse,omitempty"`
	IsSeniorAdminAuthorisationRequired bool                            `xml:"IsSeniorAdminAuthorisationRequired,omitempty" json:"IsSeniorAdminAuthorisationRequired,omitempty" yaml:"IsSeniorAdminAuthorisationRequired,omitempty"`
	NonOverridableWarnings             *ArrayOfstring                  `xml:"NonOverridableWarnings,omitempty" json:"NonOverridableWarnings,omitempty" yaml:"NonOverridableWarnings,omitempty"`
	OverridableWarnings                *ArrayOfstring                  `xml:"OverridableWarnings,omitempty" json:"OverridableWarnings,omitempty" yaml:"OverridableWarnings,omitempty"`
}

// CreateUnavailablePeriodResponse was auto-generated from WSDL.
type CreateUnavailablePeriodResponse struct {
	ErrorCode                  int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription           string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                 int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	LatestEndDate              DateTime       `xml:"LatestEndDate,omitempty" json:"LatestEndDate,omitempty" yaml:"LatestEndDate,omitempty"`
	TemporaryNonAvailabilityId int            `xml:"TemporaryNonAvailabilityId,omitempty" json:"TemporaryNonAvailabilityId,omitempty" yaml:"TemporaryNonAvailabilityId,omitempty"`
	UnavailablePeriodId        *ArrayOfstring `xml:"UnavailablePeriodId,omitempty" json:"UnavailablePeriodId,omitempty" yaml:"UnavailablePeriodId,omitempty"`
}

// DNASNCNotes was auto-generated from WSDL.
type DNASNCNotes struct {
	ID    int    `xml:"ID,omitempty" json:"ID,omitempty" yaml:"ID,omitempty"`
	Notes string `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
}

// DNASNCWarnings was auto-generated from WSDL.
type DNASNCWarnings struct {
	ExpiryDate      DateTime     `xml:"ExpiryDate,omitempty" json:"ExpiryDate,omitempty" yaml:"ExpiryDate,omitempty"`
	HrReview        *HrReview    `xml:"HrReview,omitempty" json:"HrReview,omitempty" yaml:"HrReview,omitempty"`
	IsUnderHRReview bool         `xml:"IsUnderHRReview,omitempty" json:"IsUnderHRReview,omitempty" yaml:"IsUnderHRReview,omitempty"`
	RefNum          int          `xml:"RefNum,omitempty" json:"RefNum,omitempty" yaml:"RefNum,omitempty"`
	ShiftDate       DateTime     `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	WarningLevel    WarningLevel `xml:"WarningLevel,omitempty" json:"WarningLevel,omitempty" yaml:"WarningLevel,omitempty"`
	WarningSentOn   DateTime     `xml:"WarningSentOn,omitempty" json:"WarningSentOn,omitempty" yaml:"WarningSentOn,omitempty"`
	WarningType     DNASNC       `xml:"WarningType,omitempty" json:"WarningType,omitempty" yaml:"WarningType,omitempty"`
}

// DNAorSNCContactType was auto-generated from WSDL.
type DNAorSNCContactType struct {
	Email           string `xml:"Email,omitempty" json:"Email,omitempty" yaml:"Email,omitempty"`
	FaxNumber       string `xml:"FaxNumber,omitempty" json:"FaxNumber,omitempty" yaml:"FaxNumber,omitempty"`
	TelephoneNumber string `xml:"TelephoneNumber,omitempty" json:"TelephoneNumber,omitempty" yaml:"TelephoneNumber,omitempty"`
}

// DNAorSNCFromAddressType was auto-generated from WSDL.
type DNAorSNCFromAddressType struct {
	AddressLine1 string `xml:"AddressLine1,omitempty" json:"AddressLine1,omitempty" yaml:"AddressLine1,omitempty"`
	AddressLine2 string `xml:"AddressLine2,omitempty" json:"AddressLine2,omitempty" yaml:"AddressLine2,omitempty"`
	County       string `xml:"County,omitempty" json:"County,omitempty" yaml:"County,omitempty"`
	PostCode     string `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	Town         string `xml:"Town,omitempty" json:"Town,omitempty" yaml:"Town,omitempty"`
}

// DNAorSNCLetter was auto-generated from WSDL.
type DNAorSNCLetter struct {
	FileName             string   `xml:"FileName,omitempty" json:"FileName,omitempty" yaml:"FileName,omitempty"`
	LetterBody           string   `xml:"LetterBody,omitempty" json:"LetterBody,omitempty" yaml:"LetterBody,omitempty"`
	LetterDate           DateTime `xml:"LetterDate,omitempty" json:"LetterDate,omitempty" yaml:"LetterDate,omitempty"`
	LetterSender         string   `xml:"LetterSender,omitempty" json:"LetterSender,omitempty" yaml:"LetterSender,omitempty"`
	LetterSenderPosition string   `xml:"LetterSenderPosition,omitempty" json:"LetterSenderPosition,omitempty" yaml:"LetterSenderPosition,omitempty"`
	LogoImageFile        []byte   `xml:"LogoImageFile,omitempty" json:"LogoImageFile,omitempty" yaml:"LogoImageFile,omitempty"`
	Ref                  string   `xml:"Ref,omitempty" json:"Ref,omitempty" yaml:"Ref,omitempty"`
	ReportSubject        string   `xml:"ReportSubject,omitempty" json:"ReportSubject,omitempty" yaml:"ReportSubject,omitempty"`
	Subject              string   `xml:"Subject,omitempty" json:"Subject,omitempty" yaml:"Subject,omitempty"`
}

// DayDefinitionsComplexType was auto-generated from WSDL.
type DayDefinitionsComplexType struct {
	EndDay    string   `xml:"EndDay,omitempty" json:"EndDay,omitempty" yaml:"EndDay,omitempty"`
	EndTime   DateTime `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	SplitName string   `xml:"SplitName,omitempty" json:"SplitName,omitempty" yaml:"SplitName,omitempty"`
	StartDay  string   `xml:"StartDay,omitempty" json:"StartDay,omitempty" yaml:"StartDay,omitempty"`
	StartTime DateTime `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
}

// DeleteAvailabilityItemResponse was auto-generated from WSDL.
type DeleteAvailabilityItemResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// DeleteContactDetailsResponse was auto-generated from WSDL.
type DeleteContactDetailsResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// DeleteLeadTimeResponse was auto-generated from WSDL.
type DeleteLeadTimeResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// DeleteShiftRequest was auto-generated from WSDL.
type DeleteShiftRequest struct {
	XMLName                xml.Name `xml:"http://tempuri.org/ DeleteShiftRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/DeleteShiftRequest"`
	RequestId              int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BookingReferenceNumber string   `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	FlexibleWorkerNotified bool     `xml:"FlexibleWorkerNotified,omitempty" json:"FlexibleWorkerNotified,omitempty" yaml:"FlexibleWorkerNotified,omitempty"`
	ReasonCode             string   `xml:"ReasonCode,omitempty" json:"ReasonCode,omitempty" yaml:"ReasonCode,omitempty"`
	RequesterId            string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	WebUserID              string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	IsPendingAuthorisation bool     `xml:"IsPendingAuthorisation,omitempty" json:"IsPendingAuthorisation,omitempty" yaml:"IsPendingAuthorisation,omitempty"`
	CallerToken            string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// DeleteShiftRequestResponse was auto-generated from WSDL.
type DeleteShiftRequestResponse struct {
	DeleteShiftRequestResult *DeleteShiftResponse `xml:"DeleteShiftRequestResult,omitempty" json:"DeleteShiftRequestResult,omitempty" yaml:"DeleteShiftRequestResult,omitempty"`
}

// DeleteShiftResponse was auto-generated from WSDL.
type DeleteShiftResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// DeleteUnavailablePeriodResponse was auto-generated from WSDL.
type DeleteUnavailablePeriodResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// EditContactDetailsResponse was auto-generated from WSDL.
type EditContactDetailsResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// EditRightToWorkResponse was auto-generated from WSDL.
type EditRightToWorkResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// EvidenceDetail was auto-generated from WSDL.
type EvidenceDetail struct {
	Checked    bool   `xml:"Checked,omitempty" json:"Checked,omitempty" yaml:"Checked,omitempty"`
	Evidence   string `xml:"Evidence,omitempty" json:"Evidence,omitempty" yaml:"Evidence,omitempty"`
	EvidenceId int    `xml:"EvidenceId,omitempty" json:"EvidenceId,omitempty" yaml:"EvidenceId,omitempty"`
}

// ExactHolidayBookingResponse was auto-generated from WSDL.
type ExactHolidayBookingResponse struct {
	ErrorCode        int             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	HolidayBooking   *HolidayBooking `xml:"HolidayBooking,omitempty" json:"HolidayBooking,omitempty" yaml:"HolidayBooking,omitempty"`
}

// ExactShiftRequest was auto-generated from WSDL.
type ExactShiftRequest struct {
	XMLName                xml.Name `xml:"http://tempuri.org/ ExactShiftRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ExactShiftRequest"`
	RequestId              int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId              string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	BookingReferenceNumber string   `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	CallerToken            string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ExactShiftRequestResponse was auto-generated from WSDL.
type ExactShiftRequestResponse struct {
	ExactShiftRequestResult *ExactShiftResponse `xml:"ExactShiftRequestResult,omitempty" json:"ExactShiftRequestResult,omitempty" yaml:"ExactShiftRequestResult,omitempty"`
}

// ExactShiftResponse was auto-generated from WSDL.
type ExactShiftResponse struct {
	ErrorCode        int           `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string        `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int           `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shift            *IVRShiftType `xml:"Shift,omitempty" json:"Shift,omitempty" yaml:"Shift,omitempty"`
}

// ExactStaffRequest was auto-generated from WSDL.
type ExactStaffRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ ExactStaffRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ExactStaffRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffId     string   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ExactStaffRequestResponse was auto-generated from WSDL.
type ExactStaffRequestResponse struct {
	ExactStaffRequestResult *ExactStaffResponse `xml:"ExactStaffRequestResult,omitempty" json:"ExactStaffRequestResult,omitempty" yaml:"ExactStaffRequestResult,omitempty"`
}

// ExactStaffResponse was auto-generated from WSDL.
type ExactStaffResponse struct {
	ErrorCode        int          `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string       `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int          `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AgencyResponse   *AgencyStaff `xml:"AgencyResponse,omitempty" json:"AgencyResponse,omitempty" yaml:"AgencyResponse,omitempty"`
	BankResponse     *BankStaff   `xml:"BankResponse,omitempty" json:"BankResponse,omitempty" yaml:"BankResponse,omitempty"`
}

// ExactUserRequest was auto-generated from WSDL.
type ExactUserRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ ExactUserRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ExactUserRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ExactUserRequestResponse was auto-generated from WSDL.
type ExactUserRequestResponse struct {
	ExactUserRequestResult *ExactUserResponse `xml:"ExactUserRequestResult,omitempty" json:"ExactUserRequestResult,omitempty" yaml:"ExactUserRequestResult,omitempty"`
}

// ExactUserResponse was auto-generated from WSDL.
type ExactUserResponse struct {
	ErrorCode          int             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription   string          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId         int             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AgencyUserTypeResp *AgencyUserType `xml:"AgencyUserTypeResp,omitempty" json:"AgencyUserTypeResp,omitempty" yaml:"AgencyUserTypeResp,omitempty"`
	WardUserTypeResp   *WardUserType   `xml:"WardUserTypeResp,omitempty" json:"WardUserTypeResp,omitempty" yaml:"WardUserTypeResp,omitempty"`
}

// ExternalUser was auto-generated from WSDL.
type ExternalUser struct {
	EmailID   string `xml:"EmailID,omitempty" json:"EmailID,omitempty" yaml:"EmailID,omitempty"`
	FirstName string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	Surname   string `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
}

// FWAddress was auto-generated from WSDL.
type FWAddress struct {
	Address1   string `xml:"Address1,omitempty" json:"Address1,omitempty" yaml:"Address1,omitempty"`
	Address2   string `xml:"Address2,omitempty" json:"Address2,omitempty" yaml:"Address2,omitempty"`
	Address3   string `xml:"Address3,omitempty" json:"Address3,omitempty" yaml:"Address3,omitempty"`
	Country    string `xml:"Country,omitempty" json:"Country,omitempty" yaml:"Country,omitempty"`
	County     string `xml:"County,omitempty" json:"County,omitempty" yaml:"County,omitempty"`
	PostalCode string `xml:"PostalCode,omitempty" json:"PostalCode,omitempty" yaml:"PostalCode,omitempty"`
	Town       string `xml:"Town,omitempty" json:"Town,omitempty" yaml:"Town,omitempty"`
}

// FWCancelReason was auto-generated from WSDL.
type FWCancelReason struct {
	Code        string `xml:"Code,omitempty" json:"Code,omitempty" yaml:"Code,omitempty"`
	Description string `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
}

// FWContacts was auto-generated from WSDL.
type FWContacts struct {
	Contact     string   `xml:"Contact,omitempty" json:"Contact,omitempty" yaml:"Contact,omitempty"`
	ContactType string   `xml:"ContactType,omitempty" json:"ContactType,omitempty" yaml:"ContactType,omitempty"`
	EndTime     DateTime `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	StartTime   DateTime `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	UseContact  bool     `xml:"UseContact,omitempty" json:"UseContact,omitempty" yaml:"UseContact,omitempty"`
}

// FWDetails was auto-generated from WSDL.
type FWDetails struct {
	FirstName string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	StaffID   string `xml:"StaffID,omitempty" json:"StaffID,omitempty" yaml:"StaffID,omitempty"`
	Surname   string `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
}

// FWProfile was auto-generated from WSDL.
type FWProfile struct {
	Assignments *ArrayOfstring `xml:"Assignments,omitempty" json:"Assignments,omitempty" yaml:"Assignments,omitempty"`
	Name        *Name          `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	StaffId     string         `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	Trusts      *ArrayOfstring `xml:"Trusts,omitempty" json:"Trusts,omitempty" yaml:"Trusts,omitempty"`
	Wards       *ArrayOfWard   `xml:"Wards,omitempty" json:"Wards,omitempty" yaml:"Wards,omitempty"`
	WebUserID   string         `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
}

// FailedFillResponse was auto-generated from WSDL.
type FailedFillResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// FlexibleWorker was auto-generated from WSDL.
type FlexibleWorker struct {
	Assignments      *ArrayOfstring `xml:"Assignments,omitempty" json:"Assignments,omitempty" yaml:"Assignments,omitempty"`
	DateOfBirth      DateTime       `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	EngagementStatus string         `xml:"EngagementStatus,omitempty" json:"EngagementStatus,omitempty" yaml:"EngagementStatus,omitempty"`
	FirstName        string         `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	Gender           string         `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	MiddleName       string         `xml:"MiddleName,omitempty" json:"MiddleName,omitempty" yaml:"MiddleName,omitempty"`
	NINumber         string         `xml:"NINumber,omitempty" json:"NINumber,omitempty" yaml:"NINumber,omitempty"`
	PostCode         string         `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	PrimaryTrust     string         `xml:"PrimaryTrust,omitempty" json:"PrimaryTrust,omitempty" yaml:"PrimaryTrust,omitempty"`
	StaffId          string         `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	Surname          string         `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
	Trusts           *ArrayOfstring `xml:"Trusts,omitempty" json:"Trusts,omitempty" yaml:"Trusts,omitempty"`
	Wards            *ArrayOfWard   `xml:"Wards,omitempty" json:"Wards,omitempty" yaml:"Wards,omitempty"`
	WebUserId        string         `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
}

// FlexibleWorkerTimeSheetResponse was auto-generated from WSDL.
type FlexibleWorkerTimeSheetResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TimeSheets       *ArrayOfTimeSheet `xml:"TimeSheets,omitempty" json:"TimeSheets,omitempty" yaml:"TimeSheets,omitempty"`
}

// ForwardPerformanceEvaluation was auto-generated from WSDL.
type ForwardPerformanceEvaluation struct {
	XMLName            xml.Name      `xml:"http://tempuri.org/ ForwardPerformanceEvaluation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ForwardPerformanceEvaluation"`
	RequestId          int           `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	ExternalUser       *ExternalUser `xml:"ExternalUser,omitempty" json:"ExternalUser,omitempty" yaml:"ExternalUser,omitempty"`
	OtherUserID        string        `xml:"OtherUserID,omitempty" json:"OtherUserID,omitempty" yaml:"OtherUserID,omitempty"`
	ReasonToRefuse     string        `xml:"ReasonToRefuse,omitempty" json:"ReasonToRefuse,omitempty" yaml:"ReasonToRefuse,omitempty"`
	ReferenceNumber    int           `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	ReturnBackToSender bool          `xml:"ReturnBackToSender,omitempty" json:"ReturnBackToSender,omitempty" yaml:"ReturnBackToSender,omitempty"`
	WebUserID          string        `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	RequesterId        string        `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken        string        `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ForwardPerformanceEvaluationResponse was auto-generated from
// WSDL.
type ForwardPerformanceEvaluationResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// GeneralLeadTimeResponse was auto-generated from WSDL.
type GeneralLeadTimeResponse struct {
	ErrorCode        int     `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string  `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int     `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GeneralLeadTime  float64 `xml:"GeneralLeadTime,omitempty" json:"GeneralLeadTime,omitempty" yaml:"GeneralLeadTime,omitempty"`
	StaffId          string  `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// GetActivateReasonsResponse was auto-generated from WSDL.
type GetActivateReasonsResponse struct {
	ErrorCode        int                     `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                  `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                     `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ActivateReasons  *ArrayOfActivateReasons `xml:"ActivateReasons,omitempty" json:"ActivateReasons,omitempty" yaml:"ActivateReasons,omitempty"`
}

// GetAgencies was auto-generated from WSDL.
type GetAgencies struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetAgencies" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetAgencies"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserID   string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetAgenciesResponse was auto-generated from WSDL.
type GetAgenciesResponse struct {
	GetAgenciesResult *AgencyResponse `xml:"GetAgenciesResult,omitempty" json:"GetAgenciesResult,omitempty" yaml:"GetAgenciesResult,omitempty"`
}

// GetAgenciesUsedByTrust was auto-generated from WSDL.
type GetAgenciesUsedByTrust struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetAgenciesUsedByTrust" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetAgenciesUsedByTrust"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyCode  string   `xml:"AgencyCode,omitempty" json:"AgencyCode,omitempty" yaml:"AgencyCode,omitempty"`
	AreaOfWork  string   `xml:"AreaOfWork,omitempty" json:"AreaOfWork,omitempty" yaml:"AreaOfWork,omitempty"`
	Assignment  string   `xml:"Assignment,omitempty" json:"Assignment,omitempty" yaml:"Assignment,omitempty"`
	Location    string   `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	StaffGroup  string   `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	TrustCode   string   `xml:"TrustCode,omitempty" json:"TrustCode,omitempty" yaml:"TrustCode,omitempty"`
	Ward        string   `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WebUserID   string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetAgenciesUsedByTrustResponse was auto-generated from WSDL.
type GetAgenciesUsedByTrustResponse struct {
	GetAgenciesUsedByTrustResult *AgenciesUsedByTrustResponse `xml:"GetAgenciesUsedByTrustResult,omitempty" json:"GetAgenciesUsedByTrustResult,omitempty" yaml:"GetAgenciesUsedByTrustResult,omitempty"`
}

// GetAgencyRateContracts was auto-generated from WSDL.
type GetAgencyRateContracts struct {
	XMLName     xml.Name              `xml:"http://tempuri.org/ GetAgencyRateContracts" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetAgencyRateContracts"`
	RequestId   int                   `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	EndDate     DateTime              `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate   DateTime              `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	Status      AgencyRateCardsStatus `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	WebUserID   string                `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string                `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetAgencyRateContractsResponse was auto-generated from WSDL.
type GetAgencyRateContractsResponse struct {
	GetAgencyRateContractsResult *AgencyRateContractResponse `xml:"GetAgencyRateContractsResult,omitempty" json:"GetAgencyRateContractsResult,omitempty" yaml:"GetAgencyRateContractsResult,omitempty"`
}

// GetAvailableShifts was auto-generated from WSDL.
type GetAvailableShifts struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetAvailableShifts" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetAvailableShifts"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffId     string   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	StartDate   DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	EndDate     DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetAvailableShiftsResponse was auto-generated from WSDL.
type GetAvailableShiftsResponse struct {
	ErrorCode        int           `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string        `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int           `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shifts           *ArrayOfShift `xml:"Shifts,omitempty" json:"Shifts,omitempty" yaml:"Shifts,omitempty"`
}

// GetAwaitingAuthorisationTimeSheetResponse was auto-generated
// from WSDL.
type GetAwaitingAuthorisationTimeSheetResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TimeSheets       *ArrayOfTimeSheet `xml:"TimeSheets,omitempty" json:"TimeSheets,omitempty" yaml:"TimeSheets,omitempty"`
}

// GetAwaitingAuthorisationTimesheets was auto-generated from WSDL.
type GetAwaitingAuthorisationTimesheets struct {
	XMLName                xml.Name `xml:"http://tempuri.org/ GetAwaitingAuthorisationTimesheets" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetAwaitingAuthorisationTimesheets"`
	RequestId              int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId              string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	BookingReferenceNumber int      `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	CallerToken            string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetAwaitingAuthorisationTimesheetsResponse was auto-generated
// from WSDL.
type GetAwaitingAuthorisationTimesheetsResponse struct {
	GetAwaitingAuthorisationTimesheetsResult *GetAwaitingAuthorisationTimeSheetResponse `xml:"GetAwaitingAuthorisationTimesheetsResult,omitempty" json:"GetAwaitingAuthorisationTimesheetsResult,omitempty" yaml:"GetAwaitingAuthorisationTimesheetsResult,omitempty"`
}

// GetBackDatedShifts was auto-generated from WSDL.
type GetBackDatedShifts struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetBackDatedShifts" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetBackDatedShifts"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	ContractID  int      `xml:"ContractID,omitempty" json:"ContractID,omitempty" yaml:"ContractID,omitempty"`
	WebUserID   string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetBackDatedShiftsResponse was auto-generated from WSDL.
type GetBackDatedShiftsResponse struct {
	ErrorCode        int                     `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                  `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                     `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BackDatedShifts  *ArrayOfBackDatedShifts `xml:"BackDatedShifts,omitempty" json:"BackDatedShifts,omitempty" yaml:"BackDatedShifts,omitempty"`
}

// GetBackingReport was auto-generated from WSDL.
type GetBackingReport struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetBackingReport" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetBackingReport"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BackingReportId int      `xml:"BackingReportId,omitempty" json:"BackingReportId,omitempty" yaml:"BackingReportId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetBackingReportResponse was auto-generated from WSDL.
type GetBackingReportResponse struct {
	ErrorCode           int                        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string                     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int                        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BackingReportShifts *ArrayOfBackingReportShift `xml:"BackingReportShifts,omitempty" json:"BackingReportShifts,omitempty" yaml:"BackingReportShifts,omitempty"`
	FrameworkCharge     float64                    `xml:"FrameworkCharge,omitempty" json:"FrameworkCharge,omitempty" yaml:"FrameworkCharge,omitempty"`
	Total               float64                    `xml:"Total,omitempty" json:"Total,omitempty" yaml:"Total,omitempty"`
}

// GetCancellationReasons was auto-generated from WSDL.
type GetCancellationReasons struct {
	XMLName     xml.Name          `xml:"http://tempuri.org/ GetCancellationReasons" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetCancellationReasons"`
	RequestId   int               `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Reason      ReasonOptionsEnum `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	CallerToken string            `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetCancellationReasonsResponse was auto-generated from WSDL.
type GetCancellationReasonsResponse struct {
	ErrorCode           int             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	CancellationReasons *ArrayOfReasons `xml:"CancellationReasons,omitempty" json:"CancellationReasons,omitempty" yaml:"CancellationReasons,omitempty"`
}

// GetContractInformation was auto-generated from WSDL.
type GetContractInformation struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetContractInformation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetContractInformation"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffId     string   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetContractInformationResponse was auto-generated from WSDL.
type GetContractInformationResponse struct {
	GetContractInformationResult *ContractInformationResponse `xml:"GetContractInformationResult,omitempty" json:"GetContractInformationResult,omitempty" yaml:"GetContractInformationResult,omitempty"`
}

// GetDNAandSNCWarningLetter was auto-generated from WSDL.
type GetDNAandSNCWarningLetter struct {
	DNAorSNCLetter  *DNAorSNCLetter  `xml:"DNAorSNCLetter,omitempty" json:"DNAorSNCLetter,omitempty" yaml:"DNAorSNCLetter,omitempty"`
	ExpiryDate      DateTime         `xml:"ExpiryDate,omitempty" json:"ExpiryDate,omitempty" yaml:"ExpiryDate,omitempty"`
	ReceiverDetails *ReceiverDetails `xml:"ReceiverDetails,omitempty" json:"ReceiverDetails,omitempty" yaml:"ReceiverDetails,omitempty"`
	RefNum          int              `xml:"RefNum,omitempty" json:"RefNum,omitempty" yaml:"RefNum,omitempty"`
	SenderDetails   *SenderDetails   `xml:"SenderDetails,omitempty" json:"SenderDetails,omitempty" yaml:"SenderDetails,omitempty"`
	ShiftDate       DateTime         `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	WarningLevel    WarningLevel     `xml:"WarningLevel,omitempty" json:"WarningLevel,omitempty" yaml:"WarningLevel,omitempty"`
	WarningSentOn   DateTime         `xml:"WarningSentOn,omitempty" json:"WarningSentOn,omitempty" yaml:"WarningSentOn,omitempty"`
	WarningType     DNASNC           `xml:"WarningType,omitempty" json:"WarningType,omitempty" yaml:"WarningType,omitempty"`
}

// GetDNAandSNCWarningLetterResponse was auto-generated from WSDL.
type GetDNAandSNCWarningLetterResponse struct {
	ErrorCode                 int                        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription          string                     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                int                        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GetDNAandSNCWarningLetter *GetDNAandSNCWarningLetter `xml:"GetDNAandSNCWarningLetter,omitempty" json:"GetDNAandSNCWarningLetter,omitempty" yaml:"GetDNAandSNCWarningLetter,omitempty"`
}

// GetDNAandSNCWarningsResponse was auto-generated from WSDL.
type GetDNAandSNCWarningsResponse struct {
	ErrorCode        int                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	DNASNCWarnings   *ArrayOfDNASNCWarnings `xml:"DNASNCWarnings,omitempty" json:"DNASNCWarnings,omitempty" yaml:"DNASNCWarnings,omitempty"`
}

// GetFlexibleWorkerResponse was auto-generated from WSDL.
type GetFlexibleWorkerResponse struct {
	ErrorCode        int             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	FW               *FlexibleWorker `xml:"FW,omitempty" json:"FW,omitempty" yaml:"FW,omitempty"`
}

// GetInvoiceBackingReportsList was auto-generated from WSDL.
type GetInvoiceBackingReportsList struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetInvoiceBackingReportsList" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetInvoiceBackingReportsList"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Agency      int      `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	EndDate     DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate   DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	WebUserID   string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetInvoiceBackingReportsListResponse was auto-generated from
// WSDL.
type GetInvoiceBackingReportsListResponse struct {
	ErrorCode        int                   `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                   `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BackingReports   *ArrayOfBackingReport `xml:"BackingReports,omitempty" json:"BackingReports,omitempty" yaml:"BackingReports,omitempty"`
}

// GetInvoiceDetails was auto-generated from WSDL.
type GetInvoiceDetails struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetInvoiceDetails" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetInvoiceDetails"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BackingReportId int      `xml:"BackingReportId,omitempty" json:"BackingReportId,omitempty" yaml:"BackingReportId,omitempty"`
	InvoicedId      int      `xml:"InvoicedId,omitempty" json:"InvoicedId,omitempty" yaml:"InvoicedId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetInvoiceDetailsReportPdf was auto-generated from WSDL.
type GetInvoiceDetailsReportPdf struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetInvoiceDetailsReportPdf" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetInvoiceDetailsReportPdf"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BackingReportId int      `xml:"BackingReportId,omitempty" json:"BackingReportId,omitempty" yaml:"BackingReportId,omitempty"`
	InvoiceId       int      `xml:"InvoiceId,omitempty" json:"InvoiceId,omitempty" yaml:"InvoiceId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetInvoiceDetailsReportPdfResponse was auto-generated from WSDL.
type GetInvoiceDetailsReportPdfResponse struct {
	ErrorCode               int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription        string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId              int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	InvoiceDetailsReportPdf []byte `xml:"InvoiceDetailsReportPdf,omitempty" json:"InvoiceDetailsReportPdf,omitempty" yaml:"InvoiceDetailsReportPdf,omitempty"`
}

// GetInvoiceDetailsResponse was auto-generated from WSDL.
type GetInvoiceDetailsResponse struct {
	ErrorCode                 int                   `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription          string                `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                int                   `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Agency                    *Agency               `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	DateofInvoice             DateTime              `xml:"DateofInvoice,omitempty" json:"DateofInvoice,omitempty" yaml:"DateofInvoice,omitempty"`
	FinalTotalValue           float64               `xml:"FinalTotalValue,omitempty" json:"FinalTotalValue,omitempty" yaml:"FinalTotalValue,omitempty"`
	FrameworkManagementCharge float64               `xml:"FrameworkManagementCharge,omitempty" json:"FrameworkManagementCharge,omitempty" yaml:"FrameworkManagementCharge,omitempty"`
	InvoiceDetails            *ArrayOfInvoicedetail `xml:"InvoiceDetails,omitempty" json:"InvoiceDetails,omitempty" yaml:"InvoiceDetails,omitempty"`
	InvoiceNumber             string                `xml:"InvoiceNumber,omitempty" json:"InvoiceNumber,omitempty" yaml:"InvoiceNumber,omitempty"`
	InvoiceReceiptDate        DateTime              `xml:"InvoiceReceiptDate,omitempty" json:"InvoiceReceiptDate,omitempty" yaml:"InvoiceReceiptDate,omitempty"`
	TotalCommission           float64               `xml:"TotalCommission,omitempty" json:"TotalCommission,omitempty" yaml:"TotalCommission,omitempty"`
	TotalCostwithCommission   float64               `xml:"TotalCostwithCommission,omitempty" json:"TotalCostwithCommission,omitempty" yaml:"TotalCostwithCommission,omitempty"`
	TotalShiftCost            float64               `xml:"TotalShiftCost,omitempty" json:"TotalShiftCost,omitempty" yaml:"TotalShiftCost,omitempty"`
	Trust                     *Trust                `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	VATCharged                float64               `xml:"VATCharged,omitempty" json:"VATCharged,omitempty" yaml:"VATCharged,omitempty"`
}

// GetInvoiceSummaryReportPdf was auto-generated from WSDL.
type GetInvoiceSummaryReportPdf struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetInvoiceSummaryReportPdf" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetInvoiceSummaryReportPdf"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BackingReportId int      `xml:"BackingReportId,omitempty" json:"BackingReportId,omitempty" yaml:"BackingReportId,omitempty"`
	InvoiceId       int      `xml:"InvoiceId,omitempty" json:"InvoiceId,omitempty" yaml:"InvoiceId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetInvoiceSummaryReportPdfResponse was auto-generated from WSDL.
type GetInvoiceSummaryReportPdfResponse struct {
	ErrorCode               int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription        string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId              int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	InvoiceSummaryReportPdf []byte `xml:"InvoiceSummaryReportPdf,omitempty" json:"InvoiceSummaryReportPdf,omitempty" yaml:"InvoiceSummaryReportPdf,omitempty"`
}

// GetJobCodesForTrust was auto-generated from WSDL.
type GetJobCodesForTrust struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetJobCodesForTrust" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetJobCodesForTrust"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	ShiftDate   DateTime `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	TrustId     string   `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetJobCodesForTrustResponse was auto-generated from WSDL.
type GetJobCodesForTrustResponse struct {
	ErrorCode        int              `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string           `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int              `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	JobCodes         *ArrayOfJobCodes `xml:"JobCodes,omitempty" json:"JobCodes,omitempty" yaml:"JobCodes,omitempty"`
}

// GetModifyTimeSheetReasons was auto-generated from WSDL.
type GetModifyTimeSheetReasons struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetModifyTimeSheetReasons" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetModifyTimeSheetReasons"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetModifyTimeSheetReasonsResponse was auto-generated from WSDL.
type GetModifyTimeSheetReasonsResponse struct {
	ErrorCode              int                            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string                         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int                            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ModifyTimeSheetReasons *ArrayOfModifyTimeSheetReasons `xml:"ModifyTimeSheetReasons,omitempty" json:"ModifyTimeSheetReasons,omitempty" yaml:"ModifyTimeSheetReasons,omitempty"`
}

// GetNewStartersDetailsResponse was auto-generated from WSDL.
type GetNewStartersDetailsResponse struct {
	ErrorCode        int                `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string             `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	NewStarters      *ArrayOfNewStarter `xml:"NewStarters,omitempty" json:"NewStarters,omitempty" yaml:"NewStarters,omitempty"`
}

// GetNotifications was auto-generated from WSDL.
type GetNotifications struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetNotifications" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetNotifications"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetNotificationsResponse was auto-generated from WSDL.
type GetNotificationsResponse struct {
	GetNotificationsResult *NotificationResponse `xml:"GetNotificationsResult,omitempty" json:"GetNotificationsResult,omitempty" yaml:"GetNotificationsResult,omitempty"`
}

// GetPerformanceEvaluation was auto-generated from WSDL.
type GetPerformanceEvaluation struct {
	XMLName      xml.Name       `xml:"http://tempuri.org/ GetPerformanceEvaluation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetPerformanceEvaluation"`
	Answers      *ArrayOfAnswer `xml:"Answers,omitempty" json:"Answers,omitempty" yaml:"Answers,omitempty"`
	Information  string         `xml:"Information,omitempty" json:"Information,omitempty" yaml:"Information,omitempty"`
	QuestionID   string         `xml:"QuestionID,omitempty" json:"QuestionID,omitempty" yaml:"QuestionID,omitempty"`
	QuestionText string         `xml:"QuestionText,omitempty" json:"QuestionText,omitempty" yaml:"QuestionText,omitempty"`
	TypeOfAnswer string         `xml:"TypeOfAnswer,omitempty" json:"TypeOfAnswer,omitempty" yaml:"TypeOfAnswer,omitempty"`
}

// GetPerformanceEvaluationReferral was auto-generated from WSDL.
type GetPerformanceEvaluationReferral struct {
	XMLName              xml.Name `xml:"http://tempuri.org/ GetPerformanceEvaluationReferral" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetPerformanceEvaluationReferral"`
	RequestId            int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	EncryptedQueryString string   `xml:"EncryptedQueryString,omitempty" json:"EncryptedQueryString,omitempty" yaml:"EncryptedQueryString,omitempty"`
	WebUserID            string   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken          string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetPerformanceEvaluationReferralResponse was auto-generated
// from WSDL.
type GetPerformanceEvaluationReferralResponse struct {
	GetPerformanceEvaluationReferralResult *GetperformanceevaluatioreferralResponse `xml:"GetPerformanceEvaluationReferralResult,omitempty" json:"GetPerformanceEvaluationReferralResult,omitempty" yaml:"GetPerformanceEvaluationReferralResult,omitempty"`
}

// GetPerformanceEvaluationResponse was auto-generated from WSDL.
type GetPerformanceEvaluationResponse struct {
	ErrorCode                int                              `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription         string                           `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId               int                              `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GetPerformanceEvaluation *ArrayOfGetPerformanceEvaluation `xml:"GetPerformanceEvaluation,omitempty" json:"GetPerformanceEvaluation,omitempty" yaml:"GetPerformanceEvaluation,omitempty"`
	Preamble                 string                           `xml:"Preamble,omitempty" json:"Preamble,omitempty" yaml:"Preamble,omitempty"`
}

// GetPersonalisedRatesPermission was auto-generated from WSDL.
type GetPersonalisedRatesPermission struct {
	XMLName     xml.Name                           `xml:"http://tempuri.org/ GetPersonalisedRatesPermission" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetPersonalisedRatesPermission"`
	RequestId   int                                `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	EndDate     DateTime                           `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	FromDate    DateTime                           `xml:"FromDate,omitempty" json:"FromDate,omitempty" yaml:"FromDate,omitempty"`
	Status      PersonalisedRatesRequestStatusEnum `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	WebUserId   string                             `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string                             `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetPersonalisedRatesPermissionResponse was auto-generated from
// WSDL.
type GetPersonalisedRatesPermissionResponse struct {
	ErrorCode                 int                               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription          string                            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                int                               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	PersonalisedRatesRequests *ArrayOfPersonalisedRatesRequests `xml:"PersonalisedRatesRequests,omitempty" json:"PersonalisedRatesRequests,omitempty" yaml:"PersonalisedRatesRequests,omitempty"`
}

// GetPriorQualificationResponse was auto-generated from WSDL.
type GetPriorQualificationResponse struct {
	ErrorCode           int                         `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string                      `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int                         `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	PriorQualifications *ArrayOfPriorQualificataion `xml:"PriorQualifications,omitempty" json:"PriorQualifications,omitempty" yaml:"PriorQualifications,omitempty"`
}

// GetPriorQualifications was auto-generated from WSDL.
type GetPriorQualifications struct {
	XMLName     xml.Name                       `xml:"http://tempuri.org/ GetPriorQualifications" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetPriorQualifications"`
	RequestId   int                            `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	EndDate     DateTime                       `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate   DateTime                       `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	Status      PriorQualificationStatusAction `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	WebUserID   string                         `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	CallerToken string                         `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetPriorQualificationsResponse was auto-generated from WSDL.
type GetPriorQualificationsResponse struct {
	GetPriorQualificationsResult *GetPriorQualificationResponse `xml:"GetPriorQualificationsResult,omitempty" json:"GetPriorQualificationsResult,omitempty" yaml:"GetPriorQualificationsResult,omitempty"`
}

// GetPushNotificationResponse was auto-generated from WSDL.
type GetPushNotificationResponse struct {
	ErrorCode                    int                      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription             string                   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                   int                      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	IsPushNotificationsMandatory bool                     `xml:"IsPushNotificationsMandatory,omitempty" json:"IsPushNotificationsMandatory,omitempty" yaml:"IsPushNotificationsMandatory,omitempty"`
	PushNotifications            *ArrayOfPushNotification `xml:"PushNotifications,omitempty" json:"PushNotifications,omitempty" yaml:"PushNotifications,omitempty"`
}

// GetPushNotifications was auto-generated from WSDL.
type GetPushNotifications struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetPushNotifications" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetPushNotifications"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetPushNotificationsResponse was auto-generated from WSDL.
type GetPushNotificationsResponse struct {
	GetPushNotificationsResult *GetPushNotificationResponse `xml:"GetPushNotificationsResult,omitempty" json:"GetPushNotificationsResult,omitempty" yaml:"GetPushNotificationsResult,omitempty"`
}

// GetQueriedTimeSheetResponse was auto-generated from WSDL.
type GetQueriedTimeSheetResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TimeSheets       *ArrayOfTimeSheet `xml:"TimeSheets,omitempty" json:"TimeSheets,omitempty" yaml:"TimeSheets,omitempty"`
}

// GetQueriedTimeSheets was auto-generated from WSDL.
type GetQueriedTimeSheets struct {
	XMLName                xml.Name `xml:"http://tempuri.org/ GetQueriedTimeSheets" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetQueriedTimeSheets"`
	RequestId              int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId              string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	BookingReferenceNumber int      `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	CallerToken            string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetQueriedTimeSheetsResponse was auto-generated from WSDL.
type GetQueriedTimeSheetsResponse struct {
	GetQueriedTimeSheetsResult *GetQueriedTimeSheetResponse `xml:"GetQueriedTimeSheetsResult,omitempty" json:"GetQueriedTimeSheetsResult,omitempty" yaml:"GetQueriedTimeSheetsResult,omitempty"`
}

// GetRangeRequestDetails was auto-generated from WSDL.
type GetRangeRequestDetails struct {
	XMLName              xml.Name `xml:"http://tempuri.org/ GetRangeRequestDetails" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetRangeRequestDetails"`
	RequestId            int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	ShiftReferenceNumber string   `xml:"ShiftReferenceNumber,omitempty" json:"ShiftReferenceNumber,omitempty" yaml:"ShiftReferenceNumber,omitempty"`
	WebUserId            string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	CallerToken          string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetRangeRequestDetailsResponse was auto-generated from WSDL.
type GetRangeRequestDetailsResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shifts           *ArrayOfIVRShiftType `xml:"Shifts,omitempty" json:"Shifts,omitempty" yaml:"Shifts,omitempty"`
}

// GetSelfBillingAgreements was auto-generated from WSDL.
type GetSelfBillingAgreements struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetSelfBillingAgreements" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetSelfBillingAgreements"`
	Agency          *Agency  `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	AgencySignatory string   `xml:"AgencySignatory,omitempty" json:"AgencySignatory,omitempty" yaml:"AgencySignatory,omitempty"`
	AgencySignature []byte   `xml:"AgencySignature,omitempty" json:"AgencySignature,omitempty" yaml:"AgencySignature,omitempty"`
	AgencyVATNumber string   `xml:"AgencyVATNumber,omitempty" json:"AgencyVATNumber,omitempty" yaml:"AgencyVATNumber,omitempty"`
	AgreementID     string   `xml:"AgreementID,omitempty" json:"AgreementID,omitempty" yaml:"AgreementID,omitempty"`
	DeactivatedFrom DateTime `xml:"DeactivatedFrom,omitempty" json:"DeactivatedFrom,omitempty" yaml:"DeactivatedFrom,omitempty"`
	EffectiveFrom   DateTime `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
	ExpiryDate      DateTime `xml:"ExpiryDate,omitempty" json:"ExpiryDate,omitempty" yaml:"ExpiryDate,omitempty"`
	Status          string   `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Trust           *Trust   `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	TrustSignatory  string   `xml:"TrustSignatory,omitempty" json:"TrustSignatory,omitempty" yaml:"TrustSignatory,omitempty"`
	TrustSignature  []byte   `xml:"TrustSignature,omitempty" json:"TrustSignature,omitempty" yaml:"TrustSignature,omitempty"`
	TrustVATNumber  string   `xml:"TrustVATNumber,omitempty" json:"TrustVATNumber,omitempty" yaml:"TrustVATNumber,omitempty"`
}

// GetSelfBillingAgreementsResponse was auto-generated from WSDL.
type GetSelfBillingAgreementsResponse struct {
	ErrorCode                int                              `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription         string                           `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId               int                              `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	GetSelfBillingAgreements *ArrayOfGetSelfBillingAgreements `xml:"GetSelfBillingAgreements,omitempty" json:"GetSelfBillingAgreements,omitempty" yaml:"GetSelfBillingAgreements,omitempty"`
}

// GetShiftTypesForTrustGradeResponse was auto-generated from WSDL.
type GetShiftTypesForTrustGradeResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ShiftTypes       *ArrayOfShiftType `xml:"ShiftTypes,omitempty" json:"ShiftTypes,omitempty" yaml:"ShiftTypes,omitempty"`
}

// GetShiftsSince was auto-generated from WSDL.
type GetShiftsSince struct {
	XMLName                xml.Name                 `xml:"http://tempuri.org/ GetShiftsSince" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetShiftsSince"`
	RequestId              int                      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	BookedApprovedReleased ShiftsSinceBookingStatus `xml:"BookedApprovedReleased,omitempty" json:"BookedApprovedReleased,omitempty" yaml:"BookedApprovedReleased,omitempty"`
	DateTime               DateTime                 `xml:"DateTime,omitempty" json:"DateTime,omitempty" yaml:"DateTime,omitempty"`
	EngagementStatus       string                   `xml:"EngagementStatus,omitempty" json:"EngagementStatus,omitempty" yaml:"EngagementStatus,omitempty"`
	LocationCode           string                   `xml:"LocationCode,omitempty" json:"LocationCode,omitempty" yaml:"LocationCode,omitempty"`
	ShiftFrequency         int                      `xml:"ShiftFrequency,omitempty" json:"ShiftFrequency,omitempty" yaml:"ShiftFrequency,omitempty"`
	ShiftID                string                   `xml:"ShiftID,omitempty" json:"ShiftID,omitempty" yaml:"ShiftID,omitempty"`
	TrustCode              string                   `xml:"TrustCode,omitempty" json:"TrustCode,omitempty" yaml:"TrustCode,omitempty"`
	WardCode               string                   `xml:"WardCode,omitempty" json:"WardCode,omitempty" yaml:"WardCode,omitempty"`
	CallerToken            string                   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetShiftsSinceResponse was auto-generated from WSDL.
type GetShiftsSinceResponse struct {
	GetShiftsSinceResult *ShiftsSinceResponse `xml:"GetShiftsSinceResult,omitempty" json:"GetShiftsSinceResult,omitempty" yaml:"GetShiftsSinceResult,omitempty"`
}

// GetSubstantiveWorkerHolidaysResponse was auto-generated from
// WSDL.
type GetSubstantiveWorkerHolidaysResponse struct {
	ErrorCode                 int                               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription          string                            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                int                               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	SubstantiveWorkerHolidays *ArrayOfSubstantiveWorkerHolidays `xml:"SubstantiveWorkerHolidays,omitempty" json:"SubstantiveWorkerHolidays,omitempty" yaml:"SubstantiveWorkerHolidays,omitempty"`
}

// GetSurfaceInformation was auto-generated from WSDL.
type GetSurfaceInformation struct {
	XMLName      xml.Name `xml:"http://tempuri.org/ GetSurfaceInformation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetSurfaceInformation"`
	RequestId    int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	LocationCode string   `xml:"LocationCode,omitempty" json:"LocationCode,omitempty" yaml:"LocationCode,omitempty"`
	WardCode     string   `xml:"WardCode,omitempty" json:"WardCode,omitempty" yaml:"WardCode,omitempty"`
	CallerToken  string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetSurfaceInformationResponse was auto-generated from WSDL.
type GetSurfaceInformationResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ShiftDetails     *ArrayOfShiftDetails `xml:"ShiftDetails,omitempty" json:"ShiftDetails,omitempty" yaml:"ShiftDetails,omitempty"`
}

// GetTrustStaffGroupAuthorisationCodeSetting was auto-generated
// from WSDL.
type GetTrustStaffGroupAuthorisationCodeSetting struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetTrustStaffGroupAuthorisationCodeSetting" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetTrustStaffGroupAuthorisationCodeSetting"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffGroup  string   `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	TrustId     string   `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetTrustStaffGroupAuthorisationCodeSettingResponse was auto-generated
// from WSDL.
type GetTrustStaffGroupAuthorisationCodeSettingResponse struct {
	ErrorCode                        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription                 string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	IsTrustAuthorisationCodeRequired bool   `xml:"IsTrustAuthorisationCodeRequired,omitempty" json:"IsTrustAuthorisationCodeRequired,omitempty" yaml:"IsTrustAuthorisationCodeRequired,omitempty"`
}

// GetTrusts was auto-generated from WSDL.
type GetTrusts struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetTrusts" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetTrusts"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetTrustsResponse was auto-generated from WSDL.
type GetTrustsResponse struct {
	GetTrustsResult *TrustsResponse `xml:"GetTrustsResult,omitempty" json:"GetTrustsResult,omitempty" yaml:"GetTrustsResult,omitempty"`
}

// GetUserInfoForTimeSheetModification was auto-generated from
// WSDL.
type GetUserInfoForTimeSheetModification struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ GetUserInfoForTimeSheetModification" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetUserInfoForTimeSheetModification"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	StaffId         string   `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	ReferenceNumber int      `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetUserInfoForTimeSheetModificationResponse was auto-generated
// from WSDL.
type GetUserInfoForTimeSheetModificationResponse struct {
	GetUserInfoForTimeSheetModificationResult *UserInfoForTimeSheetModificationResponse `xml:"GetUserInfoForTimeSheetModificationResult,omitempty" json:"GetUserInfoForTimeSheetModificationResult,omitempty" yaml:"GetUserInfoForTimeSheetModificationResult,omitempty"`
}

// GetWebUserIdOfWardUsers was auto-generated from WSDL.
type GetWebUserIdOfWardUsers struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ GetWebUserIdOfWardUsers" json:"-" yaml:"-" action:"http://tempuri.org/IClient/GetWebUserIdOfWardUsers"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	LocationID  string   `xml:"LocationID,omitempty" json:"LocationID,omitempty" yaml:"LocationID,omitempty"`
	TrustID     string   `xml:"TrustID,omitempty" json:"TrustID,omitempty" yaml:"TrustID,omitempty"`
	WardID      string   `xml:"WardID,omitempty" json:"WardID,omitempty" yaml:"WardID,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// GetWebUserIdOfWardUsersResponse was auto-generated from WSDL.
type GetWebUserIdOfWardUsersResponse struct {
	ErrorCode                                     int                                                   `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription                              string                                                `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                                    int                                                   `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TrustUsersWhoCanCompletePerformanceEvaluation *ArrayOfTrustUsersWhoCanCompletePerformanceEvaluation `xml:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty" json:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty" yaml:"TrustUsersWhoCanCompletePerformanceEvaluation,omitempty"`
}

// GetperformanceevaluatioreferralResponse was auto-generated from
// WSDL.
type GetperformanceevaluatioreferralResponse struct {
	ErrorCode                      int                                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription               string                                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                     int                                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	PerformanceEvaluationReferrals *ArrayOfPerformanceEvaluationReferrals `xml:"PerformanceEvaluationReferrals,omitempty" json:"PerformanceEvaluationReferrals,omitempty" yaml:"PerformanceEvaluationReferrals,omitempty"`
}

// History was auto-generated from WSDL.
type History struct {
	Changes     string   `xml:"Changes,omitempty" json:"Changes,omitempty" yaml:"Changes,omitempty"`
	HistoryDate DateTime `xml:"HistoryDate,omitempty" json:"HistoryDate,omitempty" yaml:"HistoryDate,omitempty"`
	Reason      string   `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
}

// HolidayBooking was auto-generated from WSDL.
type HolidayBooking struct {
	BookingReferenceNumber int                  `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	BookingRequestedDate   DateTime             `xml:"BookingRequestedDate,omitempty" json:"BookingRequestedDate,omitempty" yaml:"BookingRequestedDate,omitempty"`
	EndTime                DateTime             `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Rate                   float64              `xml:"Rate,omitempty" json:"Rate,omitempty" yaml:"Rate,omitempty"`
	ShiftDate              DateTime             `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftStatus            HolidayBookingStatus `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	StaffId                string               `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	StartTime              DateTime             `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
}

// HrReview was auto-generated from WSDL.
type HrReview struct {
	HRUser string              `xml:"HRUser,omitempty" json:"HRUser,omitempty" yaml:"HRUser,omitempty"`
	Notes  *ArrayOfDNASNCNotes `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
}

// IVRShiftType was auto-generated from WSDL.
type IVRShiftType struct {
	Agency                          *ArrayOfAgencyType  `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	AssignmentCode                  string              `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	BookingReason                   *BookingReason      `xml:"BookingReason,omitempty" json:"BookingReason,omitempty" yaml:"BookingReason,omitempty"`
	BookingReferenceNumber          string              `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	Date                            DateTime            `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	EndTime                         DateTime            `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Gender                          GenderEnum          `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	HasComment                      bool                `xml:"HasComment,omitempty" json:"HasComment,omitempty" yaml:"HasComment,omitempty"`
	HasGoldenKey                    bool                `xml:"HasGoldenKey,omitempty" json:"HasGoldenKey,omitempty" yaml:"HasGoldenKey,omitempty"`
	Induction                       *Induction          `xml:"Induction,omitempty" json:"Induction,omitempty" yaml:"Induction,omitempty"`
	IsQualifiedAssignment           bool                `xml:"IsQualifiedAssignment,omitempty" json:"IsQualifiedAssignment,omitempty" yaml:"IsQualifiedAssignment,omitempty"`
	IsRangeRequest                  bool                `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	LocationId                      string              `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	NoAgency                        bool                `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Note                            *ArrayOfstring      `xml:"Note,omitempty" json:"Note,omitempty" yaml:"Note,omitempty"`
	NoteDetails                     *ArrayOfNoteDetails `xml:"NoteDetails,omitempty" json:"NoteDetails,omitempty" yaml:"NoteDetails,omitempty"`
	OnCall                          bool                `xml:"OnCall,omitempty" json:"OnCall,omitempty" yaml:"OnCall,omitempty"`
	ProbabilityScore                *Decimal            `xml:"ProbabilityScore,omitempty" json:"ProbabilityScore,omitempty" yaml:"ProbabilityScore,omitempty"`
	RangeFilledBy                   string              `xml:"RangeFilledBy,omitempty" json:"RangeFilledBy,omitempty" yaml:"RangeFilledBy,omitempty"`
	SecondaryAssignmentCode         string              `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration string              `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftStatus                     ShiftStatusEnum     `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	Shift_Type                      ShiftType           `xml:"Shift_Type,omitempty" json:"Shift_Type,omitempty" yaml:"Shift_Type,omitempty"`
	StaffFacade                     *StaffFacadeType    `xml:"StaffFacade,omitempty" json:"StaffFacade,omitempty" yaml:"StaffFacade,omitempty"`
	StaffGroup                      *StaffGroup         `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	StartTime                       DateTime            `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	TrustAuthorisationCode          string              `xml:"TrustAuthorisationCode,omitempty" json:"TrustAuthorisationCode,omitempty" yaml:"TrustAuthorisationCode,omitempty"`
	TrustId                         string              `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	TwoTierAuthorisationShiftStatus string              `xml:"TwoTierAuthorisationShiftStatus,omitempty" json:"TwoTierAuthorisationShiftStatus,omitempty" yaml:"TwoTierAuthorisationShiftStatus,omitempty"`
	WardDetails                     *WardDetails        `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
	WardId                          string              `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
}

// Induction was auto-generated from WSDL.
type Induction struct {
	InductionExpiryOn DateTime `xml:"InductionExpiryOn,omitempty" json:"InductionExpiryOn,omitempty" yaml:"InductionExpiryOn,omitempty"`
	InductionRequired bool     `xml:"InductionRequired,omitempty" json:"InductionRequired,omitempty" yaml:"InductionRequired,omitempty"`
}

// InsertDataResponse was auto-generated from WSDL.
type InsertDataResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	InsertDataId     string `xml:"InsertDataId,omitempty" json:"InsertDataId,omitempty" yaml:"InsertDataId,omitempty"`
}

// Invoicedetail was auto-generated from WSDL.
type Invoicedetail struct {
	ActualHours  DateTime      `xml:"ActualHours,omitempty" json:"ActualHours,omitempty" yaml:"ActualHours,omitempty"`
	AgencyWorker *AgencyWorker `xml:"AgencyWorker,omitempty" json:"AgencyWorker,omitempty" yaml:"AgencyWorker,omitempty"`
	Commission   float64       `xml:"Commission,omitempty" json:"Commission,omitempty" yaml:"Commission,omitempty"`
	Rate         string        `xml:"Rate,omitempty" json:"Rate,omitempty" yaml:"Rate,omitempty"`
	RefNumber    int           `xml:"RefNumber,omitempty" json:"RefNumber,omitempty" yaml:"RefNumber,omitempty"`
	ShiftCost    float64       `xml:"ShiftCost,omitempty" json:"ShiftCost,omitempty" yaml:"ShiftCost,omitempty"`
	ShiftDate    DateTime      `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftType    ShiftType     `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	Total        float64       `xml:"Total,omitempty" json:"Total,omitempty" yaml:"Total,omitempty"`
}

// JobCodes was auto-generated from WSDL.
type JobCodes struct {
	DeactivateEffectiveFrom DateTime `xml:"DeactivateEffectiveFrom,omitempty" json:"DeactivateEffectiveFrom,omitempty" yaml:"DeactivateEffectiveFrom,omitempty"`
	EffectiveFrom           DateTime `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
	EffectiveTo             DateTime `xml:"EffectiveTo,omitempty" json:"EffectiveTo,omitempty" yaml:"EffectiveTo,omitempty"`
	JobCodesCode            string   `xml:"JobCodesCode,omitempty" json:"JobCodesCode,omitempty" yaml:"JobCodesCode,omitempty"`
	JobCodesDescription     string   `xml:"JobCodesDescription,omitempty" json:"JobCodesDescription,omitempty" yaml:"JobCodesDescription,omitempty"`
	ReasonDisabled          bool     `xml:"ReasonDisabled,omitempty" json:"ReasonDisabled,omitempty" yaml:"ReasonDisabled,omitempty"`
}

// LeadTimeEntry was auto-generated from WSDL.
type LeadTimeEntry struct {
	EndDate       DateTime    `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	LeadTimeHours float64     `xml:"LeadTimeHours,omitempty" json:"LeadTimeHours,omitempty" yaml:"LeadTimeHours,omitempty"`
	LeadTimeID    string      `xml:"LeadTimeID,omitempty" json:"LeadTimeID,omitempty" yaml:"LeadTimeID,omitempty"`
	Recurrence    *Recurrence `xml:"Recurrence,omitempty" json:"Recurrence,omitempty" yaml:"Recurrence,omitempty"`
	StartDate     DateTime    `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
}

// Location was auto-generated from WSDL.
type Location struct {
	LocationId   string `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	LocationName string `xml:"LocationName,omitempty" json:"LocationName,omitempty" yaml:"LocationName,omitempty"`
	Trust        *Trust `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// LocationType was auto-generated from WSDL.
type LocationType struct {
	LocationId   string `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	LocationName string `xml:"LocationName,omitempty" json:"LocationName,omitempty" yaml:"LocationName,omitempty"`
	Trust        *Trust `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// LoginAndGetProfileResponse was auto-generated from WSDL.
type LoginAndGetProfileResponse struct {
	ErrorCode             int        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription      string     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId            int        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	FWProfile             *FWProfile `xml:"FWProfile,omitempty" json:"FWProfile,omitempty" yaml:"FWProfile,omitempty"`
	PasswordResetRequired bool       `xml:"PasswordResetRequired,omitempty" json:"PasswordResetRequired,omitempty" yaml:"PasswordResetRequired,omitempty"`
}

// MarketingShift was auto-generated from WSDL.
type MarketingShift struct {
	AssignmentCode         string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	BookingReferenceNumber int                         `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	EndTime                DateTime                    `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	IsRangeRequest         bool                        `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	Location               *Location                   `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	Notes                  bool                        `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	RangeFilledBy          string                      `xml:"RangeFilledBy,omitempty" json:"RangeFilledBy,omitempty" yaml:"RangeFilledBy,omitempty"`
	ShiftDate              DateTime                    `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftNotes             *ArrayOfCommentsComplexType `xml:"ShiftNotes,omitempty" json:"ShiftNotes,omitempty" yaml:"ShiftNotes,omitempty"`
	ShiftType              ShiftType                   `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	StartTime              DateTime                    `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	Status                 string                      `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Trust                  *Trust                      `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward                   *Ward                       `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WardDetails            *WardDetails                `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
}

// MarketingShiftsRequestResponse was auto-generated from WSDL.
type MarketingShiftsRequestResponse struct {
	ErrorCode        int                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	MarketingShifts  *ArrayOfMarketingShift `xml:"MarketingShifts,omitempty" json:"MarketingShifts,omitempty" yaml:"MarketingShifts,omitempty"`
}

// Message was auto-generated from WSDL.
type Message struct {
	ArrayOfMessages *ArrayOfArrayOfMessages `xml:"ArrayOfMessages,omitempty" json:"ArrayOfMessages,omitempty" yaml:"ArrayOfMessages,omitempty"`
	Preamble        string                  `xml:"Preamble,omitempty" json:"Preamble,omitempty" yaml:"Preamble,omitempty"`
}

// MileageComplexType was auto-generated from WSDL.
type MileageComplexType struct {
	Claim          float64        `xml:"Claim,omitempty" json:"Claim,omitempty" yaml:"Claim,omitempty"`
	EngineCapacity EngineCapacity `xml:"EngineCapacity,omitempty" json:"EngineCapacity,omitempty" yaml:"EngineCapacity,omitempty"`
	Miles          Miles          `xml:"Miles,omitempty" json:"Miles,omitempty" yaml:"Miles,omitempty"`
	VehicleType    VehicleType    `xml:"VehicleType,omitempty" json:"VehicleType,omitempty" yaml:"VehicleType,omitempty"`
}

// ModifyContractStatus was auto-generated from WSDL.
type ModifyContractStatus struct {
	XMLName     xml.Name                    `xml:"http://tempuri.org/ ModifyContractStatus" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifyContractStatus"`
	RequestId   int                         `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Action      AgencyRateCardsActionStatus `xml:"Action,omitempty" json:"Action,omitempty" yaml:"Action,omitempty"`
	ContractID  int                         `xml:"ContractID,omitempty" json:"ContractID,omitempty" yaml:"ContractID,omitempty"`
	Reason      string                      `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	WebUserID   string                      `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	RequesterId string                      `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken string                      `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifyContractStatusResponse was auto-generated from WSDL.
type ModifyContractStatusResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ModifyPersonalisedRatesPermission was auto-generated from WSDL.
type ModifyPersonalisedRatesPermission struct {
	XMLName                   xml.Name                               `xml:"http://tempuri.org/ ModifyPersonalisedRatesPermission" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifyPersonalisedRatesPermission"`
	RequestId                 int                                    `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	PersonalisedRateRequestId int                                    `xml:"PersonalisedRateRequestId,omitempty" json:"PersonalisedRateRequestId,omitempty" yaml:"PersonalisedRateRequestId,omitempty"`
	Reason                    string                                 `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	Status                    ModifyPersonalisedRatePermissionStatus `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	WebUserId                 string                                 `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId               string                                 `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken               string                                 `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifyPersonalisedRatesPermissionResponse was auto-generated
// from WSDL.
type ModifyPersonalisedRatesPermissionResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ModifyPriorQualification was auto-generated from WSDL.
type ModifyPriorQualification struct {
	XMLName              xml.Name                  `xml:"http://tempuri.org/ ModifyPriorQualification" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifyPriorQualification"`
	PriorQualificationID int                       `xml:"PriorQualificationID,omitempty" json:"PriorQualificationID,omitempty" yaml:"PriorQualificationID,omitempty"`
	Reason               string                    `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	Status               PriorQualificationsStatus `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Webuserid            string                    `xml:"Webuserid,omitempty" json:"Webuserid,omitempty" yaml:"Webuserid,omitempty"`
	RequesterId          string                    `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	RequestId            int                       `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken          string                    `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifyPriorQualificationResponse was auto-generated from WSDL.
type ModifyPriorQualificationResponse struct {
	ModifyPriorQualificationResult *ModifyPriorQualificatonResponse `xml:"ModifyPriorQualificationResult,omitempty" json:"ModifyPriorQualificationResult,omitempty" yaml:"ModifyPriorQualificationResult,omitempty"`
}

// ModifyPriorQualificatonResponse was auto-generated from WSDL.
type ModifyPriorQualificatonResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ModifySelfBillingAgreement was auto-generated from WSDL.
type ModifySelfBillingAgreement struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ ModifySelfBillingAgreement" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifySelfBillingAgreement"`
	Action      Action   `xml:"Action,omitempty" json:"Action,omitempty" yaml:"Action,omitempty"`
	AgreementId int      `xml:"AgreementId,omitempty" json:"AgreementId,omitempty" yaml:"AgreementId,omitempty"`
	Picture     string   `xml:"Picture,omitempty" json:"Picture,omitempty" yaml:"Picture,omitempty"`
	SignedBy    string   `xml:"SignedBy,omitempty" json:"SignedBy,omitempty" yaml:"SignedBy,omitempty"`
	WebUserId   string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifySelfBillingAgreementResponse was auto-generated from WSDL.
type ModifySelfBillingAgreementResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ModifyShiftRequest was auto-generated from WSDL.
type ModifyShiftRequest struct {
	XMLName                         xml.Name                `xml:"http://tempuri.org/ ModifyShiftRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifyShiftRequest"`
	RequestId                       int                     `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AssignmentCode                  string                  `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	AuthorizationCode               string                  `xml:"AuthorizationCode,omitempty" json:"AuthorizationCode,omitempty" yaml:"AuthorizationCode,omitempty"`
	BookingReferenceNumber          int                     `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	Date                            DateTime                `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	EndTime                         DateTime                `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	FlexibleWorkerNotified          bool                    `xml:"FlexibleWorkerNotified,omitempty" json:"FlexibleWorkerNotified,omitempty" yaml:"FlexibleWorkerNotified,omitempty"`
	Gender                          GenderEnum              `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	IsPendingAuthorisation          bool                    `xml:"IsPendingAuthorisation,omitempty" json:"IsPendingAuthorisation,omitempty" yaml:"IsPendingAuthorisation,omitempty"`
	LocationId                      string                  `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	NoAgency                        bool                    `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Notes                           *ArrayOfNoteComplexType `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	RemoveGoldenKey                 bool                    `xml:"RemoveGoldenKey,omitempty" json:"RemoveGoldenKey,omitempty" yaml:"RemoveGoldenKey,omitempty"`
	SecondaryAssignmentCode         string                  `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration string                  `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	Shift_Type                      ShiftType               `xml:"Shift_Type,omitempty" json:"Shift_Type,omitempty" yaml:"Shift_Type,omitempty"`
	StartTime                       DateTime                `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	WardId                          string                  `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WebUserId                       string                  `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId                     string                  `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	AgencyId                        int                     `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	StaffId                         string                  `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	CallerToken                     string                  `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifyShiftRequestResponse was auto-generated from WSDL.
type ModifyShiftRequestResponse struct {
	ModifyShiftRequestResult *ModifyShiftResponse `xml:"ModifyShiftRequestResult,omitempty" json:"ModifyShiftRequestResult,omitempty" yaml:"ModifyShiftRequestResult,omitempty"`
}

// ModifyShiftResponse was auto-generated from WSDL.
type ModifyShiftResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ModifyTimeSheetReasons was auto-generated from WSDL.
type ModifyTimeSheetReasons struct {
	Code        string `xml:"Code,omitempty" json:"Code,omitempty" yaml:"Code,omitempty"`
	Description string `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
}

// ModifyTimeSheetResponse was auto-generated from WSDL.
type ModifyTimeSheetResponse struct {
	ErrorCode                     int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription              string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                    int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	PerformanceEvaluationRequired bool   `xml:"PerformanceEvaluationRequired,omitempty" json:"PerformanceEvaluationRequired,omitempty" yaml:"PerformanceEvaluationRequired,omitempty"`
}

// ModifyTimeSheets was auto-generated from WSDL.
type ModifyTimeSheets struct {
	XMLName            xml.Name  `xml:"http://tempuri.org/ ModifyTimeSheets" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ModifyTimeSheets"`
	RequestId          int       `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AssignmentCode     string    `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Authorize          bool      `xml:"Authorize,omitempty" json:"Authorize,omitempty" yaml:"Authorize,omitempty"`
	BreakInMins        int       `xml:"BreakInMins,omitempty" json:"BreakInMins,omitempty" yaml:"BreakInMins,omitempty"`
	Comments           string    `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
	FWCancelled        bool      `xml:"FWCancelled,omitempty" json:"FWCancelled,omitempty" yaml:"FWCancelled,omitempty"`
	FWDna              bool      `xml:"FWDna,omitempty" json:"FWDna,omitempty" yaml:"FWDna,omitempty"`
	IgnoreWarning      bool      `xml:"IgnoreWarning,omitempty" json:"IgnoreWarning,omitempty" yaml:"IgnoreWarning,omitempty"`
	LocationId         string    `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	StartTime          DateTime  `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	EndTime            DateTime  `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Location           string    `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	NotifiedAt         DateTime  `xml:"NotifiedAt,omitempty" json:"NotifiedAt,omitempty" yaml:"NotifiedAt,omitempty"`
	NotifiedOn         DateTime  `xml:"NotifiedOn,omitempty" json:"NotifiedOn,omitempty" yaml:"NotifiedOn,omitempty"`
	Reason             string    `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	ReferenceNumber    int       `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	RequesterId        string    `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	Shift_Type         ShiftType `xml:"Shift_Type,omitempty" json:"Shift_Type,omitempty" yaml:"Shift_Type,omitempty"`
	VerifyTimesheet    bool      `xml:"VerifyTimesheet,omitempty" json:"VerifyTimesheet,omitempty" yaml:"VerifyTimesheet,omitempty"`
	WardCancelled      bool      `xml:"WardCancelled,omitempty" json:"WardCancelled,omitempty" yaml:"WardCancelled,omitempty"`
	WardId             string    `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WebUserId          string    `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	AgencyId           int       `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	StaffId            string    `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	DeleteShiftRequest bool      `xml:"DeleteShiftRequest,omitempty" json:"DeleteShiftRequest,omitempty" yaml:"DeleteShiftRequest,omitempty"`
	CallerToken        string    `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ModifyTimeSheetsResponse was auto-generated from WSDL.
type ModifyTimeSheetsResponse struct {
	ModifyTimeSheetsResult *ModifyTimeSheetResponse `xml:"ModifyTimeSheetsResult,omitempty" json:"ModifyTimeSheetsResult,omitempty" yaml:"ModifyTimeSheetsResult,omitempty"`
}

// Name was auto-generated from WSDL.
type Name struct {
	FirstName  string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	LastName   string `xml:"LastName,omitempty" json:"LastName,omitempty" yaml:"LastName,omitempty"`
	MiddleName string `xml:"MiddleName,omitempty" json:"MiddleName,omitempty" yaml:"MiddleName,omitempty"`
}

// NameType was auto-generated from WSDL.
type NameType struct {
	FirstName  string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	LastName   string `xml:"LastName,omitempty" json:"LastName,omitempty" yaml:"LastName,omitempty"`
	MiddleName string `xml:"MiddleName,omitempty" json:"MiddleName,omitempty" yaml:"MiddleName,omitempty"`
}

// NewStarter was auto-generated from WSDL.
type NewStarter struct {
	Email     string `xml:"Email,omitempty" json:"Email,omitempty" yaml:"Email,omitempty"`
	FirstName string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	StaffId   string `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	Surname   string `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
}

// NewWorkerResponse was auto-generated from WSDL.
type NewWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	StaffId          string `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
}

// Note was auto-generated from WSDL.
type Note struct {
	Note1  string        `xml:"Note1,omitempty" json:"Note1,omitempty" yaml:"Note1,omitempty"`
	ViewBy ViewNotesEnum `xml:"ViewBy,omitempty" json:"ViewBy,omitempty" yaml:"ViewBy,omitempty"`
}

// NoteComplexType was auto-generated from WSDL.
type NoteComplexType struct {
	Note1  string        `xml:"Note1,omitempty" json:"Note1,omitempty" yaml:"Note1,omitempty"`
	ViewBy ViewNotesEnum `xml:"ViewBy,omitempty" json:"ViewBy,omitempty" yaml:"ViewBy,omitempty"`
}

// NoteDetails was auto-generated from WSDL.
type NoteDetails struct {
	Note       string         `xml:"Note,omitempty" json:"Note,omitempty" yaml:"Note,omitempty"`
	ViewableBy *ArrayOfstring `xml:"ViewableBy,omitempty" json:"ViewableBy,omitempty" yaml:"ViewableBy,omitempty"`
}

// Notification was auto-generated from WSDL.
type Notification struct {
	NoteId string `xml:"NoteId,omitempty" json:"NoteId,omitempty" yaml:"NoteId,omitempty"`
	Text   string `xml:"Text,omitempty" json:"Text,omitempty" yaml:"Text,omitempty"`
}

// NotificationResponse was auto-generated from WSDL.
type NotificationResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Notifications    *ArrayOfNotification `xml:"Notifications,omitempty" json:"Notifications,omitempty" yaml:"Notifications,omitempty"`
}

// Parameters was auto-generated from WSDL.
type Parameters struct {
	Agency     string `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	Grade      string `xml:"Grade,omitempty" json:"Grade,omitempty" yaml:"Grade,omitempty"`
	Location   string `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	StaffGroup string `xml:"StaffGroup,omitempty" json:"StaffGroup,omitempty" yaml:"StaffGroup,omitempty"`
	Trust      string `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward       string `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// PatternItem was auto-generated from WSDL.
type PatternItem struct {
	AvailabilityType string   `xml:"AvailabilityType,omitempty" json:"AvailabilityType,omitempty" yaml:"AvailabilityType,omitempty"`
	DayOfWeek        int      `xml:"DayOfWeek,omitempty" json:"DayOfWeek,omitempty" yaml:"DayOfWeek,omitempty"`
	EndTime          DateTime `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	StartTime        DateTime `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
}

// PaySlipIdResponse was auto-generated from WSDL.
type PaySlipIdResponse struct {
	ErrorCode        int             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	PaySlips         *ArrayOfPayslip `xml:"PaySlips,omitempty" json:"PaySlips,omitempty" yaml:"PaySlips,omitempty"`
}

// PaySlipResponse was auto-generated from WSDL.
type PaySlipResponse struct {
	ErrorCode                 int                       `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription          string                    `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                int                       `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ASGNumber                 string                    `xml:"ASGNumber,omitempty" json:"ASGNumber,omitempty" yaml:"ASGNumber,omitempty"`
	Address1                  string                    `xml:"Address1,omitempty" json:"Address1,omitempty" yaml:"Address1,omitempty"`
	Address2                  string                    `xml:"Address2,omitempty" json:"Address2,omitempty" yaml:"Address2,omitempty"`
	Address3                  string                    `xml:"Address3,omitempty" json:"Address3,omitempty" yaml:"Address3,omitempty"`
	AnnualSalary              string                    `xml:"AnnualSalary,omitempty" json:"AnnualSalary,omitempty" yaml:"AnnualSalary,omitempty"`
	AssignmentLocation        string                    `xml:"AssignmentLocation,omitempty" json:"AssignmentLocation,omitempty" yaml:"AssignmentLocation,omitempty"`
	County                    string                    `xml:"County,omitempty" json:"County,omitempty" yaml:"County,omitempty"`
	Deductions                *ArrayOfPayslipDeductions `xml:"Deductions,omitempty" json:"Deductions,omitempty" yaml:"Deductions,omitempty"`
	EmployeeName              string                    `xml:"EmployeeName,omitempty" json:"EmployeeName,omitempty" yaml:"EmployeeName,omitempty"`
	EmployeeName2             string                    `xml:"EmployeeName2,omitempty" json:"EmployeeName2,omitempty" yaml:"EmployeeName2,omitempty"`
	EmployeeNo                string                    `xml:"EmployeeNo,omitempty" json:"EmployeeNo,omitempty" yaml:"EmployeeNo,omitempty"`
	Frequency                 string                    `xml:"Frequency,omitempty" json:"Frequency,omitempty" yaml:"Frequency,omitempty"`
	GrossPayYTD               string                    `xml:"GrossPayYTD,omitempty" json:"GrossPayYTD,omitempty" yaml:"GrossPayYTD,omitempty"`
	IncrementDate             string                    `xml:"IncrementDate,omitempty" json:"IncrementDate,omitempty" yaml:"IncrementDate,omitempty"`
	Message                   *ArrayOfstring            `xml:"Message,omitempty" json:"Message,omitempty" yaml:"Message,omitempty"`
	NICostsYTD                string                    `xml:"NICostsYTD,omitempty" json:"NICostsYTD,omitempty" yaml:"NICostsYTD,omitempty"`
	NILetter                  string                    `xml:"NILetter,omitempty" json:"NILetter,omitempty" yaml:"NILetter,omitempty"`
	NINumber                  string                    `xml:"NINumber,omitempty" json:"NINumber,omitempty" yaml:"NINumber,omitempty"`
	NIPayYTD                  string                    `xml:"NIPayYTD,omitempty" json:"NIPayYTD,omitempty" yaml:"NIPayYTD,omitempty"`
	NetPay                    string                    `xml:"NetPay,omitempty" json:"NetPay,omitempty" yaml:"NetPay,omitempty"`
	NonTaxablePayPTD          string                    `xml:"NonTaxablePayPTD,omitempty" json:"NonTaxablePayPTD,omitempty" yaml:"NonTaxablePayPTD,omitempty"`
	OrganisationName          string                    `xml:"OrganisationName,omitempty" json:"OrganisationName,omitempty" yaml:"OrganisationName,omitempty"`
	OtherNICostsYTD           string                    `xml:"OtherNICostsYTD,omitempty" json:"OtherNICostsYTD,omitempty" yaml:"OtherNICostsYTD,omitempty"`
	OtherNIPayYTD             string                    `xml:"OtherNIPayYTD,omitempty" json:"OtherNIPayYTD,omitempty" yaml:"OtherNIPayYTD,omitempty"`
	PayDate                   DateTime                  `xml:"PayDate,omitempty" json:"PayDate,omitempty" yaml:"PayDate,omitempty"`
	PayLocation               string                    `xml:"PayLocation,omitempty" json:"PayLocation,omitempty" yaml:"PayLocation,omitempty"`
	PayMethod                 string                    `xml:"PayMethod,omitempty" json:"PayMethod,omitempty" yaml:"PayMethod,omitempty"`
	PayPoint                  string                    `xml:"PayPoint,omitempty" json:"PayPoint,omitempty" yaml:"PayPoint,omitempty"`
	PaySlipId                 int                       `xml:"PaySlipId,omitempty" json:"PaySlipId,omitempty" yaml:"PaySlipId,omitempty"`
	Payments                  *ArrayOfPayslipPayments   `xml:"Payments,omitempty" json:"Payments,omitempty" yaml:"Payments,omitempty"`
	PayrollName               string                    `xml:"PayrollName,omitempty" json:"PayrollName,omitempty" yaml:"PayrollName,omitempty"`
	PayscaleDescription       string                    `xml:"PayscaleDescription,omitempty" json:"PayscaleDescription,omitempty" yaml:"PayscaleDescription,omitempty"`
	PayslipListEmployeeNameId string                    `xml:"PayslipListEmployeeNameId,omitempty" json:"PayslipListEmployeeNameId,omitempty" yaml:"PayslipListEmployeeNameId,omitempty"`
	PensionContsYTD           string                    `xml:"PensionContsYTD,omitempty" json:"PensionContsYTD,omitempty" yaml:"PensionContsYTD,omitempty"`
	PensionablePayPTD         string                    `xml:"PensionablePayPTD,omitempty" json:"PensionablePayPTD,omitempty" yaml:"PensionablePayPTD,omitempty"`
	PensionablePayYTD         string                    `xml:"PensionablePayYTD,omitempty" json:"PensionablePayYTD,omitempty" yaml:"PensionablePayYTD,omitempty"`
	PeriodEndDate             DateTime                  `xml:"PeriodEndDate,omitempty" json:"PeriodEndDate,omitempty" yaml:"PeriodEndDate,omitempty"`
	PositionTitle             string                    `xml:"PositionTitle,omitempty" json:"PositionTitle,omitempty" yaml:"PositionTitle,omitempty"`
	PostCode                  string                    `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	PreviousTaxPaidYTD        string                    `xml:"PreviousTaxPaidYTD,omitempty" json:"PreviousTaxPaidYTD,omitempty" yaml:"PreviousTaxPaidYTD,omitempty"`
	PreviousTaxablePayYTD     string                    `xml:"PreviousTaxablePayYTD,omitempty" json:"PreviousTaxablePayYTD,omitempty" yaml:"PreviousTaxablePayYTD,omitempty"`
	ProratedSalary            string                    `xml:"ProratedSalary,omitempty" json:"ProratedSalary,omitempty" yaml:"ProratedSalary,omitempty"`
	SDRefNumber               string                    `xml:"SDRefNumber,omitempty" json:"SDRefNumber,omitempty" yaml:"SDRefNumber,omitempty"`
	StandardHours             string                    `xml:"StandardHours,omitempty" json:"StandardHours,omitempty" yaml:"StandardHours,omitempty"`
	StatAddress               string                    `xml:"StatAddress,omitempty" json:"StatAddress,omitempty" yaml:"StatAddress,omitempty"`
	TaxCode                   string                    `xml:"TaxCode,omitempty" json:"TaxCode,omitempty" yaml:"TaxCode,omitempty"`
	TaxName                   string                    `xml:"TaxName,omitempty" json:"TaxName,omitempty" yaml:"TaxName,omitempty"`
	TaxPaidYTD                string                    `xml:"TaxPaidYTD,omitempty" json:"TaxPaidYTD,omitempty" yaml:"TaxPaidYTD,omitempty"`
	TaxPeriod                 string                    `xml:"TaxPeriod,omitempty" json:"TaxPeriod,omitempty" yaml:"TaxPeriod,omitempty"`
	TaxRef                    string                    `xml:"TaxRef,omitempty" json:"TaxRef,omitempty" yaml:"TaxRef,omitempty"`
	TaxablePayPTD             string                    `xml:"TaxablePayPTD,omitempty" json:"TaxablePayPTD,omitempty" yaml:"TaxablePayPTD,omitempty"`
	TaxablePayYTD             string                    `xml:"TaxablePayYTD,omitempty" json:"TaxablePayYTD,omitempty" yaml:"TaxablePayYTD,omitempty"`
	ThreadNumber              string                    `xml:"ThreadNumber,omitempty" json:"ThreadNumber,omitempty" yaml:"ThreadNumber,omitempty"`
	TotalDeductions           string                    `xml:"TotalDeductions,omitempty" json:"TotalDeductions,omitempty" yaml:"TotalDeductions,omitempty"`
	TotalPayments             string                    `xml:"TotalPayments,omitempty" json:"TotalPayments,omitempty" yaml:"TotalPayments,omitempty"`
	Town                      string                    `xml:"Town,omitempty" json:"Town,omitempty" yaml:"Town,omitempty"`
	Trust                     string                    `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	TrustId                   string                    `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
}

// Payslip was auto-generated from WSDL.
type Payslip struct {
	PayDate     DateTime `xml:"PayDate,omitempty" json:"PayDate,omitempty" yaml:"PayDate,omitempty"`
	PaySlipId   int      `xml:"PaySlipId,omitempty" json:"PaySlipId,omitempty" yaml:"PaySlipId,omitempty"`
	PayrollType string   `xml:"PayrollType,omitempty" json:"PayrollType,omitempty" yaml:"PayrollType,omitempty"`
}

// PayslipDeductions was auto-generated from WSDL.
type PayslipDeductions struct {
	DeductionElement string `xml:"DeductionElement,omitempty" json:"DeductionElement,omitempty" yaml:"DeductionElement,omitempty"`
	DeductionValue1  string `xml:"DeductionValue1,omitempty" json:"DeductionValue1,omitempty" yaml:"DeductionValue1,omitempty"`
	DeductionValue2  string `xml:"DeductionValue2,omitempty" json:"DeductionValue2,omitempty" yaml:"DeductionValue2,omitempty"`
	Id               int    `xml:"Id,omitempty" json:"Id,omitempty" yaml:"Id,omitempty"`
}

// PayslipPayments was auto-generated from WSDL.
type PayslipPayments struct {
	PayementElement    string `xml:"PayementElement,omitempty" json:"PayementElement,omitempty" yaml:"PayementElement,omitempty"`
	PaymentInputValue1 string `xml:"PaymentInputValue1,omitempty" json:"PaymentInputValue1,omitempty" yaml:"PaymentInputValue1,omitempty"`
	PaymentInputValue2 string `xml:"PaymentInputValue2,omitempty" json:"PaymentInputValue2,omitempty" yaml:"PaymentInputValue2,omitempty"`
	PaymentInputvalue3 string `xml:"PaymentInputvalue3,omitempty" json:"PaymentInputvalue3,omitempty" yaml:"PaymentInputvalue3,omitempty"`
	PaymentValue1      string `xml:"PaymentValue1,omitempty" json:"PaymentValue1,omitempty" yaml:"PaymentValue1,omitempty"`
	PayslipPaymentId   int    `xml:"PayslipPaymentId,omitempty" json:"PayslipPaymentId,omitempty" yaml:"PayslipPaymentId,omitempty"`
}

// PerformanceEvaluationComments was auto-generated from WSDL.
type PerformanceEvaluationComments struct {
	ByWhen     DateTime `xml:"ByWhen,omitempty" json:"ByWhen,omitempty" yaml:"ByWhen,omitempty"`
	Comment    string   `xml:"Comment,omitempty" json:"Comment,omitempty" yaml:"Comment,omitempty"`
	ReferredBy string   `xml:"ReferredBy,omitempty" json:"ReferredBy,omitempty" yaml:"ReferredBy,omitempty"`
	ReferredTo string   `xml:"ReferredTo,omitempty" json:"ReferredTo,omitempty" yaml:"ReferredTo,omitempty"`
}

// PerformanceEvaluationReferrals was auto-generated from WSDL.
type PerformanceEvaluationReferrals struct {
	AssignmentCode          string                                `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Comments                *ArrayOfPerformanceEvaluationComments `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
	EndTime                 DateTime                              `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Flexibleworker          *FWDetails                            `xml:"Flexibleworker,omitempty" json:"Flexibleworker,omitempty" yaml:"Flexibleworker,omitempty"`
	Location                *Location                             `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	MustCompleteBefore      DateTime                              `xml:"MustCompleteBefore,omitempty" json:"MustCompleteBefore,omitempty" yaml:"MustCompleteBefore,omitempty"`
	PerformanceEvaluationID int                                   `xml:"PerformanceEvaluationID,omitempty" json:"PerformanceEvaluationID,omitempty" yaml:"PerformanceEvaluationID,omitempty"`
	ReferenceNumber         int                                   `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	ReferredBy              string                                `xml:"ReferredBy,omitempty" json:"ReferredBy,omitempty" yaml:"ReferredBy,omitempty"`
	ShiftDate               DateTime                              `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	StartTime               DateTime                              `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	Ward                    *Ward                                 `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// PerformanceEvaluation_Answer was auto-generated from WSDL.
type PerformanceEvaluation_Answer struct {
	AddressedDuringAssignment bool   `xml:"AddressedDuringAssignment,omitempty" json:"AddressedDuringAssignment,omitempty" yaml:"AddressedDuringAssignment,omitempty"`
	AnswerID                  string `xml:"AnswerID,omitempty" json:"AnswerID,omitempty" yaml:"AnswerID,omitempty"`
	AnswerSelected            bool   `xml:"AnswerSelected,omitempty" json:"AnswerSelected,omitempty" yaml:"AnswerSelected,omitempty"`
	Reason                    string `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
}

// PersonalInformationResponse was auto-generated from WSDL.
type PersonalInformationResponse struct {
	ErrorCode          int      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription   string   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId         int      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ContractEndDate    DateTime `xml:"ContractEndDate,omitempty" json:"ContractEndDate,omitempty" yaml:"ContractEndDate,omitempty"`
	ContractStartDate  DateTime `xml:"ContractStartDate,omitempty" json:"ContractStartDate,omitempty" yaml:"ContractStartDate,omitempty"`
	DateOfBirth        DateTime `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Disability         string   `xml:"Disability,omitempty" json:"Disability,omitempty" yaml:"Disability,omitempty"`
	DisabilityStatus   bool     `xml:"DisabilityStatus,omitempty" json:"DisabilityStatus,omitempty" yaml:"DisabilityStatus,omitempty"`
	ESRNumber          string   `xml:"ESRNumber,omitempty" json:"ESRNumber,omitempty" yaml:"ESRNumber,omitempty"`
	EthnicOrigin       string   `xml:"EthnicOrigin,omitempty" json:"EthnicOrigin,omitempty" yaml:"EthnicOrigin,omitempty"`
	Gender             string   `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	Name               *Name    `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	Nationality        string   `xml:"Nationality,omitempty" json:"Nationality,omitempty" yaml:"Nationality,omitempty"`
	NiNumber           string   `xml:"NiNumber,omitempty" json:"NiNumber,omitempty" yaml:"NiNumber,omitempty"`
	PreferredName      string   `xml:"PreferredName,omitempty" json:"PreferredName,omitempty" yaml:"PreferredName,omitempty"`
	ReasonForEnd       string   `xml:"ReasonForEnd,omitempty" json:"ReasonForEnd,omitempty" yaml:"ReasonForEnd,omitempty"`
	SmartCardActive    bool     `xml:"SmartCardActive,omitempty" json:"SmartCardActive,omitempty" yaml:"SmartCardActive,omitempty"`
	SmartCardIssueDate DateTime `xml:"SmartCardIssueDate,omitempty" json:"SmartCardIssueDate,omitempty" yaml:"SmartCardIssueDate,omitempty"`
	SmartCardUniqueId  string   `xml:"SmartCardUniqueId,omitempty" json:"SmartCardUniqueId,omitempty" yaml:"SmartCardUniqueId,omitempty"`
	StaffType          string   `xml:"StaffType,omitempty" json:"StaffType,omitempty" yaml:"StaffType,omitempty"`
	Title              string   `xml:"Title,omitempty" json:"Title,omitempty" yaml:"Title,omitempty"`
	WebUserId          string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
}

// PersonalisedRatesRequests was auto-generated from WSDL.
type PersonalisedRatesRequests struct {
	Agency    *Agency                         `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	Comments  *ArrayOfCommentsComplexType     `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
	RequestId int                             `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Status    PersonalisedRatesRequest_Status `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	Trust     *Trust                          `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
}

// PriorQualificataion was auto-generated from WSDL.
type PriorQualificataion struct {
	AWName               string                      `xml:"AWName,omitempty" json:"AWName,omitempty" yaml:"AWName,omitempty"`
	Agency               *Agency                     `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	Assignment           string                      `xml:"Assignment,omitempty" json:"Assignment,omitempty" yaml:"Assignment,omitempty"`
	Comments             *ArrayOfCommentsComplexType `xml:"Comments,omitempty" json:"Comments,omitempty" yaml:"Comments,omitempty"`
	Duration             int                         `xml:"Duration,omitempty" json:"Duration,omitempty" yaml:"Duration,omitempty"`
	PriorQualificationID int                         `xml:"PriorQualificationID,omitempty" json:"PriorQualificationID,omitempty" yaml:"PriorQualificationID,omitempty"`
	StartDate            DateTime                    `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	Status               string                      `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
}

// PushNotification was auto-generated from WSDL.
type PushNotification struct {
	AssignmentCode     string          `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Changed            string          `xml:"Changed,omitempty" json:"Changed,omitempty" yaml:"Changed,omitempty"`
	EndTime            DateTime        `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	History            *ArrayOfHistory `xml:"History,omitempty" json:"History,omitempty" yaml:"History,omitempty"`
	InductionRequired  string          `xml:"InductionRequired,omitempty" json:"InductionRequired,omitempty" yaml:"InductionRequired,omitempty"`
	Location           string          `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	Name               string          `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	NoteId             string          `xml:"NoteId,omitempty" json:"NoteId,omitempty" yaml:"NoteId,omitempty"`
	PushNotificationID int             `xml:"PushNotificationID,omitempty" json:"PushNotificationID,omitempty" yaml:"PushNotificationID,omitempty"`
	RecentChange       string          `xml:"RecentChange,omitempty" json:"RecentChange,omitempty" yaml:"RecentChange,omitempty"`
	RefNumber          int             `xml:"RefNumber,omitempty" json:"RefNumber,omitempty" yaml:"RefNumber,omitempty"`
	ShiftDate          DateTime        `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftType          ShiftType       `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	StartTime          DateTime        `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	Trust              string          `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward               string          `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// Query was auto-generated from WSDL.
type Query struct {
	ByWhen    DateTime `xml:"ByWhen,omitempty" json:"ByWhen,omitempty" yaml:"ByWhen,omitempty"`
	ByWhom    string   `xml:"ByWhom,omitempty" json:"ByWhom,omitempty" yaml:"ByWhom,omitempty"`
	QueryText string   `xml:"QueryText,omitempty" json:"QueryText,omitempty" yaml:"QueryText,omitempty"`
}

// QueryTimeSheet was auto-generated from WSDL.
type QueryTimeSheet struct {
	XMLName         xml.Name `xml:"http://tempuri.org/ QueryTimeSheet" json:"-" yaml:"-" action:"http://tempuri.org/IClient/QueryTimeSheet"`
	RequestId       int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId       string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	ReferenceNumber int      `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	RequesterId     string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	Text            string   `xml:"Text,omitempty" json:"Text,omitempty" yaml:"Text,omitempty"`
	CallerToken     string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// QueryTimeSheetResponse was auto-generated from WSDL.
type QueryTimeSheetResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// RateComplexType was auto-generated from WSDL.
type RateComplexType struct {
	AgencyFee      float64 `xml:"AgencyFee,omitempty" json:"AgencyFee,omitempty" yaml:"AgencyFee,omitempty"`
	AssignmentCode string  `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Band           string  `xml:"Band,omitempty" json:"Band,omitempty" yaml:"Band,omitempty"`
	PaymentMethod  string  `xml:"PaymentMethod,omitempty" json:"PaymentMethod,omitempty" yaml:"PaymentMethod,omitempty"`
	PrizeZone      string  `xml:"PrizeZone,omitempty" json:"PrizeZone,omitempty" yaml:"PrizeZone,omitempty"`
	RateType       string  `xml:"RateType,omitempty" json:"RateType,omitempty" yaml:"RateType,omitempty"`
	RateValue      float64 `xml:"RateValue,omitempty" json:"RateValue,omitempty" yaml:"RateValue,omitempty"`
	Sector         string  `xml:"Sector,omitempty" json:"Sector,omitempty" yaml:"Sector,omitempty"`
	Shift          string  `xml:"Shift,omitempty" json:"Shift,omitempty" yaml:"Shift,omitempty"`
	TimePeriod     string  `xml:"TimePeriod,omitempty" json:"TimePeriod,omitempty" yaml:"TimePeriod,omitempty"`
	TotalCharge    float64 `xml:"TotalCharge,omitempty" json:"TotalCharge,omitempty" yaml:"TotalCharge,omitempty"`
}

// ReactivateStaffResponse was auto-generated from WSDL.
type ReactivateStaffResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// ReasonCode was auto-generated from WSDL.
type ReasonCode struct {
	AllowDeactivation            bool   `xml:"AllowDeactivation,omitempty" json:"AllowDeactivation,omitempty" yaml:"AllowDeactivation,omitempty"`
	ClockStatus                  string `xml:"ClockStatus,omitempty" json:"ClockStatus,omitempty" yaml:"ClockStatus,omitempty"`
	ContinuedService             bool   `xml:"ContinuedService,omitempty" json:"ContinuedService,omitempty" yaml:"ContinuedService,omitempty"`
	EvidenceAddress              string `xml:"EvidenceAddress,omitempty" json:"EvidenceAddress,omitempty" yaml:"EvidenceAddress,omitempty"`
	EvidenceDocument             string `xml:"EvidenceDocument,omitempty" json:"EvidenceDocument,omitempty" yaml:"EvidenceDocument,omitempty"`
	Extendable                   bool   `xml:"Extendable,omitempty" json:"Extendable,omitempty" yaml:"Extendable,omitempty"`
	InformationURL               string `xml:"InformationURL,omitempty" json:"InformationURL,omitempty" yaml:"InformationURL,omitempty"`
	MaximumDuration              int    `xml:"MaximumDuration,omitempty" json:"MaximumDuration,omitempty" yaml:"MaximumDuration,omitempty"`
	RequestStatus                string `xml:"RequestStatus,omitempty" json:"RequestStatus,omitempty" yaml:"RequestStatus,omitempty"`
	ShiftBookableDuringPeriod    bool   `xml:"ShiftBookableDuringPeriod,omitempty" json:"ShiftBookableDuringPeriod,omitempty" yaml:"ShiftBookableDuringPeriod,omitempty"`
	StatutoryPay                 bool   `xml:"StatutoryPay,omitempty" json:"StatutoryPay,omitempty" yaml:"StatutoryPay,omitempty"`
	StatutoryPayTextMessage      string `xml:"StatutoryPayTextMessage,omitempty" json:"StatutoryPayTextMessage,omitempty" yaml:"StatutoryPayTextMessage,omitempty"`
	UnAvailableReasonCode        string `xml:"UnAvailableReasonCode,omitempty" json:"UnAvailableReasonCode,omitempty" yaml:"UnAvailableReasonCode,omitempty"`
	UnAvailableReasonDescription string `xml:"UnAvailableReasonDescription,omitempty" json:"UnAvailableReasonDescription,omitempty" yaml:"UnAvailableReasonDescription,omitempty"`
}

// Reasons was auto-generated from WSDL.
type Reasons struct {
	Code        string `xml:"Code,omitempty" json:"Code,omitempty" yaml:"Code,omitempty"`
	Description string `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
}

// ReceiverDetails was auto-generated from WSDL.
type ReceiverDetails struct {
	Address        *FWAddress         `xml:"Address,omitempty" json:"Address,omitempty" yaml:"Address,omitempty"`
	ContactDetails *ArrayOfFWContacts `xml:"ContactDetails,omitempty" json:"ContactDetails,omitempty" yaml:"ContactDetails,omitempty"`
	Name           string             `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	StaffID        string             `xml:"StaffID,omitempty" json:"StaffID,omitempty" yaml:"StaffID,omitempty"`
}

// Recurrence was auto-generated from WSDL.
type Recurrence struct {
	DailyRecurrenceType   string       `xml:"DailyRecurrenceType,omitempty" json:"DailyRecurrenceType,omitempty" yaml:"DailyRecurrenceType,omitempty"`
	DayNumber             DateTime     `xml:"DayNumber,omitempty" json:"DayNumber,omitempty" yaml:"DayNumber,omitempty"`
	DayOfMonth            DateTime     `xml:"DayOfMonth,omitempty" json:"DayOfMonth,omitempty" yaml:"DayOfMonth,omitempty"`
	DaySequence           string       `xml:"DaySequence,omitempty" json:"DaySequence,omitempty" yaml:"DaySequence,omitempty"`
	Month                 int          `xml:"Month,omitempty" json:"Month,omitempty" yaml:"Month,omitempty"`
	MonthlyRecurrenceType string       `xml:"MonthlyRecurrenceType,omitempty" json:"MonthlyRecurrenceType,omitempty" yaml:"MonthlyRecurrenceType,omitempty"`
	Months                int          `xml:"Months,omitempty" json:"Months,omitempty" yaml:"Months,omitempty"`
	NoOfDays              int          `xml:"NoOfDays,omitempty" json:"NoOfDays,omitempty" yaml:"NoOfDays,omitempty"`
	NoOfYears             int          `xml:"NoOfYears,omitempty" json:"NoOfYears,omitempty" yaml:"NoOfYears,omitempty"`
	NumberOfWeeks         string       `xml:"NumberOfWeeks,omitempty" json:"NumberOfWeeks,omitempty" yaml:"NumberOfWeeks,omitempty"`
	RecurringDays         *ArrayOfchar `xml:"RecurringDays,omitempty" json:"RecurringDays,omitempty" yaml:"RecurringDays,omitempty"`
	RecurrncePatternType  string       `xml:"RecurrncePatternType,omitempty" json:"RecurrncePatternType,omitempty" yaml:"RecurrncePatternType,omitempty"`
	WeekDay               string       `xml:"WeekDay,omitempty" json:"WeekDay,omitempty" yaml:"WeekDay,omitempty"`
	YearlyRecurrenceType  string       `xml:"YearlyRecurrenceType,omitempty" json:"YearlyRecurrenceType,omitempty" yaml:"YearlyRecurrenceType,omitempty"`
}

// RefusedBookings was auto-generated from WSDL.
type RefusedBookings struct {
	AgencyId                        string                      `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	AgencyName                      string                      `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	AssignmentCode                  string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Gender                          string                      `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	HasComment                      bool                        `xml:"HasComment,omitempty" json:"HasComment,omitempty" yaml:"HasComment,omitempty"`
	IsRangeRequest                  bool                        `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	Location                        string                      `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	NoAgency                        bool                        `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Notes                           *ArrayOfCommentsComplexType `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	OverlappingBookingRefNum        *ArrayOfint                 `xml:"OverlappingBookingRefNum,omitempty" json:"OverlappingBookingRefNum,omitempty" yaml:"OverlappingBookingRefNum,omitempty"`
	PartialMatch                    bool                        `xml:"PartialMatch,omitempty" json:"PartialMatch,omitempty" yaml:"PartialMatch,omitempty"`
	RangeFilledBy                   string                      `xml:"RangeFilledBy,omitempty" json:"RangeFilledBy,omitempty" yaml:"RangeFilledBy,omitempty"`
	SecondaryAssignmentCode         string                      `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration int                         `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftBookingReferenceNumber     int                         `xml:"ShiftBookingReferenceNumber,omitempty" json:"ShiftBookingReferenceNumber,omitempty" yaml:"ShiftBookingReferenceNumber,omitempty"`
	ShiftDate                       DateTime                    `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftEndTime                    DateTime                    `xml:"ShiftEndTime,omitempty" json:"ShiftEndTime,omitempty" yaml:"ShiftEndTime,omitempty"`
	ShiftStartTime                  DateTime                    `xml:"ShiftStartTime,omitempty" json:"ShiftStartTime,omitempty" yaml:"ShiftStartTime,omitempty"`
	ShiftStatus                     string                      `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	ShiftTimeType                   string                      `xml:"ShiftTimeType,omitempty" json:"ShiftTimeType,omitempty" yaml:"ShiftTimeType,omitempty"`
	ShiftType                       string                      `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	Trust                           string                      `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward                            string                      `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WardDetails                     *WardDetails                `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
	Reason                          string                      `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	RefusedBy                       string                      `xml:"RefusedBy,omitempty" json:"RefusedBy,omitempty" yaml:"RefusedBy,omitempty"`
}

// ReleaseTimeSheetResponse was auto-generated from WSDL.
type ReleaseTimeSheetResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// RemoveAssignmentFromFlexibleWorkerResponse was auto-generated
// from WSDL.
type RemoveAssignmentFromFlexibleWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// RemoveTrustFromFlexibleWorkerResponse was auto-generated from
// WSDL.
type RemoveTrustFromFlexibleWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// RightToWorkResponse was auto-generated from WSDL.
type RightToWorkResponse struct {
	ErrorCode              int                            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription       string                         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId             int                            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ARCSerialNumber        string                         `xml:"ARCSerialNumber,omitempty" json:"ARCSerialNumber,omitempty" yaml:"ARCSerialNumber,omitempty"`
	ApplicationRefNumber   string                         `xml:"ApplicationRefNumber,omitempty" json:"ApplicationRefNumber,omitempty" yaml:"ApplicationRefNumber,omitempty"`
	AppliedForExtension    string                         `xml:"AppliedForExtension,omitempty" json:"AppliedForExtension,omitempty" yaml:"AppliedForExtension,omitempty"`
	CourseName             string                         `xml:"CourseName,omitempty" json:"CourseName,omitempty" yaml:"CourseName,omitempty"`
	EvidenceSeen           *ArrayOfEvidenceDetail         `xml:"EvidenceSeen,omitempty" json:"EvidenceSeen,omitempty" yaml:"EvidenceSeen,omitempty"`
	ExpiryDate             DateTime                       `xml:"ExpiryDate,omitempty" json:"ExpiryDate,omitempty" yaml:"ExpiryDate,omitempty"`
	ExtensionExpiryDate    DateTime                       `xml:"ExtensionExpiryDate,omitempty" json:"ExtensionExpiryDate,omitempty" yaml:"ExtensionExpiryDate,omitempty"`
	IFBRefNumber           string                         `xml:"IFBRefNumber,omitempty" json:"IFBRefNumber,omitempty" yaml:"IFBRefNumber,omitempty"`
	InstitutionName        string                         `xml:"InstitutionName,omitempty" json:"InstitutionName,omitempty" yaml:"InstitutionName,omitempty"`
	RTWProfile             string                         `xml:"RTWProfile,omitempty" json:"RTWProfile,omitempty" yaml:"RTWProfile,omitempty"`
	UnrestrictedWorkPeriod *ArrayOfUnrestrictedWorkPeriod `xml:"UnrestrictedWorkPeriod,omitempty" json:"UnrestrictedWorkPeriod,omitempty" yaml:"UnrestrictedWorkPeriod,omitempty"`
	VisaNumber             string                         `xml:"VisaNumber,omitempty" json:"VisaNumber,omitempty" yaml:"VisaNumber,omitempty"`
	VisaReview             *ArrayOfVisaReview             `xml:"VisaReview,omitempty" json:"VisaReview,omitempty" yaml:"VisaReview,omitempty"`
	WeeklyHoursRestriction int                            `xml:"WeeklyHoursRestriction,omitempty" json:"WeeklyHoursRestriction,omitempty" yaml:"WeeklyHoursRestriction,omitempty"`
}

// SearchFlexibleWorker was auto-generated from WSDL.
type SearchFlexibleWorker struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ SearchFlexibleWorker" json:"-" yaml:"-" action:"http://tempuri.org/IClient/SearchFlexibleWorker"`
	FirstName   string   `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	Surname     string   `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// SearchFlexibleWorkerResponse was auto-generated from WSDL.
type SearchFlexibleWorkerResponse struct {
	ErrorCode        int                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	FlexibleWorkers  *ArrayOfFlexibleWorker `xml:"FlexibleWorkers,omitempty" json:"FlexibleWorkers,omitempty" yaml:"FlexibleWorkers,omitempty"`
}

// SearchHolidayBookingResponse was auto-generated from WSDL.
type SearchHolidayBookingResponse struct {
	ErrorCode        int                    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                 `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	HolidayBookings  *ArrayOfHolidayBooking `xml:"HolidayBookings,omitempty" json:"HolidayBookings,omitempty" yaml:"HolidayBookings,omitempty"`
}

// SearchWorker was auto-generated from WSDL.
type SearchWorker struct {
	XMLName        xml.Name `xml:"http://tempuri.org/ SearchWorker" json:"-" yaml:"-" action:"http://tempuri.org/IClient/SearchWorker"`
	RequestId      int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyId       string   `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	FirstName      string   `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	Surname        string   `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
	TrustID        string   `xml:"TrustID,omitempty" json:"TrustID,omitempty" yaml:"TrustID,omitempty"`
	WorkerType     string   `xml:"WorkerType,omitempty" json:"WorkerType,omitempty" yaml:"WorkerType,omitempty"`
	AssignmentCode string   `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Gender         string   `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	CallerToken    string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// SearchWorkerResponse was auto-generated from WSDL.
type SearchWorkerResponse struct {
	ErrorCode        int            `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string         `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int            `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Workers          *ArrayOfWorker `xml:"Workers,omitempty" json:"Workers,omitempty" yaml:"Workers,omitempty"`
}

// SenderDetails was auto-generated from WSDL.
type SenderDetails struct {
	Address  *DNAorSNCFromAddressType `xml:"Address,omitempty" json:"Address,omitempty" yaml:"Address,omitempty"`
	Contact  *DNAorSNCContactType     `xml:"Contact,omitempty" json:"Contact,omitempty" yaml:"Contact,omitempty"`
	Position string                   `xml:"Position,omitempty" json:"Position,omitempty" yaml:"Position,omitempty"`
	WebSite  string                   `xml:"WebSite,omitempty" json:"WebSite,omitempty" yaml:"WebSite,omitempty"`
}

// Shift was auto-generated from WSDL.
type Shift struct {
	AgencyId                        string                      `xml:"AgencyId,omitempty" json:"AgencyId,omitempty" yaml:"AgencyId,omitempty"`
	AgencyName                      string                      `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	AssignmentCode                  string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Gender                          string                      `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	HasComment                      bool                        `xml:"HasComment,omitempty" json:"HasComment,omitempty" yaml:"HasComment,omitempty"`
	IsRangeRequest                  bool                        `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	Location                        string                      `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	NoAgency                        bool                        `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Notes                           *ArrayOfCommentsComplexType `xml:"Notes,omitempty" json:"Notes,omitempty" yaml:"Notes,omitempty"`
	OverlappingBookingRefNum        *ArrayOfint                 `xml:"OverlappingBookingRefNum,omitempty" json:"OverlappingBookingRefNum,omitempty" yaml:"OverlappingBookingRefNum,omitempty"`
	PartialMatch                    bool                        `xml:"PartialMatch,omitempty" json:"PartialMatch,omitempty" yaml:"PartialMatch,omitempty"`
	RangeFilledBy                   string                      `xml:"RangeFilledBy,omitempty" json:"RangeFilledBy,omitempty" yaml:"RangeFilledBy,omitempty"`
	SecondaryAssignmentCode         string                      `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration int                         `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftBookingReferenceNumber     int                         `xml:"ShiftBookingReferenceNumber,omitempty" json:"ShiftBookingReferenceNumber,omitempty" yaml:"ShiftBookingReferenceNumber,omitempty"`
	ShiftDate                       DateTime                    `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftEndTime                    DateTime                    `xml:"ShiftEndTime,omitempty" json:"ShiftEndTime,omitempty" yaml:"ShiftEndTime,omitempty"`
	ShiftStartTime                  DateTime                    `xml:"ShiftStartTime,omitempty" json:"ShiftStartTime,omitempty" yaml:"ShiftStartTime,omitempty"`
	ShiftStatus                     string                      `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	ShiftTimeType                   string                      `xml:"ShiftTimeType,omitempty" json:"ShiftTimeType,omitempty" yaml:"ShiftTimeType,omitempty"`
	ShiftType                       string                      `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	Trust                           string                      `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	Ward                            string                      `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WardDetails                     *WardDetails                `xml:"WardDetails,omitempty" json:"WardDetails,omitempty" yaml:"WardDetails,omitempty"`
}

// ShiftDetails was auto-generated from WSDL.
type ShiftDetails struct {
	BreakEnd1     DateTime `xml:"BreakEnd1,omitempty" json:"BreakEnd1,omitempty" yaml:"BreakEnd1,omitempty"`
	BreakEnd2     DateTime `xml:"BreakEnd2,omitempty" json:"BreakEnd2,omitempty" yaml:"BreakEnd2,omitempty"`
	BreakStart1   DateTime `xml:"BreakStart1,omitempty" json:"BreakStart1,omitempty" yaml:"BreakStart1,omitempty"`
	BreakStart2   DateTime `xml:"BreakStart2,omitempty" json:"BreakStart2,omitempty" yaml:"BreakStart2,omitempty"`
	EndTime       DateTime `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	IsNormalShift bool     `xml:"IsNormalShift,omitempty" json:"IsNormalShift,omitempty" yaml:"IsNormalShift,omitempty"`
	ShiftColor    int      `xml:"ShiftColor,omitempty" json:"ShiftColor,omitempty" yaml:"ShiftColor,omitempty"`
	ShiftName     string   `xml:"ShiftName,omitempty" json:"ShiftName,omitempty" yaml:"ShiftName,omitempty"`
	StartTime     DateTime `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
}

// ShiftDetails_FW was auto-generated from WSDL.
type ShiftDetails_FW struct {
	AssignmentCode                             string     `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	Date                                       DateTime   `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	EndTime                                    DateTime   `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	HoursPriorToShiftStartToMatchSecAssignment float64    `xml:"HoursPriorToShiftStartToMatchSecAssignment,omitempty" json:"HoursPriorToShiftStartToMatchSecAssignment,omitempty" yaml:"HoursPriorToShiftStartToMatchSecAssignment,omitempty"`
	LocationID                                 string     `xml:"LocationID,omitempty" json:"LocationID,omitempty" yaml:"LocationID,omitempty"`
	SecondaryAssignmentCode                    string     `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	Shift_Type                                 ShiftType  `xml:"Shift_Type,omitempty" json:"Shift_Type,omitempty" yaml:"Shift_Type,omitempty"`
	StartTime                                  DateTime   `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	TrustID                                    string     `xml:"TrustID,omitempty" json:"TrustID,omitempty" yaml:"TrustID,omitempty"`
	WardID                                     string     `xml:"WardID,omitempty" json:"WardID,omitempty" yaml:"WardID,omitempty"`
	Gender                                     GenderEnum `xml:"gender,omitempty" json:"gender,omitempty" yaml:"gender,omitempty"`
}

// ShiftsRequest was auto-generated from WSDL.
type ShiftsRequest struct {
	XMLName        xml.Name         `xml:"http://tempuri.org/ ShiftsRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/ShiftsRequest"`
	RequestId      int              `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	AgencyName     string           `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	AssignmentCode string           `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	StaffId        string           `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	DateRange      IVRDateRangeEnum `xml:"DateRange,omitempty" json:"DateRange,omitempty" yaml:"DateRange,omitempty"`
	WebUserId      string           `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	TrustId        string           `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	LocationId     string           `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	WardId         string           `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	Date           DateTime         `xml:"Date,omitempty" json:"Date,omitempty" yaml:"Date,omitempty"`
	ShiftStatus    string           `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	Shift_Types    *TypeOfShifts    `xml:"Shift_Types,omitempty" json:"Shift_Types,omitempty" yaml:"Shift_Types,omitempty"`
	StartTime      DateTime         `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	StartDate      DateTime         `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	EndDate        DateTime         `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	CallerToken    string           `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// ShiftsRequestResponse was auto-generated from WSDL.
type ShiftsRequestResponse struct {
	ShiftsRequestResult *ShiftsResponse `xml:"ShiftsRequestResult,omitempty" json:"ShiftsRequestResult,omitempty" yaml:"ShiftsRequestResult,omitempty"`
}

// ShiftsResponse was auto-generated from WSDL.
type ShiftsResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Shift            *ArrayOfIVRShiftType `xml:"Shift,omitempty" json:"Shift,omitempty" yaml:"Shift,omitempty"`
}

// ShiftsSinceResponse was auto-generated from WSDL.
type ShiftsSinceResponse struct {
	ErrorCode        int                 `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string              `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                 `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BookingsData     *ArrayOfBookingData `xml:"BookingsData,omitempty" json:"BookingsData,omitempty" yaml:"BookingsData,omitempty"`
}

// SingleSignOnEncryptedInformationResponse was auto-generated
// from WSDL.
type SingleSignOnEncryptedInformationResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	EncryptionKey    string `xml:"EncryptionKey,omitempty" json:"EncryptionKey,omitempty" yaml:"EncryptionKey,omitempty"`
}

// SingleSignOnLink was auto-generated from WSDL.
type SingleSignOnLink struct {
	Id   string `xml:"Id,omitempty" json:"Id,omitempty" yaml:"Id,omitempty"`
	Link string `xml:"Link,omitempty" json:"Link,omitempty" yaml:"Link,omitempty"`
	Name string `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
}

// SingleSignOnLinkResponse was auto-generated from WSDL.
type SingleSignOnLinkResponse struct {
	ErrorCode         int                      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription  string                   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId        int                      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	SingleSignOnLinks *ArrayOfSingleSignOnLink `xml:"SingleSignOnLinks,omitempty" json:"SingleSignOnLinks,omitempty" yaml:"SingleSignOnLinks,omitempty"`
}

// SplitShift was auto-generated from WSDL.
type SplitShift struct {
	XMLName           xml.Name `xml:"http://tempuri.org/ SplitShift" json:"-" yaml:"-" action:"http://tempuri.org/IClient/SplitShift"`
	RequestId         int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Assignment        string   `xml:"Assignment,omitempty" json:"Assignment,omitempty" yaml:"Assignment,omitempty"`
	AuthorizationCode string   `xml:"AuthorizationCode,omitempty" json:"AuthorizationCode,omitempty" yaml:"AuthorizationCode,omitempty"`
	StartTime         DateTime `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	EndTime           DateTime `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Location          string   `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	OldEndTime        DateTime `xml:"OldEndTime,omitempty" json:"OldEndTime,omitempty" yaml:"OldEndTime,omitempty"`
	ReferenceNumber   int      `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	Ward              string   `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WebUser           string   `xml:"webUser,omitempty" json:"webUser,omitempty" yaml:"webUser,omitempty"`
	RequesterId       string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	CallerToken       string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// SplitShiftResponse was auto-generated from WSDL.
type SplitShiftResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ReferenceNumber  int    `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
}

// StaffBankInterfaceResponse was auto-generated from WSDL.
type StaffBankInterfaceResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// StaffFacadeType was auto-generated from WSDL.
type StaffFacadeType struct {
	AgencyStaffFacadeType *AgencyStaffFacadeType `xml:"AgencyStaffFacadeType,omitempty" json:"AgencyStaffFacadeType,omitempty" yaml:"AgencyStaffFacadeType,omitempty"`
	BankStaffFacadeType   *BankStaffFacadeType   `xml:"BankStaffFacadeType,omitempty" json:"BankStaffFacadeType,omitempty" yaml:"BankStaffFacadeType,omitempty"`
}

// StaffGroup was auto-generated from WSDL.
type StaffGroup struct {
	StaffGroupID   string `xml:"StaffGroupID,omitempty" json:"StaffGroupID,omitempty" yaml:"StaffGroupID,omitempty"`
	StaffGroupName string `xml:"StaffGroupName,omitempty" json:"StaffGroupName,omitempty" yaml:"StaffGroupName,omitempty"`
}

// StaffRequest was auto-generated from WSDL.
type StaffRequest struct {
	XMLName        xml.Name  `xml:"http://tempuri.org/ StaffRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/StaffRequest"`
	AssignmentCode string    `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	DateOfBirth    DateTime  `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	Name           *NameType `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	PhoneNumber    string    `xml:"PhoneNumber,omitempty" json:"PhoneNumber,omitempty" yaml:"PhoneNumber,omitempty"`
	TrustId        string    `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	RequestId      int       `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken    string    `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// StaffRequestResponse was auto-generated from WSDL.
type StaffRequestResponse struct {
	StaffRequestResult *StaffResponse `xml:"StaffRequestResult,omitempty" json:"StaffRequestResult,omitempty" yaml:"StaffRequestResult,omitempty"`
}

// StaffResponse was auto-generated from WSDL.
type StaffResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	BankStaff        *ArrayOfBankStaff `xml:"BankStaff,omitempty" json:"BankStaff,omitempty" yaml:"BankStaff,omitempty"`
}

// SubstantiveWorkerHolidays was auto-generated from WSDL.
type SubstantiveWorkerHolidays struct {
	HolidayEndDate   DateTime `xml:"HolidayEndDate,omitempty" json:"HolidayEndDate,omitempty" yaml:"HolidayEndDate,omitempty"`
	HolidayID        int      `xml:"HolidayID,omitempty" json:"HolidayID,omitempty" yaml:"HolidayID,omitempty"`
	HolidayStartDate DateTime `xml:"HolidayStartDate,omitempty" json:"HolidayStartDate,omitempty" yaml:"HolidayStartDate,omitempty"`
	IsConfirmed      bool     `xml:"IsConfirmed,omitempty" json:"IsConfirmed,omitempty" yaml:"IsConfirmed,omitempty"`
}

// SuggestedWorkerRequest was auto-generated from WSDL.
type SuggestedWorkerRequest struct {
	XMLName                      xml.Name         `xml:"http://tempuri.org/ SuggestedWorkerRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/SuggestedWorkerRequest"`
	RequestId                    int              `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	IgnoreOverridableValidations bool             `xml:"IgnoreOverridableValidations,omitempty" json:"IgnoreOverridableValidations,omitempty" yaml:"IgnoreOverridableValidations,omitempty"`
	ShiftDetails_FW              *ShiftDetails_FW `xml:"ShiftDetails_FW,omitempty" json:"ShiftDetails_FW,omitempty" yaml:"ShiftDetails_FW,omitempty"`
	ShiftReferenceNumber         int              `xml:"ShiftReferenceNumber,omitempty" json:"ShiftReferenceNumber,omitempty" yaml:"ShiftReferenceNumber,omitempty"`
	CallerToken                  string           `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// SuggestedWorkerRequestResponse was auto-generated from WSDL.
type SuggestedWorkerRequestResponse struct {
	SuggestedWorkerRequestResult *SuggestedWorkerResponse `xml:"SuggestedWorkerRequestResult,omitempty" json:"SuggestedWorkerRequestResult,omitempty" yaml:"SuggestedWorkerRequestResult,omitempty"`
}

// SuggestedWorkerResponse was auto-generated from WSDL.
type SuggestedWorkerResponse struct {
	ErrorCode        int                      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	SuggestedWorkers *ArrayOfSuggestedWorkers `xml:"SuggestedWorkers,omitempty" json:"SuggestedWorkers,omitempty" yaml:"SuggestedWorkers,omitempty"`
}

// SuggestedWorkers was auto-generated from WSDL.
type SuggestedWorkers struct {
	FirstName string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	NINumber  string `xml:"NINumber,omitempty" json:"NINumber,omitempty" yaml:"NINumber,omitempty"`
	NurseID   string `xml:"NurseID,omitempty" json:"NurseID,omitempty" yaml:"NurseID,omitempty"`
	SurName   string `xml:"SurName,omitempty" json:"SurName,omitempty" yaml:"SurName,omitempty"`
}

// TemporalNonAvailability was auto-generated from WSDL.
type TemporalNonAvailability struct {
	Description               string   `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
	EndDate                   DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate                 DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	TemporalNonAvailabilityId string   `xml:"TemporalNonAvailabilityId,omitempty" json:"TemporalNonAvailabilityId,omitempty" yaml:"TemporalNonAvailabilityId,omitempty"`
	Type                      string   `xml:"Type,omitempty" json:"Type,omitempty" yaml:"Type,omitempty"`
}

// TimeSheet was auto-generated from WSDL.
type TimeSheet struct {
	ActualBreakTime        DateTime        `xml:"ActualBreakTime,omitempty" json:"ActualBreakTime,omitempty" yaml:"ActualBreakTime,omitempty"`
	ActualEndTime          DateTime        `xml:"ActualEndTime,omitempty" json:"ActualEndTime,omitempty" yaml:"ActualEndTime,omitempty"`
	ActualStartTime        DateTime        `xml:"ActualStartTime,omitempty" json:"ActualStartTime,omitempty" yaml:"ActualStartTime,omitempty"`
	ActualTotalTime        DateTime        `xml:"ActualTotalTime,omitempty" json:"ActualTotalTime,omitempty" yaml:"ActualTotalTime,omitempty"`
	AgencyCode             string          `xml:"AgencyCode,omitempty" json:"AgencyCode,omitempty" yaml:"AgencyCode,omitempty"`
	AgencyName             string          `xml:"AgencyName,omitempty" json:"AgencyName,omitempty" yaml:"AgencyName,omitempty"`
	AssignmentCode         string          `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	AuthorisedBy           string          `xml:"AuthorisedBy,omitempty" json:"AuthorisedBy,omitempty" yaml:"AuthorisedBy,omitempty"`
	AuthorisedDate         DateTime        `xml:"AuthorisedDate,omitempty" json:"AuthorisedDate,omitempty" yaml:"AuthorisedDate,omitempty"`
	BookingReason          *BookingReason  `xml:"BookingReason,omitempty" json:"BookingReason,omitempty" yaml:"BookingReason,omitempty"`
	BreakTime              DateTime        `xml:"BreakTime,omitempty" json:"BreakTime,omitempty" yaml:"BreakTime,omitempty"`
	EndTime                DateTime        `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	FirstName              string          `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	InductionDelivered     bool            `xml:"InductionDelivered,omitempty" json:"InductionDelivered,omitempty" yaml:"InductionDelivered,omitempty"`
	InductionRequired      bool            `xml:"InductionRequired,omitempty" json:"InductionRequired,omitempty" yaml:"InductionRequired,omitempty"`
	Location               *Location       `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	NurseID                string          `xml:"NurseID,omitempty" json:"NurseID,omitempty" yaml:"NurseID,omitempty"`
	PaymentProcessedOn     DateTime        `xml:"PaymentProcessedOn,omitempty" json:"PaymentProcessedOn,omitempty" yaml:"PaymentProcessedOn,omitempty"`
	QueryText              *ArrayOfQuery   `xml:"QueryText,omitempty" json:"QueryText,omitempty" yaml:"QueryText,omitempty"`
	ReferenceNumber        int             `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	ShiftDate              DateTime        `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftType              ShiftType       `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	StartTime              DateTime        `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	Status                 TimeSheetStatus `xml:"Status,omitempty" json:"Status,omitempty" yaml:"Status,omitempty"`
	SurName                string          `xml:"SurName,omitempty" json:"SurName,omitempty" yaml:"SurName,omitempty"`
	TotalTime              DateTime        `xml:"TotalTime,omitempty" json:"TotalTime,omitempty" yaml:"TotalTime,omitempty"`
	Trust                  *Trust          `xml:"Trust,omitempty" json:"Trust,omitempty" yaml:"Trust,omitempty"`
	TrustAuthorisationCode string          `xml:"TrustAuthorisationCode,omitempty" json:"TrustAuthorisationCode,omitempty" yaml:"TrustAuthorisationCode,omitempty"`
	Verified               bool            `xml:"Verified,omitempty" json:"Verified,omitempty" yaml:"Verified,omitempty"`
	Ward                   *Ward           `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
}

// TimeSheetResponse was auto-generated from WSDL.
type TimeSheetResponse struct {
	ErrorCode        int               `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string            `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int               `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TimeSheets       *ArrayOfTimeSheet `xml:"TimeSheets,omitempty" json:"TimeSheets,omitempty" yaml:"TimeSheets,omitempty"`
}

// TimeSheets was auto-generated from WSDL.
type TimeSheets struct {
	XMLName                xml.Name        `xml:"http://tempuri.org/ TimeSheets" json:"-" yaml:"-" action:"http://tempuri.org/IClient/TimeSheets"`
	RequestId              int             `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	Staffid                string          `xml:"Staffid,omitempty" json:"Staffid,omitempty" yaml:"Staffid,omitempty"`
	WebUserId              string          `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	DateRange              DateRange       `xml:"DateRange,omitempty" json:"DateRange,omitempty" yaml:"DateRange,omitempty"`
	StartDate              DateTime        `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	EndDate                DateTime        `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	Trustid                string          `xml:"Trustid,omitempty" json:"Trustid,omitempty" yaml:"Trustid,omitempty"`
	Locationid             string          `xml:"Locationid,omitempty" json:"Locationid,omitempty" yaml:"Locationid,omitempty"`
	Wardid                 string          `xml:"Wardid,omitempty" json:"Wardid,omitempty" yaml:"Wardid,omitempty"`
	TimeSheetStatus        TimeSheetStatus `xml:"TimeSheetStatus,omitempty" json:"TimeSheetStatus,omitempty" yaml:"TimeSheetStatus,omitempty"`
	BookingReferenceNumber int             `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	CallerToken            string          `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// TimeSheetsResponse was auto-generated from WSDL.
type TimeSheetsResponse struct {
	TimeSheetsResult *TimeSheetResponse `xml:"TimeSheetsResult,omitempty" json:"TimeSheetsResult,omitempty" yaml:"TimeSheetsResult,omitempty"`
}

// Trust was auto-generated from WSDL.
type Trust struct {
	TrustId   string `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	TrustName string `xml:"TrustName,omitempty" json:"TrustName,omitempty" yaml:"TrustName,omitempty"`
}

// TrustUserPermission was auto-generated from WSDL.
type TrustUserPermission struct {
	CanAllowRetrospectiveShiftBooking                       bool `xml:"CanAllowRetrospectiveShiftBooking,omitempty" json:"CanAllowRetrospectiveShiftBooking,omitempty" yaml:"CanAllowRetrospectiveShiftBooking,omitempty"`
	CanAmendTimesheetsAfterPayment                          bool `xml:"CanAmendTimesheetsAfterPayment,omitempty" json:"CanAmendTimesheetsAfterPayment,omitempty" yaml:"CanAmendTimesheetsAfterPayment,omitempty"`
	CanApproveOrQueryOrRejectPriorAWRQualificationRequests  bool `xml:"CanApproveOrQueryOrRejectPriorAWRQualificationRequests,omitempty" json:"CanApproveOrQueryOrRejectPriorAWRQualificationRequests,omitempty" yaml:"CanApproveOrQueryOrRejectPriorAWRQualificationRequests,omitempty"`
	CanAuthoriseAgencySelfBillingAgreement                  bool `xml:"CanAuthoriseAgencySelfBillingAgreement,omitempty" json:"CanAuthoriseAgencySelfBillingAgreement,omitempty" yaml:"CanAuthoriseAgencySelfBillingAgreement,omitempty"`
	CanAuthorisePendingAgencyPayRatesContracts              bool `xml:"CanAuthorisePendingAgencyPayRatesContracts,omitempty" json:"CanAuthorisePendingAgencyPayRatesContracts,omitempty" yaml:"CanAuthorisePendingAgencyPayRatesContracts,omitempty"`
	CanAuthoriseShiftRequestPendingSeniorAdminAuthorisation bool `xml:"CanAuthoriseShiftRequestPendingSeniorAdminAuthorisation,omitempty" json:"CanAuthoriseShiftRequestPendingSeniorAdminAuthorisation,omitempty" yaml:"CanAuthoriseShiftRequestPendingSeniorAdminAuthorisation,omitempty"`
	CanAuthoriseTimesheets                                  bool `xml:"CanAuthoriseTimesheets,omitempty" json:"CanAuthoriseTimesheets,omitempty" yaml:"CanAuthoriseTimesheets,omitempty"`
	CanCreateShiftRequest                                   bool `xml:"CanCreateShiftRequest,omitempty" json:"CanCreateShiftRequest,omitempty" yaml:"CanCreateShiftRequest,omitempty"`
	CanDirectBookAgencyWorkerIntoShiftRequest               bool `xml:"CanDirectBookAgencyWorkerIntoShiftRequest,omitempty" json:"CanDirectBookAgencyWorkerIntoShiftRequest,omitempty" yaml:"CanDirectBookAgencyWorkerIntoShiftRequest,omitempty"`
	CanDirectBookFlexibleWorkerIntoShiftRequest             bool `xml:"CanDirectBookFlexibleWorkerIntoShiftRequest,omitempty" json:"CanDirectBookFlexibleWorkerIntoShiftRequest,omitempty" yaml:"CanDirectBookFlexibleWorkerIntoShiftRequest,omitempty"`
	CanInputTrustAuthorisationCode                          bool `xml:"CanInputTrustAuthorisationCode,omitempty" json:"CanInputTrustAuthorisationCode,omitempty" yaml:"CanInputTrustAuthorisationCode,omitempty"`
	CanModifyCancelShiftRequest                             bool `xml:"CanModifyCancelShiftRequest,omitempty" json:"CanModifyCancelShiftRequest,omitempty" yaml:"CanModifyCancelShiftRequest,omitempty"`
	CanQueryOrRejectOrApprovePersonalisedPayRateRequest     bool `xml:"CanQueryOrRejectOrApprovePersonalisedPayRateRequest,omitempty" json:"CanQueryOrRejectOrApprovePersonalisedPayRateRequest,omitempty" yaml:"CanQueryOrRejectOrApprovePersonalisedPayRateRequest,omitempty"`
	CanRemoveGoldenKey                                      bool `xml:"CanRemoveGoldenKey,omitempty" json:"CanRemoveGoldenKey,omitempty" yaml:"CanRemoveGoldenKey,omitempty"`
	CanRemovePadLock                                        bool `xml:"CanRemovePadLock,omitempty" json:"CanRemovePadLock,omitempty" yaml:"CanRemovePadLock,omitempty"`
	CanSearchUsingTwoTierAuthorisation                      bool `xml:"CanSearchUsingTwoTierAuthorisation,omitempty" json:"CanSearchUsingTwoTierAuthorisation,omitempty" yaml:"CanSearchUsingTwoTierAuthorisation,omitempty"`
	CanVerifyTimesheets                                     bool `xml:"CanVerifyTimesheets,omitempty" json:"CanVerifyTimesheets,omitempty" yaml:"CanVerifyTimesheets,omitempty"`
	CanViewAgencyPayRatesContract                           bool `xml:"CanViewAgencyPayRatesContract,omitempty" json:"CanViewAgencyPayRatesContract,omitempty" yaml:"CanViewAgencyPayRatesContract,omitempty"`
	CanViewAgencySelfBillingAgreements                      bool `xml:"CanViewAgencySelfBillingAgreements,omitempty" json:"CanViewAgencySelfBillingAgreements,omitempty" yaml:"CanViewAgencySelfBillingAgreements,omitempty"`
	CanViewCancelledShiftRequests                           bool `xml:"CanViewCancelledShiftRequests,omitempty" json:"CanViewCancelledShiftRequests,omitempty" yaml:"CanViewCancelledShiftRequests,omitempty"`
	CanViewGeneratedBackingReport                           bool `xml:"CanViewGeneratedBackingReport,omitempty" json:"CanViewGeneratedBackingReport,omitempty" yaml:"CanViewGeneratedBackingReport,omitempty"`
	CanViewPersonalisedPayRates                             bool `xml:"CanViewPersonalisedPayRates,omitempty" json:"CanViewPersonalisedPayRates,omitempty" yaml:"CanViewPersonalisedPayRates,omitempty"`
	CanViewPriorAWRQualifications                           bool `xml:"CanViewPriorAWRQualifications,omitempty" json:"CanViewPriorAWRQualifications,omitempty" yaml:"CanViewPriorAWRQualifications,omitempty"`
	CanViewShiftRequestsPendingAuthorization                bool `xml:"CanViewShiftRequestsPendingAuthorization,omitempty" json:"CanViewShiftRequestsPendingAuthorization,omitempty" yaml:"CanViewShiftRequestsPendingAuthorization,omitempty"`
	CanViewShifts                                           bool `xml:"CanViewShifts,omitempty" json:"CanViewShifts,omitempty" yaml:"CanViewShifts,omitempty"`
}

// TrustUsersWhoCanCompletePerformanceEvaluation was auto-generated
// from WSDL.
type TrustUsersWhoCanCompletePerformanceEvaluation struct {
	FirstName string `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	Surname   string `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
	WebuserID string `xml:"WebuserID,omitempty" json:"WebuserID,omitempty" yaml:"WebuserID,omitempty"`
}

// TrustsResponse was auto-generated from WSDL.
type TrustsResponse struct {
	ErrorCode        int           `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string        `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int           `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	Trusts           *ArrayOfTrust `xml:"Trusts,omitempty" json:"Trusts,omitempty" yaml:"Trusts,omitempty"`
}

// TwoTierAuthorisation was auto-generated from WSDL.
type TwoTierAuthorisation struct {
	XMLName              xml.Name    `xml:"http://tempuri.org/ TwoTierAuthorisation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/TwoTierAuthorisation"`
	Authorise            bool        `xml:"Authorise,omitempty" json:"Authorise,omitempty" yaml:"Authorise,omitempty"`
	IgnoreValidation     bool        `xml:"IgnoreValidation,omitempty" json:"IgnoreValidation,omitempty" yaml:"IgnoreValidation,omitempty"`
	OnFailure            bool        `xml:"OnFailure,omitempty" json:"OnFailure,omitempty" yaml:"OnFailure,omitempty"`
	Reason               string      `xml:"Reason,omitempty" json:"Reason,omitempty" yaml:"Reason,omitempty"`
	ShiftReferenceNumber *ArrayOfint `xml:"ShiftReferenceNumber,omitempty" json:"ShiftReferenceNumber,omitempty" yaml:"ShiftReferenceNumber,omitempty"`
	WebUserId            string      `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	RequesterId          string      `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	RequestId            int         `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken          string      `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// TwoTierAuthorisationResponse was auto-generated from WSDL.
type TwoTierAuthorisationResponse struct {
	ErrorCode        int                             `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string                          `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                             `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ReferenceNumber  *ArrayOfAuthorisedShiftsDetails `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
}

// TwoTierAuthorisationShiftStatus was auto-generated from WSDL.
type TwoTierAuthorisationShiftStatus struct {
	XMLName             xml.Name                 `xml:"http://tempuri.org/ TwoTierAuthorisationShiftStatus" json:"-" yaml:"-" action:"http://tempuri.org/IClient/TwoTierAuthorisationShiftStatus"`
	EndDate             DateTime                 `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	IncludeRangeRequest bool                     `xml:"IncludeRangeRequest,omitempty" json:"IncludeRangeRequest,omitempty" yaml:"IncludeRangeRequest,omitempty"`
	Location            string                   `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	ShiftStatus         ShiftAuthorisationStatus `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	StartDate           DateTime                 `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	Ward                string                   `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WebUserID           string                   `xml:"WebUserID,omitempty" json:"WebUserID,omitempty" yaml:"WebUserID,omitempty"`
	RequestId           int                      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken         string                   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// TwoTierAuthorisationShiftStatusResponse was auto-generated from
// WSDL.
type TwoTierAuthorisationShiftStatusResponse struct {
	ErrorCode                  int                                         `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription           string                                      `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId                 int                                         `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	TwoTierAuthorisationShifts *ArrayOfTwoTierAuthorisationShiftStatusType `xml:"TwoTierAuthorisationShifts,omitempty" json:"TwoTierAuthorisationShifts,omitempty" yaml:"TwoTierAuthorisationShifts,omitempty"`
}

// TwoTierAuthorisationShiftStatusType was auto-generated from
// WSDL.
type TwoTierAuthorisationShiftStatusType struct {
	Agency                          *ArrayOfAgency              `xml:"Agency,omitempty" json:"Agency,omitempty" yaml:"Agency,omitempty"`
	AssignmentCode                  string                      `xml:"AssignmentCode,omitempty" json:"AssignmentCode,omitempty" yaml:"AssignmentCode,omitempty"`
	BookingReferenceNumber          string                      `xml:"BookingReferenceNumber,omitempty" json:"BookingReferenceNumber,omitempty" yaml:"BookingReferenceNumber,omitempty"`
	EndTime                         DateTime                    `xml:"EndTime,omitempty" json:"EndTime,omitempty" yaml:"EndTime,omitempty"`
	Gender                          string                      `xml:"Gender,omitempty" json:"Gender,omitempty" yaml:"Gender,omitempty"`
	HasComment                      bool                        `xml:"HasComment,omitempty" json:"HasComment,omitempty" yaml:"HasComment,omitempty"`
	IsRangeRequest                  bool                        `xml:"IsRangeRequest,omitempty" json:"IsRangeRequest,omitempty" yaml:"IsRangeRequest,omitempty"`
	LocationId                      string                      `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	NoAgency                        bool                        `xml:"NoAgency,omitempty" json:"NoAgency,omitempty" yaml:"NoAgency,omitempty"`
	Note                            *ArrayOfCommentsComplexType `xml:"Note,omitempty" json:"Note,omitempty" yaml:"Note,omitempty"`
	OnCall                          bool                        `xml:"OnCall,omitempty" json:"OnCall,omitempty" yaml:"OnCall,omitempty"`
	ProbabilityScore                float64                     `xml:"ProbabilityScore,omitempty" json:"ProbabilityScore,omitempty" yaml:"ProbabilityScore,omitempty"`
	RangeRequestNum                 int                         `xml:"RangeRequestNum,omitempty" json:"RangeRequestNum,omitempty" yaml:"RangeRequestNum,omitempty"`
	RangeRequestToBeFilledBy        string                      `xml:"RangeRequestToBeFilledBy,omitempty" json:"RangeRequestToBeFilledBy,omitempty" yaml:"RangeRequestToBeFilledBy,omitempty"`
	SecondaryAssignmentCode         string                      `xml:"SecondaryAssignmentCode,omitempty" json:"SecondaryAssignmentCode,omitempty" yaml:"SecondaryAssignmentCode,omitempty"`
	SecondaryAssignmentCodeDuration float64                     `xml:"SecondaryAssignmentCodeDuration,omitempty" json:"SecondaryAssignmentCodeDuration,omitempty" yaml:"SecondaryAssignmentCodeDuration,omitempty"`
	ShiftDate                       DateTime                    `xml:"ShiftDate,omitempty" json:"ShiftDate,omitempty" yaml:"ShiftDate,omitempty"`
	ShiftStatus                     ShiftStatusEnum             `xml:"ShiftStatus,omitempty" json:"ShiftStatus,omitempty" yaml:"ShiftStatus,omitempty"`
	ShiftType                       ShiftType                   `xml:"ShiftType,omitempty" json:"ShiftType,omitempty" yaml:"ShiftType,omitempty"`
	StaffFacade                     *StaffFacadeType            `xml:"StaffFacade,omitempty" json:"StaffFacade,omitempty" yaml:"StaffFacade,omitempty"`
	StartTime                       DateTime                    `xml:"StartTime,omitempty" json:"StartTime,omitempty" yaml:"StartTime,omitempty"`
	TrustId                         string                      `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	WardId                          string                      `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
}

// TypeOfShifts was auto-generated from WSDL.
type TypeOfShifts struct {
	OnCall             bool `xml:"OnCall,omitempty" json:"OnCall,omitempty" yaml:"OnCall,omitempty"`
	ProgrammedActivity bool `xml:"ProgrammedActivity,omitempty" json:"ProgrammedActivity,omitempty" yaml:"ProgrammedActivity,omitempty"`
	SleepIn            bool `xml:"SleepIn,omitempty" json:"SleepIn,omitempty" yaml:"SleepIn,omitempty"`
	Standard           bool `xml:"Standard,omitempty" json:"Standard,omitempty" yaml:"Standard,omitempty"`
}

// UnAvailablePeriod was auto-generated from WSDL.
type UnAvailablePeriod struct {
	EndDate                              DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate                            DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
	StatutoryContinuousServiceStatusCode int      `xml:"StatutoryContinuousServiceStatusCode,omitempty" json:"StatutoryContinuousServiceStatusCode,omitempty" yaml:"StatutoryContinuousServiceStatusCode,omitempty"`
	StatutoryPayStatusCode               int      `xml:"StatutoryPayStatusCode,omitempty" json:"StatutoryPayStatusCode,omitempty" yaml:"StatutoryPayStatusCode,omitempty"`
	UnAvailabileReasonDescription        string   `xml:"UnAvailabileReasonDescription,omitempty" json:"UnAvailabileReasonDescription,omitempty" yaml:"UnAvailabileReasonDescription,omitempty"`
	UnavailabilityPeriodId               string   `xml:"UnavailabilityPeriodId,omitempty" json:"UnavailabilityPeriodId,omitempty" yaml:"UnavailabilityPeriodId,omitempty"`
}

// UnAvailableReasonCodesInformation was auto-generated from WSDL.
type UnAvailableReasonCodesInformation struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ UnAvailableReasonCodesInformation" json:"-" yaml:"-" action:"http://tempuri.org/IClient/UnAvailableReasonCodesInformation"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// UnAvailableReasonCodesInformationResponse was auto-generated
// from WSDL.
type UnAvailableReasonCodesInformationResponse struct {
	UnAvailableReasonCodesInformationResult *UnavailableReasonCodeInformationResponse `xml:"UnAvailableReasonCodesInformationResult,omitempty" json:"UnAvailableReasonCodesInformationResult,omitempty" yaml:"UnAvailableReasonCodesInformationResult,omitempty"`
}

// UnavailableReasonCodeInformationResponse was auto-generated
// from WSDL.
type UnavailableReasonCodeInformationResponse struct {
	ErrorCode        int                `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string             `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	ReasonCodes      *ArrayOfReasonCode `xml:"ReasonCodes,omitempty" json:"ReasonCodes,omitempty" yaml:"ReasonCodes,omitempty"`
}

// UnitChildrenRequest was auto-generated from WSDL.
type UnitChildrenRequest struct {
	XMLName     xml.Name `xml:"http://tempuri.org/ UnitChildrenRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/UnitChildrenRequest"`
	RequestId   int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	TrustId     string   `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	LcoationId  string   `xml:"LcoationId,omitempty" json:"LcoationId,omitempty" yaml:"LcoationId,omitempty"`
	CallerToken string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// UnitChildrenRequestResponse was auto-generated from WSDL.
type UnitChildrenRequestResponse struct {
	UnitChildrenRequestResult *UnitChildrenResponse `xml:"UnitChildrenRequestResult,omitempty" json:"UnitChildrenRequestResult,omitempty" yaml:"UnitChildrenRequestResult,omitempty"`
}

// UnitChildrenResponse was auto-generated from WSDL.
type UnitChildrenResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	LocationResponse *ArrayOfLocationType `xml:"LocationResponse,omitempty" json:"LocationResponse,omitempty" yaml:"LocationResponse,omitempty"`
	WardResponse     *ArrayOfWardDetails  `xml:"WardResponse,omitempty" json:"WardResponse,omitempty" yaml:"WardResponse,omitempty"`
}

// UnrestrictedWorkPeriod was auto-generated from WSDL.
type UnrestrictedWorkPeriod struct {
	EndDate   DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
}

// UpdateAvailabilityItemResponse was auto-generated from WSDL.
type UpdateAvailabilityItemResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateCourseCompletionStatusResponse was auto-generated from
// WSDL.
type UpdateCourseCompletionStatusResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateGeneralLeadTimeResponse was auto-generated from WSDL.
type UpdateGeneralLeadTimeResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateHolidayBookingResponse was auto-generated from WSDL.
type UpdateHolidayBookingResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateLeadTimesResponse was auto-generated from WSDL.
type UpdateLeadTimesResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateSubstantiveWorkerHolidaysResponse was auto-generated from
// WSDL.
type UpdateSubstantiveWorkerHolidaysResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	HolidayID        int    `xml:"HolidayID,omitempty" json:"HolidayID,omitempty" yaml:"HolidayID,omitempty"`
}

// UpdateUnavailablePeriodResponse was auto-generated from WSDL.
type UpdateUnavailablePeriodResponse struct {
	ErrorCode        int      `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string   `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int      `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	LatestEndDate    DateTime `xml:"LatestEndDate,omitempty" json:"LatestEndDate,omitempty" yaml:"LatestEndDate,omitempty"`
}

// UpdateWorkerResponse was auto-generated from WSDL.
type UpdateWorkerResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UpdateWorkingPatternResponse was auto-generated from WSDL.
type UpdateWorkingPatternResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// UserInfoForTimeSheetModificationResponse was auto-generated
// from WSDL.
type UserInfoForTimeSheetModificationResponse struct {
	ErrorCode         int                        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription  string                     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId        int                        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AssignmentCodes   *ArrayOfAssignmentCodeType `xml:"AssignmentCodes,omitempty" json:"AssignmentCodes,omitempty" yaml:"AssignmentCodes,omitempty"`
	FWCancelReasons   *ArrayOfFWCancelReason     `xml:"FWCancelReasons,omitempty" json:"FWCancelReasons,omitempty" yaml:"FWCancelReasons,omitempty"`
	Reasons           *ArrayOfReasons            `xml:"Reasons,omitempty" json:"Reasons,omitempty" yaml:"Reasons,omitempty"`
	ShiftTypes        *ArrayOfShiftType          `xml:"ShiftTypes,omitempty" json:"ShiftTypes,omitempty" yaml:"ShiftTypes,omitempty"`
	WardCancelReasons *ArrayOfWardCancelReason   `xml:"WardCancelReasons,omitempty" json:"WardCancelReasons,omitempty" yaml:"WardCancelReasons,omitempty"`
	Wards             *ArrayOfWard               `xml:"Wards,omitempty" json:"Wards,omitempty" yaml:"Wards,omitempty"`
	Locations         *ArrayOfLocation           `xml:"locations,omitempty" json:"locations,omitempty" yaml:"locations,omitempty"`
}

// VATComplexType was auto-generated from WSDL.
type VATComplexType struct {
	ApplicableTo  string   `xml:"ApplicableTo,omitempty" json:"ApplicableTo,omitempty" yaml:"ApplicableTo,omitempty"`
	EffectiveFrom DateTime `xml:"EffectiveFrom,omitempty" json:"EffectiveFrom,omitempty" yaml:"EffectiveFrom,omitempty"`
	VATRegistered bool     `xml:"VATRegistered,omitempty" json:"VATRegistered,omitempty" yaml:"VATRegistered,omitempty"`
}

// ValidationInformationResponse was auto-generated from WSDL.
type ValidationInformationResponse struct {
	ErrorCode           int                        `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription    string                     `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId          int                        `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	AssignmentCodeTypes *ArrayOfAssignmentCodeType `xml:"AssignmentCodeTypes,omitempty" json:"AssignmentCodeTypes,omitempty" yaml:"AssignmentCodeTypes,omitempty"`
	CRBIssueDate        DateTime                   `xml:"CRBIssueDate,omitempty" json:"CRBIssueDate,omitempty" yaml:"CRBIssueDate,omitempty"`
	CRBReferenceNumber  string                     `xml:"CRBReferenceNumber,omitempty" json:"CRBReferenceNumber,omitempty" yaml:"CRBReferenceNumber,omitempty"`
	IntentionToPractice bool                       `xml:"IntentionToPractice,omitempty" json:"IntentionToPractice,omitempty" yaml:"IntentionToPractice,omitempty"`
	RegistrationBody    string                     `xml:"RegistrationBody,omitempty" json:"RegistrationBody,omitempty" yaml:"RegistrationBody,omitempty"`
	RegistrationExpiry  DateTime                   `xml:"RegistrationExpiry,omitempty" json:"RegistrationExpiry,omitempty" yaml:"RegistrationExpiry,omitempty"`
	RegistrationNumber  string                     `xml:"RegistrationNumber,omitempty" json:"RegistrationNumber,omitempty" yaml:"RegistrationNumber,omitempty"`
	RevalidationDate    DateTime                   `xml:"RevalidationDate,omitempty" json:"RevalidationDate,omitempty" yaml:"RevalidationDate,omitempty"`
}

// VisaReview was auto-generated from WSDL.
type VisaReview struct {
	CheckedOn   DateTime `xml:"CheckedOn,omitempty" json:"CheckedOn,omitempty" yaml:"CheckedOn,omitempty"`
	DocsChecked bool     `xml:"DocsChecked,omitempty" json:"DocsChecked,omitempty" yaml:"DocsChecked,omitempty"`
	EndDate     DateTime `xml:"EndDate,omitempty" json:"EndDate,omitempty" yaml:"EndDate,omitempty"`
	StartDate   DateTime `xml:"StartDate,omitempty" json:"StartDate,omitempty" yaml:"StartDate,omitempty"`
}

// Ward was auto-generated from WSDL.
type Ward struct {
	Locations *Location `xml:"Locations,omitempty" json:"Locations,omitempty" yaml:"Locations,omitempty"`
	WardId    string    `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WardName  string    `xml:"WardName,omitempty" json:"WardName,omitempty" yaml:"WardName,omitempty"`
}

// WardCancelReason was auto-generated from WSDL.
type WardCancelReason struct {
	Code        string `xml:"Code,omitempty" json:"Code,omitempty" yaml:"Code,omitempty"`
	Description string `xml:"Description,omitempty" json:"Description,omitempty" yaml:"Description,omitempty"`
}

// WardDetails was auto-generated from WSDL.
type WardDetails struct {
	Address      string `xml:"Address,omitempty" json:"Address,omitempty" yaml:"Address,omitempty"`
	Directorate  string `xml:"Directorate,omitempty" json:"Directorate,omitempty" yaml:"Directorate,omitempty"`
	LocationId   string `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	LocationName string `xml:"LocationName,omitempty" json:"LocationName,omitempty" yaml:"LocationName,omitempty"`
	PhoneNumber  string `xml:"PhoneNumber,omitempty" json:"PhoneNumber,omitempty" yaml:"PhoneNumber,omitempty"`
	WardId       string `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WardName     string `xml:"WardName,omitempty" json:"WardName,omitempty" yaml:"WardName,omitempty"`
	WardType     string `xml:"WardType,omitempty" json:"WardType,omitempty" yaml:"WardType,omitempty"`
}

// WardInduction was auto-generated from WSDL.
type WardInduction struct {
	XMLName            xml.Name `xml:"http://tempuri.org/ WardInduction" json:"-" yaml:"-" action:"http://tempuri.org/IClient/WardInduction"`
	RequestId          int      `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	WebUserId          string   `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	Authorise          bool     `xml:"Authorise,omitempty" json:"Authorise,omitempty" yaml:"Authorise,omitempty"`
	InductionDelivered bool     `xml:"InductionDelivered,omitempty" json:"InductionDelivered,omitempty" yaml:"InductionDelivered,omitempty"`
	ReferenceNumber    int      `xml:"ReferenceNumber,omitempty" json:"ReferenceNumber,omitempty" yaml:"ReferenceNumber,omitempty"`
	RequesterId        string   `xml:"RequesterId,omitempty" json:"RequesterId,omitempty" yaml:"RequesterId,omitempty"`
	Release            bool     `xml:"Release,omitempty" json:"Release,omitempty" yaml:"Release,omitempty"`
	CallerToken        string   `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// WardInductionResponse was auto-generated from WSDL.
type WardInductionResponse struct {
	ErrorCode        int    `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int    `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
}

// WardType was auto-generated from WSDL.
type WardType struct {
	Location *Location `xml:"Location,omitempty" json:"Location,omitempty" yaml:"Location,omitempty"`
	WardId   string    `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	WardName string    `xml:"WardName,omitempty" json:"WardName,omitempty" yaml:"WardName,omitempty"`
}

// WardUserRequest was auto-generated from WSDL.
type WardUserRequest struct {
	XMLName               xml.Name  `xml:"http://tempuri.org/ WardUserRequest" json:"-" yaml:"-" action:"http://tempuri.org/IClient/WardUserRequest"`
	DateOfBirth           DateTime  `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	LocationId            string    `xml:"LocationId,omitempty" json:"LocationId,omitempty" yaml:"LocationId,omitempty"`
	Name                  *NameType `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	NumberOfWardsToReturn string    `xml:"NumberOfWardsToReturn,omitempty" json:"NumberOfWardsToReturn,omitempty" yaml:"NumberOfWardsToReturn,omitempty"`
	TrustId               string    `xml:"TrustId,omitempty" json:"TrustId,omitempty" yaml:"TrustId,omitempty"`
	WardId                string    `xml:"WardId,omitempty" json:"WardId,omitempty" yaml:"WardId,omitempty"`
	RequestId             int       `xml:"RequestId,omitempty" json:"RequestId,omitempty" yaml:"RequestId,omitempty"`
	CallerToken           string    `xml:"CallerToken,omitempty" json:"CallerToken,omitempty" yaml:"CallerToken,omitempty"`
}

// WardUserRequestResponse was auto-generated from WSDL.
type WardUserRequestResponse struct {
	WardUserRequestResult *WardUserResponse `xml:"WardUserRequestResult,omitempty" json:"WardUserRequestResult,omitempty" yaml:"WardUserRequestResult,omitempty"`
}

// WardUserResponse was auto-generated from WSDL.
type WardUserResponse struct {
	ErrorCode        int                  `xml:"ErrorCode,omitempty" json:"ErrorCode,omitempty" yaml:"ErrorCode,omitempty"`
	ErrorDescription string               `xml:"ErrorDescription,omitempty" json:"ErrorDescription,omitempty" yaml:"ErrorDescription,omitempty"`
	ResponseId       int                  `xml:"ResponseId,omitempty" json:"ResponseId,omitempty" yaml:"ResponseId,omitempty"`
	WardUser         *ArrayOfWardUserType `xml:"WardUser,omitempty" json:"WardUser,omitempty" yaml:"WardUser,omitempty"`
}

// WardUserType was auto-generated from WSDL.
type WardUserType struct {
	DateOfBirth          DateTime             `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	EmailDomains         *ArrayOfstring       `xml:"EmailDomains,omitempty" json:"EmailDomains,omitempty" yaml:"EmailDomains,omitempty"`
	Name                 *NameType            `xml:"Name,omitempty" json:"Name,omitempty" yaml:"Name,omitempty"`
	TrustUserPermissions *TrustUserPermission `xml:"TrustUserPermissions,omitempty" json:"TrustUserPermissions,omitempty" yaml:"TrustUserPermissions,omitempty"`
	Ward                 *ArrayOfWardType     `xml:"Ward,omitempty" json:"Ward,omitempty" yaml:"Ward,omitempty"`
	WebUserId            string               `xml:"WebUserId,omitempty" json:"WebUserId,omitempty" yaml:"WebUserId,omitempty"`
	WebUserType          WebUserType          `xml:"WebUserType,omitempty" json:"WebUserType,omitempty" yaml:"WebUserType,omitempty"`
}

// Worker was auto-generated from WSDL.
type Worker struct {
	Agencies    *ArrayOfAgency `xml:"Agencies,omitempty" json:"Agencies,omitempty" yaml:"Agencies,omitempty"`
	DateOfBirth DateTime       `xml:"DateOfBirth,omitempty" json:"DateOfBirth,omitempty" yaml:"DateOfBirth,omitempty"`
	FirstName   string         `xml:"FirstName,omitempty" json:"FirstName,omitempty" yaml:"FirstName,omitempty"`
	PostCode    string         `xml:"PostCode,omitempty" json:"PostCode,omitempty" yaml:"PostCode,omitempty"`
	StaffId     string         `xml:"StaffId,omitempty" json:"StaffId,omitempty" yaml:"StaffId,omitempty"`
	Surname     string         `xml:"Surname,omitempty" json:"Surname,omitempty" yaml:"Surname,omitempty"`
}

// iClient implements the IClient interface.
type iClient struct {
	cli *soap.Client
}

// AuthenticateCaller was auto-generated from WSDL.
func (p *iClient) AuthenticateCaller(α *AuthenticateCaller) (β *AuthenticateCallerResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AuthenticateCallerResponse `xml:"AuthenticateCallerResponse>AuthenticateCallerResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AcknowledgePushNotification was auto-generated from WSDL.
func (p *iClient) AcknowledgePushNotification(α *AcknowledgePushNotification) (β *AcknowledgePushNotificationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AcknowledgePushNotificationResponse `xml:"AcknowledgePushNotificationResponse>AcknowledgePushNotificationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AgencyRatesContract was auto-generated from WSDL.
func (p *iClient) AgencyRatesContract(α *AgencyRatesContract) (β *AgencyRatesContractResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AgencyRatesContractResponse `xml:"AgencyRatesContractResponse>AgencyRatesContractResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AgencyRequest was auto-generated from WSDL.
func (p *iClient) AgencyRequest(α *AgencyRequest) (β *AgencyRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AgencyRequestResponse `xml:"AgencyRequestResponse>AgencyRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AgencyStaffRequest was auto-generated from WSDL.
func (p *iClient) AgencyStaffRequest(α *AgencyStaffRequest) (β *AgencyStaffRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AgencyStaffRequestResponse `xml:"AgencyStaffRequestResponse>AgencyStaffRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AgencyUserRequest was auto-generated from WSDL.
func (p *iClient) AgencyUserRequest(α *AgencyUserRequest) (β *AgencyUserRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AgencyUserRequestResponse `xml:"AgencyUserRequestResponse>AgencyUserRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AmendTimeSheet was auto-generated from WSDL.
func (p *iClient) AmendTimeSheet(α *AmendTimeSheet) (β *AmendTimeSheetResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AmendTimeSheetResponse `xml:"AmendTimeSheetResponse>AmendTimeSheetResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AssignmentCodesRequest was auto-generated from WSDL.
func (p *iClient) AssignmentCodesRequest(α *AssignmentCodesRequest) (β *AssignmentCodesRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AssignmentCodesRequestResponse `xml:"AssignmentCodesRequestResponse>AssignmentCodesRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AuthenticateUser was auto-generated from WSDL.
func (p *iClient) AuthenticateUser(α *AuthenticateUser) (β *AuthenticateUserResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AuthenticateUserResponse `xml:"AuthenticateUserResponse>AuthenticateUserResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AvailableShifts was auto-generated from WSDL.
func (p *iClient) AvailableShifts(α *AvailableShifts) (β *AvailableShiftsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AvailableShiftsResponse `xml:"AvailableShiftsResponse>AvailableShiftsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// AvailableShifts_AdvancedSearch was auto-generated from WSDL.
func (p *iClient) AvailableShifts_AdvancedSearch(α *AvailableShifts_AdvancedSearch) (β *AvailableShifts_AdvancedSearchResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M AvailableShifts_AdvancedSearchResponse `xml:"AvailableShifts_AdvancedSearchResponse>AvailableShifts_AdvancedSearchResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// BookStaffRequest was auto-generated from WSDL.
func (p *iClient) BookStaffRequest(α *BookStaffRequest) (β *BookStaffRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M BookStaffRequestResponse `xml:"BookStaffRequestResponse>BookStaffRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// CancelBookingRequest was auto-generated from WSDL.
func (p *iClient) CancelBookingRequest(α *CancelBookingRequest) (β *CancelBookingRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M CancelBookingRequestResponse `xml:"CancelBookingRequestResponse>CancelBookingRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// CompletePerformanceEvaluation was auto-generated from WSDL.
func (p *iClient) CompletePerformanceEvaluation(α *CompletePerformanceEvaluation) (β *CompletePerformanceEvaluationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M CompletePerformanceEvaluationResponse `xml:"CompletePerformanceEvaluationResponse>CompletePerformanceEvaluationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// CreateShiftRequest was auto-generated from WSDL.
func (p *iClient) CreateShiftRequest(α *CreateShiftRequest) (β *CreateShiftRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M CreateShiftRequestResponse `xml:"CreateShiftRequestResponse>CreateShiftRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// DeleteShiftRequest was auto-generated from WSDL.
func (p *iClient) DeleteShiftRequest(α *DeleteShiftRequest) (β *DeleteShiftRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M DeleteShiftRequestResponse `xml:"DeleteShiftRequestResponse>DeleteShiftRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ExactShiftRequest was auto-generated from WSDL.
func (p *iClient) ExactShiftRequest(α *ExactShiftRequest) (β *ExactShiftRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ExactShiftRequestResponse `xml:"ExactShiftRequestResponse>ExactShiftRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ExactStaffRequest was auto-generated from WSDL.
func (p *iClient) ExactStaffRequest(α *ExactStaffRequest) (β *ExactStaffRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ExactStaffRequestResponse `xml:"ExactStaffRequestResponse>ExactStaffRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ExactUserRequest was auto-generated from WSDL.
func (p *iClient) ExactUserRequest(α *ExactUserRequest) (β *ExactUserRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ExactUserRequestResponse `xml:"ExactUserRequestResponse>ExactUserRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ForwardPerformanceEvaluation was auto-generated from WSDL.
func (p *iClient) ForwardPerformanceEvaluation(α *ForwardPerformanceEvaluation) (β *ForwardPerformanceEvaluationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ForwardPerformanceEvaluationResponse `xml:"ForwardPerformanceEvaluationResponse>ForwardPerformanceEvaluationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetAgencies was auto-generated from WSDL.
func (p *iClient) GetAgencies(α *GetAgencies) (β *GetAgenciesResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetAgenciesResponse `xml:"GetAgenciesResponse>GetAgenciesResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetAgenciesUsedByTrust was auto-generated from WSDL.
func (p *iClient) GetAgenciesUsedByTrust(α *GetAgenciesUsedByTrust) (β *GetAgenciesUsedByTrustResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetAgenciesUsedByTrustResponse `xml:"GetAgenciesUsedByTrustResponse>GetAgenciesUsedByTrustResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetAgencyRateContracts was auto-generated from WSDL.
func (p *iClient) GetAgencyRateContracts(α *GetAgencyRateContracts) (β *GetAgencyRateContractsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetAgencyRateContractsResponse `xml:"GetAgencyRateContractsResponse>GetAgencyRateContractsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetAvailableShifts was auto-generated from WSDL.
func (p *iClient) GetAvailableShifts(α *GetAvailableShifts) (β *GetAvailableShiftsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetAvailableShiftsResponse `xml:"GetAvailableShiftsResponse>GetAvailableShiftsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetBackDatedShifts was auto-generated from WSDL.
func (p *iClient) GetBackDatedShifts(α *GetBackDatedShifts) (β *GetBackDatedShiftsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetBackDatedShiftsResponse `xml:"GetBackDatedShiftsResponse>GetBackDatedShiftsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetBackingReport was auto-generated from WSDL.
func (p *iClient) GetBackingReport(α *GetBackingReport) (β *GetBackingReportResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetBackingReportResponse `xml:"GetBackingReportResponse>GetBackingReportResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetContractInformation was auto-generated from WSDL.
func (p *iClient) GetContractInformation(α *GetContractInformation) (β *GetContractInformationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetContractInformationResponse `xml:"GetContractInformationResponse>GetContractInformationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetInvoiceBackingReportsList was auto-generated from WSDL.
func (p *iClient) GetInvoiceBackingReportsList(α *GetInvoiceBackingReportsList) (β *GetInvoiceBackingReportsListResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetInvoiceBackingReportsListResponse `xml:"GetInvoiceBackingReportsListResponse>GetInvoiceBackingReportsListResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetInvoiceDetails was auto-generated from WSDL.
func (p *iClient) GetInvoiceDetails(α *GetInvoiceDetails) (β *GetInvoiceDetailsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetInvoiceDetailsResponse `xml:"GetInvoiceDetailsResponse>GetInvoiceDetailsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetInvoiceDetailsReportPdf was auto-generated from WSDL.
func (p *iClient) GetInvoiceDetailsReportPdf(α *GetInvoiceDetailsReportPdf) (β *GetInvoiceDetailsReportPdfResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetInvoiceDetailsReportPdfResponse `xml:"GetInvoiceDetailsReportPdfResponse>GetInvoiceDetailsReportPdfResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetInvoiceSummaryReportPdf was auto-generated from WSDL.
func (p *iClient) GetInvoiceSummaryReportPdf(α *GetInvoiceSummaryReportPdf) (β *GetInvoiceSummaryReportPdfResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetInvoiceSummaryReportPdfResponse `xml:"GetInvoiceSummaryReportPdfResponse>GetInvoiceSummaryReportPdfResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetNotifications was auto-generated from WSDL.
func (p *iClient) GetNotifications(α *GetNotifications) (β *GetNotificationsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetNotificationsResponse `xml:"GetNotificationsResponse>GetNotificationsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetPerformanceEvaluation was auto-generated from WSDL.
func (p *iClient) GetPerformanceEvaluation(α *GetPerformanceEvaluation) (β *GetPerformanceEvaluationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetPerformanceEvaluationResponse `xml:"GetPerformanceEvaluationResponse>GetPerformanceEvaluationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetPerformanceEvaluationReferral was auto-generated from WSDL.
func (p *iClient) GetPerformanceEvaluationReferral(α *GetPerformanceEvaluationReferral) (β *GetPerformanceEvaluationReferralResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetPerformanceEvaluationReferralResponse `xml:"GetPerformanceEvaluationReferralResponse>GetPerformanceEvaluationReferralResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetPersonalisedRatesPermission was auto-generated from WSDL.
func (p *iClient) GetPersonalisedRatesPermission(α *GetPersonalisedRatesPermission) (β *GetPersonalisedRatesPermissionResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetPersonalisedRatesPermissionResponse `xml:"GetPersonalisedRatesPermissionResponse>GetPersonalisedRatesPermissionResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetPriorQualifications was auto-generated from WSDL.
func (p *iClient) GetPriorQualifications(α *GetPriorQualifications) (β *GetPriorQualificationsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetPriorQualificationsResponse `xml:"GetPriorQualificationsResponse>GetPriorQualificationsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetPushNotifications was auto-generated from WSDL.
func (p *iClient) GetPushNotifications(α *GetPushNotifications) (β *GetPushNotificationsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetPushNotificationsResponse `xml:"GetPushNotificationsResponse>GetPushNotificationsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetSelfBillingAgreements was auto-generated from WSDL.
func (p *iClient) GetSelfBillingAgreements(α *GetSelfBillingAgreements) (β *GetSelfBillingAgreementsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetSelfBillingAgreementsResponse `xml:"GetSelfBillingAgreementsResponse>GetSelfBillingAgreementsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetShiftsSince was auto-generated from WSDL.
func (p *iClient) GetShiftsSince(α *GetShiftsSince) (β *GetShiftsSinceResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetShiftsSinceResponse `xml:"GetShiftsSinceResponse>GetShiftsSinceResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetTrusts was auto-generated from WSDL.
func (p *iClient) GetTrusts(α *GetTrusts) (β *GetTrustsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetTrustsResponse `xml:"GetTrustsResponse>GetTrustsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetUserInfoForTimeSheetModification was auto-generated from
// WSDL.
func (p *iClient) GetUserInfoForTimeSheetModification(α *GetUserInfoForTimeSheetModification) (β *GetUserInfoForTimeSheetModificationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetUserInfoForTimeSheetModificationResponse `xml:"GetUserInfoForTimeSheetModificationResponse>GetUserInfoForTimeSheetModificationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetWebUserIdOfWardUsers was auto-generated from WSDL.
func (p *iClient) GetWebUserIdOfWardUsers(α *GetWebUserIdOfWardUsers) (β *GetWebUserIdOfWardUsersResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetWebUserIdOfWardUsersResponse `xml:"GetWebUserIdOfWardUsersResponse>GetWebUserIdOfWardUsersResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifyContractStatus was auto-generated from WSDL.
func (p *iClient) ModifyContractStatus(α *ModifyContractStatus) (β *ModifyContractStatusResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifyContractStatusResponse `xml:"ModifyContractStatusResponse>ModifyContractStatusResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifyPersonalisedRatesPermission was auto-generated from WSDL.
func (p *iClient) ModifyPersonalisedRatesPermission(α *ModifyPersonalisedRatesPermission) (β *ModifyPersonalisedRatesPermissionResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifyPersonalisedRatesPermissionResponse `xml:"ModifyPersonalisedRatesPermissionResponse>ModifyPersonalisedRatesPermissionResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifyPriorQualification was auto-generated from WSDL.
func (p *iClient) ModifyPriorQualification(α *ModifyPriorQualification) (β *ModifyPriorQualificationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifyPriorQualificationResponse `xml:"ModifyPriorQualificationResponse>ModifyPriorQualificationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifySelfBillingAgreement was auto-generated from WSDL.
func (p *iClient) ModifySelfBillingAgreement(α *ModifySelfBillingAgreement) (β *ModifySelfBillingAgreementResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifySelfBillingAgreementResponse `xml:"ModifySelfBillingAgreementResponse>ModifySelfBillingAgreementResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifyShiftRequest was auto-generated from WSDL.
func (p *iClient) ModifyShiftRequest(α *ModifyShiftRequest) (β *ModifyShiftRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifyShiftRequestResponse `xml:"ModifyShiftRequestResponse>ModifyShiftRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ModifyTimeSheets was auto-generated from WSDL.
func (p *iClient) ModifyTimeSheets(α *ModifyTimeSheets) (β *ModifyTimeSheetsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ModifyTimeSheetsResponse `xml:"ModifyTimeSheetsResponse>ModifyTimeSheetsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// QueryTimeSheet was auto-generated from WSDL.
func (p *iClient) QueryTimeSheet(α *QueryTimeSheet) (β *QueryTimeSheetResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M QueryTimeSheetResponse `xml:"QueryTimeSheetResponse>QueryTimeSheetResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// SearchFlexibleWorker was auto-generated from WSDL.
func (p *iClient) SearchFlexibleWorker(α *SearchFlexibleWorker) (β *SearchFlexibleWorkerResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M SearchFlexibleWorkerResponse `xml:"SearchFlexibleWorkerResponse>SearchFlexibleWorkerResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// SearchWorker was auto-generated from WSDL.
func (p *iClient) SearchWorker(α *SearchWorker) (β *SearchWorkerResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M SearchWorkerResponse `xml:"SearchWorkerResponse>SearchWorkerResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// ShiftsRequest was auto-generated from WSDL.
func (p *iClient) ShiftsRequest(α *ShiftsRequest) (β *ShiftsRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M ShiftsRequestResponse `xml:"ShiftsRequestResponse>ShiftsRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// SplitShift was auto-generated from WSDL.
func (p *iClient) SplitShift(α *SplitShift) (β *SplitShiftResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M SplitShiftResponse `xml:"SplitShiftResponse>SplitShiftResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// StaffRequest was auto-generated from WSDL.
func (p *iClient) StaffRequest(α *StaffRequest) (β *StaffRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M StaffRequestResponse `xml:"StaffRequestResponse>StaffRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// TimeSheets was auto-generated from WSDL.
func (p *iClient) TimeSheets(α *TimeSheets) (β *TimeSheetsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M TimeSheetsResponse `xml:"TimeSheetsResponse>TimeSheetsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// TwoTierAuthorisation was auto-generated from WSDL.
func (p *iClient) TwoTierAuthorisation(α *TwoTierAuthorisation) (β *TwoTierAuthorisationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M TwoTierAuthorisationResponse `xml:"TwoTierAuthorisationResponse>TwoTierAuthorisationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// TwoTierAuthorisationShiftStatus was auto-generated from WSDL.
func (p *iClient) TwoTierAuthorisationShiftStatus(α *TwoTierAuthorisationShiftStatus) (β *TwoTierAuthorisationShiftStatusResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M TwoTierAuthorisationShiftStatusResponse `xml:"TwoTierAuthorisationShiftStatusResponse>TwoTierAuthorisationShiftStatusResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// UnAvailableReasonCodesInformation was auto-generated from WSDL.
func (p *iClient) UnAvailableReasonCodesInformation(α *UnAvailableReasonCodesInformation) (β *UnAvailableReasonCodesInformationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M UnAvailableReasonCodesInformationResponse `xml:"UnAvailableReasonCodesInformationResponse>UnAvailableReasonCodesInformationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// UnitChildrenRequest was auto-generated from WSDL.
func (p *iClient) UnitChildrenRequest(α *UnitChildrenRequest) (β *UnitChildrenRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M UnitChildrenRequestResponse `xml:"UnitChildrenRequestResponse>UnitChildrenRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// WardInduction was auto-generated from WSDL.
func (p *iClient) WardInduction(α *WardInduction) (β *WardInductionResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M WardInductionResponse `xml:"WardInductionResponse>WardInductionResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// WardUserRequest was auto-generated from WSDL.
func (p *iClient) WardUserRequest(α *WardUserRequest) (β *WardUserRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M WardUserRequestResponse `xml:"WardUserRequestResponse>WardUserRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetCancellationReasons was auto-generated from WSDL.
func (p *iClient) GetCancellationReasons(α *GetCancellationReasons) (β *GetCancellationReasonsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetCancellationReasonsResponse `xml:"GetCancellationReasonsResponse>GetCancellationReasonsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetJobCodesForTrust was auto-generated from WSDL.
func (p *iClient) GetJobCodesForTrust(α *GetJobCodesForTrust) (β *GetJobCodesForTrustResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetJobCodesForTrustResponse `xml:"GetJobCodesForTrustResponse>GetJobCodesForTrustResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetModifyTimeSheetReasons was auto-generated from WSDL.
func (p *iClient) GetModifyTimeSheetReasons(α *GetModifyTimeSheetReasons) (β *GetModifyTimeSheetReasonsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetModifyTimeSheetReasonsResponse `xml:"GetModifyTimeSheetReasonsResponse>GetModifyTimeSheetReasonsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetQueriedTimeSheets was auto-generated from WSDL.
func (p *iClient) GetQueriedTimeSheets(α *GetQueriedTimeSheets) (β *GetQueriedTimeSheetsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetQueriedTimeSheetsResponse `xml:"GetQueriedTimeSheetsResponse>GetQueriedTimeSheetsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetRangeRequestDetails was auto-generated from WSDL.
func (p *iClient) GetRangeRequestDetails(α *GetRangeRequestDetails) (β *GetRangeRequestDetailsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetRangeRequestDetailsResponse `xml:"GetRangeRequestDetailsResponse>GetRangeRequestDetailsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetTrustStaffGroupAuthorisationCodeSetting was auto-generated
// from WSDL.
func (p *iClient) GetTrustStaffGroupAuthorisationCodeSetting(α *GetTrustStaffGroupAuthorisationCodeSetting) (β *GetTrustStaffGroupAuthorisationCodeSettingResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetTrustStaffGroupAuthorisationCodeSettingResponse `xml:"GetTrustStaffGroupAuthorisationCodeSettingResponse>GetTrustStaffGroupAuthorisationCodeSettingResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// BoardView was auto-generated from WSDL.
func (p *iClient) BoardView(α *BoardView) (β *BoardViewResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M BoardViewResponse `xml:"BoardViewResponse>BoardViewResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetSurfaceInformation was auto-generated from WSDL.
func (p *iClient) GetSurfaceInformation(α *GetSurfaceInformation) (β *GetSurfaceInformationResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetSurfaceInformationResponse `xml:"GetSurfaceInformationResponse>GetSurfaceInformationResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// GetAwaitingAuthorisationTimesheets was auto-generated from WSDL.
func (p *iClient) GetAwaitingAuthorisationTimesheets(α *GetAwaitingAuthorisationTimesheets) (β *GetAwaitingAuthorisationTimesheetsResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M GetAwaitingAuthorisationTimesheetsResponse `xml:"GetAwaitingAuthorisationTimesheetsResponse>GetAwaitingAuthorisationTimesheetsResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}

// SuggestedWorkerRequest was auto-generated from WSDL.
func (p *iClient) SuggestedWorkerRequest(α *SuggestedWorkerRequest) (β *SuggestedWorkerRequestResponse, err error) {
	γ := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    struct {
			M SuggestedWorkerRequestResponse `xml:"SuggestedWorkerRequestResponse>SuggestedWorkerRequestResult"`
		}
	}{}
	if err = p.cli.RoundTrip(α, &γ); err != nil {
		return nil, err
	}
	return &γ.Body.M, nil
}
