package nhsppoc

import (
	//"io/ioutil"
	"log"
	wssbclient "nhsppoc/nhspInterfaces"

	wsclient "golang.org/x/net/websocket"
	//	model "nhsppoc/nhspInterfaces/wsdlgo"
	"encoding/json"
	//"github.com/golang/protobuf/proto"
	"net/http/httptest"

	//"gopkg.in/mgo.v2/bson"
	//protobuff "nhsppoc/protobuffModel"
	"strings"
	"testing"
	"time"
)

func TestConnection(t *testing.T) {
	/*tt := time.Now()
	d, err := time.ParseDuration("24h")
	if err != nil {
		t.Error("Wrong Duration")
	}*/

	wssbclient.InitClient()
	server := NewServer("/ws")
	s := httptest.NewServer(server.GetHandler())
	go server.Listen()
	defer s.Close()

	/*wsclient.InitClient()
	dataSummary := SearchForShifts()
	for shift := range dataSummary.DataChannel {
		log.Println("Booking:", shift.BookingReferenceNumber)

	}*/
	var url = strings.Replace(s.URL, "http", "ws", -1)
	var origin = "http://localhost/"
	ws, err := wsclient.Dial(url, "", origin)
	if err != nil {
		log.Fatal(err)
	}

	/*vv := &protobuff.Message{}
	vv.Text = "sss"
	message, err := proto.Marshal(vv)
	_, err = ws.Write(message)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Send: %s\n", vv.Text)*/
	send(ws)
	//message := &protobuff.Shift{}
	/*message1 := &protobuff.DataSummary{}
	message2 := &protobuff.Shift{}*/
	resp := &ResponseMessage{}

	readchan := make(chan int, 1)
	go func() {
		//ws.Read(msg)
		//msg, err = ioutil.ReadAll(ws)

		for {
			if err := json.NewDecoder(ws).Decode(resp); err != nil {
				log.Panicf("JSON Unmarshal error: %v\n", err.Error())
			}
			log.Printf("Received JSON %v\n", resp)
			/*json.NewDecoder(ws).Decode(resp)
			if err != nil {
				log.Fatal(err)
			}*/
			/*switch resp.MessageType {
			case 0:
				if err := proto.Unmarshal(resp.Message, message1); err != nil {
					log.Panicf("PROTO Unmarshal error: %v\n", err.Error())
				}
				log.Printf("Received DATASUMMARY %v\n", message1)
				//SearchForShifts

			case 1:
				if err := proto.Unmarshal(resp.Message, message2); err != nil {
					log.Panicf("PROTO Unmarshal error: %v\n", err.Error())
				}
				log.Printf("Received SHIFT %v\n", message2)
				//SearchForShifts
			}*/

			// log.Println("Proto Unmarshal")
		}
		//ws.Read(msg)

	}()
	select {
	case <-readchan:

		/*log.Println("JSON Unmarshal")
		if err := proto.Unmarshal(resp.Message, message); err != nil {
			log.Panicf("PROTO Unmarshal error: %v\n", err.Error())
		}
		log.Println("Proto Unmarshal")*/

	case <-time.After(time.Second * 8):
		log.Println("timeout 2")
	}

	/*for i := 0; i < 7; i++ {
		t0 := tt.Add(d)
		log.Println("Same, in UTC:", t0.UTC().Format(time.RFC3339))

		reply3, err3 := wsclient.Wsclient.BoardView(&model.BoardView{RequestId: 55, LocationId: "All", WardId: "All", CallerToken: reply.CallerToken, WebUserId: reply2.WebUserID, DateRange: "CustomDateRange", StartDate: "2016-08-29T00:00:00.000Z", EndDate: "2016-09-03T23:59:59.999Z"})
		if len(reply3.Shifts.IVRShiftType) == 21 || err3 != nil {
			t.Error("reply3.Shifts.IVRShiftType, got ", reply3, err3)
		}
	}*/

}

func send(ws *wsclient.Conn) {
	var req []byte
	var err error
	if req, err = json.Marshal(sendRequest()); err != nil {
		log.Panicf("ERROR MARSHAL REQ", err.Error())
	}

	log.Printf("MARSHAL REQ", string(req))

	if _, err = ws.Write(req); err != nil {
		log.Panicf("ERROR SEND REQ", err.Error())
	}

}

func sendRequest() IncomingMessage {
	var params = SearchParams{
		LocationId: "All",
		WardId:     "All",
		DateRange:  "CustomDateRange",
		StartDate:  "2016-08-29T00:00:00.000Z",
		EndDate:    "2016-09-03T23:59:59.999Z",
	}
	var rawParams json.RawMessage
	rawParams, err := json.Marshal(params)
	if err != nil {
		log.Println("error:", err)
	}
	var req = IncomingMessage{
		Action: ACTION_SEARCH,
		Params: &rawParams,
	}
	return req
}
